package com.gzhennaxia.gtdbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GtdBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(GtdBackendApplication.class, args);
    }

}
