import request from '../utils/request'

export function lastDay(query) {
  return request({
    url: '/summary/selectLastDays',
    method: 'get',
    params: query
  })
}

export function getTotalExpenses(query) {
  return request({
    url: '/summary/totalExpenses',
    method: 'get',
    params: query
  })
}
