function getRandomArbitrary(min, max) {
  return (Math.random() * (max - min) + min).toFixed(2)
}

module.exports = [
  {
    url: '/dailySummaryReport/selectLast30Days',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: [
          {
            'date': new Date(2022, 0, 0),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 1),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 2),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 3),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 4),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 5),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 6),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 7),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 8),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 9),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 10),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 11),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 12),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 13),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 14),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 15),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 16),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 17),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 18),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 19),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 20),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 21),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 22),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 23),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 24),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 25),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 26),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 27),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 28),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          },
          {
            'date': new Date(2022, 0, 29),
            'income': getRandomArbitrary(0, 1000),
            'expense': getRandomArbitrary(0, 1000)
          }
        ]
      }
    }
  }
]
