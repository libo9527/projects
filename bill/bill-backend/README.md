# 需求分析

## 业务

- 金额，正数代表收入，负数代表支出。

### 账目类别

一条账目具有类别属性，即这条账目属于什么类别的花费。

#### 类别具有树形结构的层级关系

【人际关系】>【生日礼物】>【水果】>【苹果】

【食】>【水果】>【苹果】

这里的两个【苹果】分类的含义是不同的，算两个分类，即不同分支下是允许重名的。

#### 类别与账目的关系是一对多的关系

一条账目的类别是最低级的类别（叶子结点）



#### 微信乘车码乘车记录

微信乘车码通过收集截屏，微信图片文字识别，提取出文字，二次编辑后，上传，更新快捷账单记录，之后再手动对账/自动对账。

文件格式：xlsx

| 线路        | 金额 | 时间           |
| ----------- | ---- | -------------- |
| 布吉-丹竹头 | -2   | 2021/8/6 7:01  |
| 五和-布吉   | -3   | 2021/8/5 19:04 |



# 原型设计

# [数据库](DATABASE.md)

# 技术清单

## 前端

## 后端

- Apache POI：操作 Excel 文件
- Apache Commons CSV：操作 CSV 文件 

### Spring Boot自定义BigDecimal精度序列化

> [Spring Boot自定义BigDecimal精度序列化](http://www.hushowly.com/articles/1332)

注意：

```java
// 这里要把 writeNumber 改成 writeString
// gen.writeNumber(decimalFormat.format(value));
gen.writeString(decimalFormat.format(value));
```



[Windows下如何查看某个端口被谁占用| 菜鸟教程](https://www.runoob.com/w3cnote/windows-finds-port-usage.html)



[intellij idea设置打开多个文件显示在多行tab上_With_Her的博客 ...](https://blog.csdn.net/With_Her/article/details/82254187)



[Spring Boot优雅地处理404异常- 程序员自由之路- 博客园](https://www.cnblogs.com/54chensongxia/p/14007696.html)



[Spring Boot 统一RESTful接口响应和统一异常处理- Naylor ...](https://www.cnblogs.com/Naylor/p/13687719.html)

### Lombok

Lombok 使用 `@RequiredArgsConstructor` 时，被注入的对象需要被定义为常量，否则会报空指针

### Spring Boot 请求参数为枚举类型

> [Spring Boot绑定枚举类型参数](https://blog.csdn.net/u014527058/article/details/62883573)
>
> [Spring Boot 使用枚举类型作为请求参数](https://xkcoding.com/2019/01/30/spring-boot-request-use-enums-params.html)
>
> [如何在springboot优雅的使用枚举- 知乎](https://zhuanlan.zhihu.com/p/150370826)

在Spring MVC和Spring Boot中，由于从客户端接收到的请求都被视为String类型，所以只能用String转枚举的converter。

### Mybatis-plus 代码生成

> [代码生成器 | MyBatis-Plus](https://baomidou.com/guide/generator.html)

模板数据从 `com.baomidou.mybatisplus.generator.engine.AbstractTemplateEngine#getObjectMap` 查

### Java 1.8 新的日期时间类

> [LocalDateTime - 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1252599548343744/1303871087444002)

### Java 1.8 Supplier

> [java8学习之Supplier与函数式接口总结- cexo - 博客园](https://www.cnblogs.com/webor2006/p/8243874.html)

### Spring Boot + Logback

> [使用SLF4J和Logback | 廖雪峰的官方网站](https://www.liaoxuefeng.com/wiki/1252599548343744/1264739155914176)
>
> [spring boot配置logback日志_凌大大的博客-CSDN博客](https://blog.csdn.net/wohaqiyi/article/details/72853962)

1. 配置文件名字不要定义为 logback.xml

   > Because the standard `logback.xml` configuration file is loaded too early, you cannot use extensions in it. You need to either use `logback-spring.xml` or define a `logging.config` property.
   >
   > 由于标准的 logback.xml 配置文件加载得太早，因此您不能在其中使用扩展名。您需要使用 logback-spring.xml 或定义 logging.config 属性。

   ![image-20210510161003674](D:\GitLab\projects\images\image-20210510161003674.png)

   如果配置文件名为 `logback.xml`，而且里面用到了 `LOG_PATH`，则刚启动时是读不到 `application.yml` 里相应的配置项 `logging.file.path` 的，会将日志文件保存到 `LOG_PATH_IS_UNDEFINED` 目录下。

2. 如果要在自定义的 logback 配置文件中使用默认的一些配置，需要引入默认的配置文件

   ```xml
   <configuration>
       <include resource="org/springframework/boot/logging/logback/defaults.xml"/>
   </configuration>
   ```

3. 文件拆分策略 TimeBasedRollingPolicy

   ```xml
   <configuration>
       <!-- DEBUG -->
       <appender name="debugAppender" class="ch.qos.logback.core.rolling.RollingFileAppender">
           <file>${LOG_PATH}/debug/debug.log</file>
           <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
               <fileNamePattern>${LOG_PATH}/debug/debug-%d{yyyy-MM-dd_HH-mm-ss}.log</fileNamePattern>
               <MaxHistory>2</MaxHistory>
               <!-- 当文件大于 10kB 时会将最旧的日志删除 -->
               <TotalSizeCap>10kB</TotalSizeCap>
           </rollingPolicy>
           <encoder>
               <pattern>${FILE_LOG_PATTERN}</pattern>
           </encoder>
           <filter class="ch.qos.logback.classic.filter.LevelFilter">
               <level>DEBUG</level>
               <onMatch>ACCEPT</onMatch>
               <onMismatch>DENY</onMismatch>
           </filter>
       </appender>
   </configuration>
   ```

   按时间拆分，比如 `%d{yyyy-MM-dd_HH-mm-ss}` 表示按秒拆分，每单位秒内日志的拆分为一个文件。

   MaxHistory=2：最大历史数为2，拆分时间单位如果为秒，则表示最多保存 2 秒前的日志，当有新日志时，会清空该日志前 2 秒再往前的数据，即前 3,4... 秒前的数据都会被清空。

   TotalSizeCap=10kb：总容量为10kb，当有新日志时，会检测该级别的日志文件总容量是否超过 10 kb，如果超过了，就会清空掉这些数据，重新开始打印。

### Spring Boot Validation

> [【SpringBoot】使用hibernate-validator实现优雅的参数校验](https://blog.csdn.net/qq877728715/article/details/113795082)

### mybatis

> [mybatis 对象 List<String> List<Integer>属性 映射](https://blog.csdn.net/a1064072510/article/details/100420401)

MySQL

> [insert into table, data from another table?](https://stackoverflow.com/questions/4241621/mysql-insert-into-table-data-from-another-table)

```mysql
INSERT INTO action_2_members ( campaign_id, mobile, vote, vote_date ) 
SELECT
campaign_id,
from_number,
received_msg,
date_received 
FROM
	received_txts 
WHERE
	campaign_id = '8'
```

### Spring Boot 获取容器中的类

> [[十\]SpringBoot 之 普通类获取Spring容器中的bean](https://www.cnblogs.com/s648667069/p/6489557.html)

### [Java获取接口所有实现类的方式](https://www.cnblogs.com/heaveneleven/p/9125228.html)

### [策略模式：代替if-else-if](https://blog.csdn.net/SkipperKevin/article/details/77370880)

## 测试

### 自动化测试

#### Selenium

> SELENIUM.md

# 规范

传给前端：xxxVo

接收前端：xxxParam

# TODO

- [ ] 删除重复账单
- [ ] 修改层级结构的数据（分类/交易平台/交易对方等）时，发送消息到消息队列，已解耦！
- [ ] Selenium 爬虫结合代理获取请求结果，保存json数据到数据库
- [ ] com.gzhennaxia.bill.controller.FileController.eleOrder 把对账部分分离出来，异步执行。