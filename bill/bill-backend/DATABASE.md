# 数据库

> 选用内嵌式数据库 SQLite

## 设计原则

- 状态字段采用枚举类实现，不存字典表。
- 层级结构的根节点 ID 统一为 0
- 层级节点路径采用 `,` 连接，例如 `0,1`

## 表

### common column


| Field       | Type & Length | Default | Note     |
| ----------- | ------------- | ------- | -------- |
| create_by   | integer       | null    | 创建者   |
| create_time | datetime      | null    | 创建时间 |
| update_by   | integer       | null    | 更新者   |
| update_time | datetime      | null    | 更新时间 |
| del_flag    | integer       | 0       | 删除位   |

### user

> 用户表


| Field    | Type & Length | Default          | Note        |
| -------- | ------------- | ---------------- | ----------- |
| id       | integer       | Auto Increasment | primary key |
| username | integer       | null             | 用户名      |
| password | varchar       | null             | 密码        |
| status   | integer       | null             | 状态        |

### bill

> 账单表


| Field | Type & Length | Default | Note |
| ----- | ---------- | ---- | ---- |
| id    | integer | Auto Increasment | primary key |
| user_id | integer | null | 用户ID |
| category_id | integer | null | 分类<br />其余见分类表 bill_category |
| desc       | varchar       | null             | 描述                                                       |
| note       | varchar       | null             | 备注                                                       |
| counterparty_id | integer | null | 交易对方 |
| counterparty_order_id | varchar | null | 交易对方订单号 |
| platform_id | integer | null | 交易平台：<br />null：未知<br />1：支付宝<br />2：微信<br />3：淘宝<br />4：美团<br />其余见交易平台表 platform |
| pay_type          | integer       | null             | 支付方式                                                     |
| pay_order_id | varchar | null | 支付方订单号 |
| type              | integer       | -1            | 账单类型<br />1：收入<br />-1：支出                         |
| ~~payable_amount~~ | decimal       | null             | 应付金额                                                     |
| ~~payment_amount~~ | decimal       | null             | 实付金额                                                     |
| ~~receivable_amount~~ | decimal       | null             | 应收金额                                                     |
| ~~receipts_amount~~ | decimal       | null             | 实收金额                                                     |
| payable_or_receivable_amount | decimal | null | 应付/应收金额 |
| payment_or_receipts_amount | decimal | null | 实付/实收金额 |
| ~~discount_id~~   | integer       | null             | 折扣ID  |
| status          | integer       | 0             | 状态<br />0: 待填充账单项<br />1: 待完善账单项<br />2: 待归档<br />3: 已归档 |

账单归档是把账单数据备份到另一个完整账单库中，按年份分库。商品数据在主库中，所有年份公用。

账单表数据库结构改变时注意要同步到归档库中。

未归档的数据不允许删除，已归档的数据才可以删除。

归档时需要重新进行汇总，并更新到汇总表中，

实时汇总数据=已归档数据的汇总数据+汇总未归档数据。即进行增量汇总！同时需要支持全量汇总功能。

### bill_item

> 账单项表


| Field                        | Type & Length | Default          | Note          |
| ---------------------------- | ------------- | ---------------- | ------------- |
| id                           | integer       | Auto Increasment | primary key   |
| bill_id                      | integer       | null             | 账单ID        |
| product_id                   | integer       | null             | 商品ID        |
| ~~payable_amount~~           | decimal       | null             | 应付金额      |
| ~~payment_amount~~           | decimal       | null             | 实付金额      |
| ~~receivable_amount~~        | decimal       | null             | 应收金额      |
| ~~receipts_amount~~          | decimal       | null             | 实收金额      |
| payable_or_receivable_amount | decimal       | null             | 应付/应收金额 |
| payment_or_receipts_amount   | decimal       | null             | 实付/实收金额 |
| note                         | varchar       | null             | 备注          |

### bill_item_sku_rel

> 账单项与规格的关系表（一条账单项可以有多个规格）


| Field        | Type & Length | Default          | Note        |
| ------------ | ------------- | ---------------- | ----------- |
| id           | integer       | Auto Increasment | primary key |
| bill_item_id | integer       | null             | 账单项ID    |
| product_id   | integer       | null             | 商品ID      |
| sku_id       | integer       | null             | 规格        |
| price        | decimal       | null             | 规格价格    |
| quantity     | integer       | null             | 规格数量    |

### bill_item_discount_rel

> 账单项与折扣的关系表（一条账单项可以有多个折扣）


| Field        | Type & Length | Default          | Note        |
| ------------ | ------------- | ---------------- | ----------- |
| id           | integer       | Auto Increasment | primary key |
| bill_item_id | integer       | null             | 账单项ID    |
| discount_id  | integer       | null             | 折扣ID      |

### platform

> 交易平台表


| Field     | Type & Length | Default          | Note                                 |
| --------- | ------------- | ---------------- | ------------------------------------ |
| id        | integer       | Auto Increasment | primary key                          |
| parent_id | integer       | null             | 父级ID                               |
| name      | varchar       | null             | 来源名称                             |
| path      | varchar       | null             | 路径                                 |
| is_leaf   | integer       | 1                | 是否为叶子节点<br />0：否<br />1：是 |
| status    | integer       | null             | 状态                                 |

### bill_category

> 账单分类表


| Field     | Type & Length | Default          | Note        |
| --------- | ------------- | ---------------- | ----------- |
| id        | integer       | Auto Increasment | primary key |
| parent_id | integer       | null             | 父级ID      |
| name      | varchar       | null             | 分类名称    |
| path | varchar | null | 路径 |
| is_leaf      | integer       | 1             | 是否为叶子节点<br />0：否<br />1：是 |
| status    | integer       | null             | 状态        |

### bill_tag

> 账单标记表


| Field  | Type & Length | Default          | Note        |
| ------ | ------------- | ---------------- | ----------- |
| id     | integer       | Auto Increasment | primary key |
| name   | varchar       | null             | 标记名称    |
| status | integer       | null             | 状态        |

### bill_tag_rel

> 账单标记关系表

| Field   | Type & Length | Default          | Note        |
| ------- | ------------- | ---------------- | ----------- |
| id      | integer       | Auto Increasment | primary key |
| bill_id | integer       | null             | 账单ID      |
| tag_id  | integer       | null             | 标记ID      |

### pay_type

> 支付方式表


| Field     | Type & Length | Default          | Note                                 |
| --------- | ------------- | ---------------- | ------------------------------------ |
| id        | integer       | Auto Increasment | primary key                          |
| parent_id | integer       | null             | 父级ID                               |
| name      | varchar       | null             | 名称                                 |
| path      | varchar       | null             | 路径                                 |
| is_leaf   | integer       | 1                | 是否为叶子节点<br />0：否<br />1：是 |
| status    | integer       | null             | 状态                                 |

### product

> 商品表

| Field       | Type & Length | Default          | Note        |
| ----------- | ------------- | ---------------- | ----------- |
| id          | integer       | Auto Increasment | primary key |
| category_id | integer       | null             | 分类ID      |
| name        | varchar       | null             | 商品名称    |

### product_category

> 商品分类表

| Field     | Type & Length | Default          | Note                                 |
| --------- | ------------- | ---------------- | ------------------------------------ |
| id        | integer       | Auto Increasment | primary key                          |
| parent_id | integer       | null             | 父级ID                               |
| name      | varchar       | null             | 分类名称                             |
| path      | varchar       | null             | 路径                                 |
| is_leaf   | integer       | 1                | 是否为叶子节点<br />0：否<br />1：是 |
| status    | integer       | null             | 状态                                 |

### product_price

> 商品价格表

| Field       | Type & Length | Default | Note     |
| ----------- | ------------- | ------- | -------- |
| product_id  | integer       | null    | 商品ID   |
| plateform   | varchar       | null    | 平台名称 |
| sku_id      | integer       | null    | 规格ID   |
| price       | decimal       | null    | 价格     |
| discount_id | integer       | null    | 折扣ID   |
| start_time  | datetime      | null    | 开始时间 |
| end_time    | datetime      | null    | 结束时间 |
| status      | integer       | null    | 状态     |

### sku

> 规格表

| Field | Type & Length | Default          | Note        |
| ----- | ------------- | ---------------- | ----------- |
| id    | integer       | Auto Increasment | primary key |
| desc  | varchar       | null             | 规格描述    |
| unit  | varchar       | null             | 单位        |
|       |               |                  |             |

### discount

> 折扣表

| Field | Type & Length | Default          | Note        |
| ----- | ------------- | ---------------- | ----------- |
| id    | integer       | Auto Increasment | primary key |
| desc  | varchar       | null             | 折扣描述    |
|       |               |                  |             |
|       |               |                  |             |

### bill_discount_rel

> 账单折扣关系表

| Field       | Type & Length | Default          | Note        |
| ----------- | ------------- | ---------------- | ----------- |
| id          | integer       | Auto Increasment | primary key |
| bill_id     | integer       | null             | 账单ID      |
| discount_id | integer       | null             | 折扣ID      |

### quick_bill

> 快捷账单表

| Field   | Type & Length | Default          | Note        |
| ------- | ------------- | ---------------- | ----------- |
| id      | integer       | Auto Increasment | primary key |
| type                   | integer       | null             | 账单类型<br />0：收入<br />1：支出                  |
| desc | varchar       | null            | 描述                |
| consume_time        |        datetime       | null | 消费时间 |
| category_name | varchar | null | 分类名 |
| money | varchar | null | 金额 |
| pay_way | varchar | null | 支付方式 |
| pay_order_id | varchar | null | 支付方订单号 |
| counterparty_order_id | varchar | null | 交易对方订单号 |
| counterparty_name | varchar | null | 交易对方 |
| platform_name | varchar | null | 交易平台 |
| note | varchar | null | 备注 |
|   status      |         integer      | 0 | 状态<br />0：待对账<br />1：对账失败<br />2：已对账 |
| reconciliation_time | datetime | null | 对账时间 |
| reconciliation_message | varchar | null | 对账信息 |
| bill_id | ineger | null | 正式账单ID |

### quick_bill_item

> 快捷账单明细表

| Field         | Type & Length | Default          | Note        |
| ------------- | ------------- | ---------------- | ----------- |
| id            | integer       | Auto Increasment | primary key |
| quick_bill_id | integer       | null             | 快捷账单ID  |
| product_id    | integer       | null             | 商品ID      |
| sku_id        | integer       | null             | 商品规格ID  |
| price         | decimal       | null             | 单价        |
| quantity      | integer       | null             | 数量        |
| total_price   | decimal       | null             | 总价        |
| discount_id   | integer       | null             | 折扣ID      |
| note          | varchar       | null             | 备注        |
| status          | integer       | null             | 状态 |

### import_history

> 导入历史表

| Field         | Type & Length | Default          | Note                                               |
| ------------- | ------------- | ---------------- | -------------------------------------------------- |
| id            | integer       | Auto Increasment | primary key                                        |
| file_path     | varchar       | null             | 文件绝对路径                                       |
| upload_time   | datetime      | null             | 文件上传时间                                       |
| import_time   | datetime      | null             | 导入时间                                           |
| success_count | integer       | null             | 成功条数                                           |
| repeat_count  | integer       | null             | 重复条数：有多少条记录是数据库已经存在的           |
| failed_count  | integer       | null             | 失败条数                                           |
| total         | integer       | null             | 导入总数                                           |
| status        | integer       | 0                | 状态<br />0：待导入快捷账单<br />1：已导入快捷账单 |
| del_flag      | integer       | 0                | 文件是否已删除                                     |

### counterparty

> 交易对方


| Field     | Type & Length | Default          | Note                                 |
| --------- | ------------- | ---------------- | ------------------------------------ |
| id        | integer       | Auto Increasment | primary key                          |
| parent_id | integer       | null             | 父级ID                               |
| name      | varchar       | null             | 分类名称                             |
| path      | varchar       | null             | 路径                                 |
| is_leaf   | integer       | 1                | 是否为叶子节点<br />0：否<br />1：是 |
| status    | integer       | null             | 状态                                 |

### thirdparty_bill

> 第三方账单表

| Field             | Type & Length | Default          | Note                |
| ----------------- | ------------- | ---------------- | ------------------- |
| id                | integer       | Auto Increasment | primary key         |
| type              | integer       | null             | 类型<br />1：饿了么 |
| consume_time      | datetime      | null             | 消费时间            |
| thirdparty_id     | varchar       | null             | 第三方主键ID        |
| counterparty_name | varchar       | null             | 交易对方            |
| desc              | varchar       | null             | 描述                |
| price             | decimal       | null             | 价格                |
| json_data         | text          | null             | JSON数据字符串      |

### analytics_result

> 统计结果表

| Field   | Type & Length | Default          | Note                                                         |
| ------- | ------------- | ---------------- | ------------------------------------------------------------ |
| id      | integer       | Auto Increasment | primary key                                                  |
| type    | integer       | null             | 类型<br />1：最新一年的年度总收入<br />2：最新一年的年度总支出 |
| label   | varchar       | null             | 统计标签                                                     |
| income  | decimal       | null             | 收入                                                         |
| expense | decimal       | null             | 支出                                                         |

### daily_summary_report

> 日汇总报表

| Field   | Type & Length | Default          | Note        |
| ------- | ------------- | ---------------- | ----------- |
| id      | integer       | Auto Increasment | primary key |
| date    | datetime      | null             | 日期        |
| income  | decimal       | null             | 收入        |
| expense | decimal       | null             | 支出        |



## SQLite

在 SQLite 数据库中，使用 int 数据类型无法将该主键设置为自增，只有设置为 interger 数据类型，才能自增。

## 枚举类的序列化

> [通用枚举 | MyBatis-Plus](https://baomidou.com/guide/enum.html) （其中 Fastjson 方式[无法实现](https://github.com/alibaba/fastjson/issues/293)，因为即使设置了 `@JSONField(serialzeFeatures= SerializerFeature.WriteEnumUsingToString)` fastjson 还是调用枚举的 `name()`）
>
> https://github.com/alibaba/fastjson/issues/3266
>
> [enum_custom_serialization | fastjson](https://github.com/alibaba/fastjson/wiki/enum_custom_serialization)