# Seleniums

> [ChromeDriver - WebDriver for Chrome - Downloads](https://chromedriver.chromium.org/downloads)
>
> [Selenium Web Driver自动化测试(java版)系列下半部分 - Selenium](https://www.jianshu.com/nb/28176708)
>
> [科学上网之selenium半自动化处理](http://www.spidercode.cn/show-30.html)
>
> [JAVA执行cmd命令(包含执行批处理文件)_df0128的专栏-CSDN ...](https://blog.csdn.net/df0128/article/details/82776689)

##### Frame

> [Unable to locate element even using any methods in selenium java?](https://sqa.stackexchange.com/questions/27910/unable-to-locate-element-even-using-any-methods-in-selenium-java)

如果存在 Frame 的网页，需要先切换到 Frame 里，再获取 Element。

```java
driver().switchTo().frame("frameName");
driver().switchTo().frame("frameIndex");
```

##### [Selenium爬虫-获取浏览器Network请求和响应- 云+社区- 腾讯云](https://cloud.tencent.com/developer/article/1549872)







