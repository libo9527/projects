## 面经

### 轻喜到家面试

> 宋兵仔

1、ThreadLocal 能实现 synchronized 相同的功能吗？为什么？
2、对象重写 equals() 为什么一定要重写 hashCode()?
3、如何保证接口的幂等性？
4、springboot 如何实现在访问一个方法之前记录主机名等信息？
5、什么情况下事务会失效？
6、线程间通信方式？
7、mybatis 一个 mapper 中可以有重载方法吗？为什么？

### 平安一账通

> 罗存勇 一面

1、项目中mq的使用场景；rocketmq 消费幂等怎么处理；怎么防止消息丢失;消费者怎么定位消息消费；
2、redis数据结构,hash存什么类型的数据；redis集群怎么实现，redis某个节点挂了之后会不会造成不可用，挂了的节点的槽点会不会转移；
3、mysql行锁什么时候会退化为表锁，b+树和红黑树的区别，索引的建立
4、fgc频率太高怎么处理
5、springmvc的请求过程
6、tomcat的异步
7、nginx和tomcat有什么区别
8、mybatis &和#的却区别；preparedstatement怎么防注入

### 平安壹账通

> 宋兵仔

1、分布式事务
2、如何保证接口的幂等性？
2、分布式锁
3、一个接口响应超时如何排查？找到问题
4、一个接口有两个任务，现在需要用两个线程来执行，完成后汇总结果怎么做？
5、阻塞队列实现原理？
6、了解的设计模式？单例模式如何实现？
7、java 中的 aop 原生注解？

### 平安壹账通

> 宋兵仔 2面

  1.项目中是如何保证库存的正确性的？
  2.redis 实现分布式锁的原理说一下？
  3.ression 如何实现集群分布式锁(red lock)的？
  4.redis 持久化 rdb 和 aof？
  5.缓存雪崩 缓存击穿 缓存穿透是指什么？如何解决？
  6.布隆过滤器原理讲一下？
  7.项目中 mq 用到的场景？
  8.为什么要选rocketmq?有哪些特性？
  9.kafka 为什么比 rocketmq 吞吐量高？
  10.说下rocketmq 实现事物消息的原理？
  11.rocketmq 是如何保证消息不丢失的？
  12.你们项目中是如何做服务治理的？
  13.限流算法了解哪些？
  14.令牌桶算法是怎么实现的？
  15.服务熔断 降级 链路追踪怎么做的？

### 平安科技一面：

> 朱尊

算法题1：查找（带权）二叉树中的最大值和最小值。
算法题2：链表A升序排列，链表B降序排列，归并链表A和B，使新链表正序排列。
3.结果是多少？
int i = 1;
i = i++;
int j = i++;
System.out.println(i + j);
4.请从JVM的层面解释为什么是这个结果
5.线程安全的集合类有哪些？
6.copyonwritearraylist和currenthashmap有什么区别？
7.hashmap的数据结构，为什么到8变成红黑树？
8.
SELECT a.id, SUM(b.sal),SUM(b.total)
FROM t_user a LEFT JOIN t_account b ON a.id = b.user_id
WHERE b.sal > 1000
GROUP BY a.id HAVING SUM(b.total) >= 0
ORDER BY a.name DESC
LIMIT 5,10的执行顺序，having和where的区别
9.什么时候会youngGC，垃圾回收算法有哪些？垃圾回收器有哪些？类加载顺序？类加载器有哪些？
10.项目中用过哪些设计模式，IOC用到了哪些设计模式
11.线程池参数和拒绝策略有哪些，为什么不用Executor，什么情况下会导致OOM
12.SpringMVC的流程？如果加了@RequestBody，流程会有什么不同？
13.spring解决循环依赖，什么时候不能解决循环依赖
14.IO类有哪些
15.用过容器吗
16.大数据的工具用过哪些
还有的想不起来了



## 希音

> 谭煜东

线程池核心参数和拒绝策略，改变到期时间策略，jvm fullgc条件，给堆分了4g，为啥查看有8g，gc日志怎么查看，redis大key危害，jedis怎么把key发到对应的机器上，redis规范有哪些，怎么用的，mq消息堆积，kafka，ack参数，再均衡，微服务几个注册中心区别和原理，熔断策略，spring 哪些设计模式，import注解，enable原理，怎么实现starer，mybatus整体源码描述下，八股文就这些，剩下的就是情景题，二面全是情景，sql超时了怎么去思考，优惠券发多了怎么去思考，数据库分库分表无法动态扩容了怎么思考，高并发推送系统设计，然后接口并发很大怎么设计，就记得这些了

还有线程状态，怎么中断，runing下是否可以中断

## 朴朴

> 谭煜东

redis穿透reddsion ，rocketmq事务消息和seata使用场景，redis签到功能的实现，排行榜的实现，mysql mvcc 索引底层原理，设计索引原则，怎么去验证，nginx连接池满了怎么思考，jvm和mysql生产遇到的问题，为啥要用rocketmq不用kafka，记得的就这些

## 腾讯

> 谭煜东

ThreadLocal 在开源框架中是如何使用的？在项目中什么场景下用过？怎么用的？

## 联想

> 雷力华

1、微服务与集群的关系 2、kafak消息原理，为什么快? 3、数据库服务器CPU100%解决方案，4、K8S，docker容器的理解 5、如何保证微服务的高可用的。6微服务之间是如何感知其他的服务不可用的？
7. 怎么实现微服务负载均衡的
redis的主从同步原理，synchronized与ReentrantLock 区别