# JVM

## 内存模型

> [《可能是把 Java 内存区域讲的最清楚的一篇文章》](https://github.com/Snailclimb/JavaGuide/blob/3965c02cc0f294b0bd3580df4868d5e396959e2e/Java相关/可能是把Java内存区域讲的最清楚的一篇文章.md)

## 内存分配与回收策略

> 3.8　实战：内存分配与回收策略 | 《深入理解Java虚拟机 第三版》

1. 对象优先在Eden分配

2. 大对象直接进入老年代

3. 长期存活的对象将进入老年代

4. 动态对象年龄判定规则

   HotSpot虚拟机Survivor空间中，按对象年龄递增排序进行累加，当累加到年龄N的对象时，如果累加和大于Survivor空间的一半，年龄大于或等于N的对象就可以直接进入老年代，无须等到 `-XX：MaxTenuringThreshold` 中要求的年龄。

   动态对象年龄判定规则是在下一次GC时触发的，即第一次GC后，存活对象内存总和超过了Survivor空间的一半，此时并不会触发动态对象年龄判定规则，下次GC后才会触发。

5. 空间分配担保

   Minor GC 后如果对象内存总和超过了Survivor可用空间，则会将**部分**对象移入老年代，**部分**对象保留在Survivor区。

## 垃圾回收器

### ParNew

> 3.5.2 ParNew收集器 | 《深入理解Java虚拟机 第三版》

ParNew收集器实质上是Serial收集器的多线程并行版本，除了同时使用多条线程进行垃圾收集之外，其余的行为包括Serial收集器可用的所有控制参数、收集算法、Stop The World、对象分配规则、回收策略等都与Serial收集器完全一致。

![](http://static.91mszl.com//data/static/images/20200407/1586264404709973934.png)

ParNew 是不少运行在服务端模式下的HotSpot虚拟机，尤其是JDK 7之前的遗留系统中首选的新生代收集器，其中有一个与功能、性能无关但其实很重要的原因是：除了Serial收集器外，目前只有它能与CMS收集器配合工作。

ParNew 收集器是激活CMS后（使用 `-XX:+UseConcMarkSweepGC` 选项）的默认新生代收集器，也可以使用 `-XX:+/-UseParNewGC` 选项来强制指定或者禁用它。

自JDK 9开始，ParNew加CMS收集器的组合就不再是官方推荐的服务端模式下的收集器解决方案了。官方希望它能完全被G1所取代，甚至还取消了ParNew加Serial Old以及Serial加CMS这两组收集器组合的支持（其实原本也很少人这样使用），并直接取消了 `-XX:+UseParNewGC` 参数，这意味着ParNew和CMS从此只能互相搭配使用，再也没有其他收集器能够和它们配合了。

ParNew默认开启的收集线程数与处理器核心数量相同，在处理器核心非常多（譬如32个，现在CPU都是多核加超线程设计，服务器达到或超过32个逻辑核心的情况非常普遍）的环境中，可以使用 `-XX:ParallelGCThreads` 参数来限制垃圾收集的线程数。

### CMS 垃圾回收器

> 3.5.6 CMS收集器 | 《深入理解Java虚拟机 第三版》

CMS（Concurrent Mark Sweep）收集器是一种以获取最短回收停顿时间为目标的收集器。

从名字（包含“Mark Sweep”）上就可以看出CMS收集器是基于标记-清除算法实现的，它的运作过程分为四个步骤，包括：

1. 初始标记（CMS initial mark）
2. 并发标记（CMS concurrent mark）
3. 重新标记（CMS remark）
4. 并发清除（CMS concurrent sweep）

其中初始标记、重新标记这两个步骤仍然需要“Stop The World”。

初始标记仅仅只是标记一下GC Roots能直接关联到的对象，速度很快；

并发标记阶段就是从GC Roots的直接关联对象开始遍历整个对象图的过程，这个过程耗时较长但是不需要停顿用户线程，可以与垃圾收集线程一起并发运行；

而重新标记阶段则是为了修正并发标记期间，因用户程序继续运作而导致标记产生变动的那一部分对象的标记记录（详见3.4.6节中关于增量更新的讲解），这个阶段的停顿时间通常会比初始标记阶段稍长一些，但也远比并发标记阶段的时间短；

最后是并发清除阶段，清理删除掉标记阶段判断的已经死亡的对象，由于不需要移动存活对象，所以这个阶段也是可以与用户线程同时并发的。

![](https://img-blog.csdnimg.cn/20210401162306453.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2ExMDc2MDY3Mjc0,size_16,color_FFFFFF,t_70)

CMS 最主要的优点在名字上已经体现出来：并发收集、低停顿。

CMS 收集器有以下三个明显的缺点：

1. CMS 收集器对处理器资源(CPU)非常敏感。

   在并发阶段，它虽然不会导致用户线程停顿，但却会因为占用了一部分线程（或者说处理器的计算能力）而导致应用程序变慢，降低总吞吐量。

   CMS 默认启动的回收线程数是 `（处理器核心数量+3）/4`，也就是说，如果处理器核心数在四个或以上，并发回收时垃圾收集线程只占用不超过25%的处理器运算资源，并且会随着处理器核心数量的增加而下降。但是当处理器核心数量不足四个时，CMS对用户程序的影响就可能变得很大。如果应用本来的处理器负载就很高，还要分出一半的运算能力去执行收集器线程，就可能导致用户程序的执行速度忽然大幅降低。

2. 由于CMS收集器无法处理“**浮动垃圾**”（Floating Garbage），有可能出现“Con-current Mode Failure”失败进而导致另一次完全“Stop The World”的Full GC的产生。

   在CMS的并发标记和并发清理阶段，用户线程是还在继续运行的，程序在运行自然就还会伴随有新的垃圾对象不断产生，但这一部分垃圾对象是出现在标记过程结束以后，CMS无法在当次收集中处理掉它们，只好留待下一次垃圾收集时再清理掉。这一部分垃圾就称为“浮动垃圾”。

   同样也是由于在垃圾收集阶段用户线程还需要持续运行，那就还需要预留足够内存空间提供给用户线程使用，因此CMS收集器不能像其他收集器那样等待到老年代几乎完全被填满了再进行收集，必须预留一部分空间供并发收集时的程序运作使用。

   在JDK5的默认设置下，CMS收集器当老年代使用了68%的空间后就会被激活，这是一个偏保守的设置，如果在实际应用中老年代增长并不是太快，可以适当调高参数 `-XX：CMSInitiatingOccu-pancyFraction` 的值来提高CMS的触发百分比，降低内存回收频率，获取更好的性能。到了JDK 6时，CMS收集器的启动阈值就已经默认提升至92%。但这又会更容易面临另一种风险：要是CMS运行期间预留的内存无法满足程序分配新对象的需要，就会出现一次“**并发失败**”（Concurrent Mode Failure），这时候虚拟机将不得不启动后备预案：冻结用户线程的执行，临时启用Serial Old收集器来重新进行老年代的垃圾收集，但这样停顿时间就很长了。所以参数 `-XX：CMSInitiatingOccupancyFraction` 设置得太高将会很容易导致大量的并发失败产生，性能反而降低，用户应在生产环境中根据实际应用情况来权衡设置。

3. CMS是一款基于“标记-清除”算法实现的收集器，这意味着收集结束时会有大量空间碎片产生。

   空间碎片过多时，将会给大对象分配带来很大麻烦，往往会出现老年代还有很多剩余空间，但就是无法找到足够大的连续空间来分配当前对象，而不得不提前触发一次Full GC的情况。为了解决这个问题，CMS收集器提供了一个 `-XX：+UseCMS-CompactAtFullCollection` 开关参数（默认是开启的，此参数从JDK 9开始废弃），用于在CMS收集器不得不进行Full GC时开启内存碎片的合并整理过程，由于这个内存整理必须移动存活对象，（在Shenandoah和ZGC出现前）是无法并发的。这样空间碎片问题是解决了，但停顿时间又会变长，因此虚拟机设计者们还提供了另外一个参数 `-XX：MSFullGCsBeforeCompaction`（此参数从JDK 9开始废弃），这个参数的作用是要求CMS收集器在执行过若干次（数量由参数值决定）不整理空间的Full GC之后，下一次进入Full GC前会先进行碎片整理（默认值为0，表示每次进入Full GC时都进行碎片整理）。

`-XX:+CMSParallelInitialMarkEnabled`：这个参数会在CMS垃圾回收器的“初始标记”阶段开启多线程并发执行。初始标记阶段，是存在STW的，开启多线程并发之后，可以尽可能优化这个阶段的性能，减少Stop the World的时间。

`-XX:+CMSScavengeBeforeRemark`：这个参数会在CMS的重新标记阶段之前，先尽量执行一次Young GC。CMS的重新标记也是会Stop the World的，所以如果在重新标记之前，先执行一次Young GC，就会回收掉一 些年轻代里没有人引用的对象，那么在重新标记阶段就可以少扫描一些对象，此时就可以提升CMS的重新标记阶段的性能，减少耗时。

### G1 收集器

> 3.5.7 Garbage First收集器 |《深入理解Java虚拟机 第三版》

Garbage First（简称G1）收集器是垃圾收集器技术发展历史上的里程碑式的成果，它开创了收集器面向局部收集的设计思路和基于Region的内存布局形式。

G1是一款主要面向服务端应用的垃圾收集器。JDK 9发布之日，G1宣告取代Parallel Scavenge加Parallel Old组合，成为服务端模式下的默认垃圾收集器。

停顿时间模型：能够支持指定在一个长度为M毫秒的时间片段内，消耗在垃圾收集上的时间大概率不超过N毫秒这样的目标。

在G1收集器出现之前的所有其他收集器，垃圾收集的目标范围要么是整个新生代（Minor GC），要么就是整个老年代（Major GC），再要么就是整个Java堆（Full GC）。而G1跳出了这个樊笼，它可以面向堆内存任何部分来组成回收集（Collection Set，一般简称CSet）进行回收，衡量标准不再是它属于哪个分代，而
是哪块内存中存放的垃圾数量最多，回收收益最大，这就是G1收集器的Mixed GC模式。G1开创的基于Region的堆内存布局是它能够实现这个目标的关键。

虽然G1也仍是遵循分代收集理论设计的，但其堆内存的布局与其他收集器有非常明显的差异：G1不再坚持固定大小以及固定数量的分代区域划分，而是把连续的Java堆划分为多个大小相等的独立区域（Region），每一个Region都可以根据需要，扮演新生代的Eden空间、Survivor空间，或者老年代空间。收集器能够对扮演不同角色的Region采用不同的策略去处理。

Region中还有一类特殊的Humongous区域，专门用来存储大对象。G1认为只要大小超过了一个Region容量一半的对象即可判定为大对象。每个Region的大小可以通过参数 `-XX：G1HeapRegionSize` 设定，取值范围为1MB～32MB，且应为2的N次幂。而对于那些超过了整个Region容量的超级大对象，将会被存放在N个连续的Humongous Region之中，G1的大多数行为都把Humongous Region作为老年代的一部分来进行看待。

虽然G1仍然保留新生代和老年代的概念，但新生代和老年代不再是固定的了，它们都是一系列区域（不需要连续）的动态集合。G1收集器之所以能建立可预测的停顿时间模型，是因为它将Region作为单次回收的最小单元，即每次收集到的内存空间都是Region大小的整数倍，这样可以有计划地避免在整个Java堆中进行全区域的垃圾收集。更具体的处理思路是让G1收集器去跟踪各个Region里面的垃圾堆积的“价值”大小，价值即回收所获得的空间大小以及回收所需时间的经验值，然后在后台维护一个优先级列表，每次根据用户设定允许的收集停顿时间（使用参数 `-XX：MaxGCPauseMillis` 指定，默认值是200毫秒），优先处理回收价值收益最大的那些Region，这也就是“Garbage First”名字的由来。

G1收集器不足：

1. 更高的内存占用负担。

   将Java堆分成多个独立Region后，Region里面存在的跨Region引用对象如何解决？使用记忆集避免全堆作为GC Roots扫描。同时由于Region数量比传统收集器的分代数量明显要多得多，因此G1收集器要比其他的传统垃圾收集器有着更高的内存占用负担。根据经验，G1至少要耗费大约相当于Java堆容量10%至20%的额外内存来维持收集器工作。

G1收集器的运作过程大致可划分为以下四个步骤：

1. 初始标记（Initial Marking）：

   仅仅只是标记一下GC Roots能直接关联到的对象，并且修改TAMS指针的值。这个阶段需要停顿线程，但耗时很短。

2. 并发标记（Concurrent Marking）：

   从GC Root开始对堆中对象进行可达性分析，递归扫描整个堆里的对象图，找出要回收的对象，这阶段耗时较长，但可与用户程序并发执行。当对象图扫描完成以后，还要重新处理SATB记录下的在并发时有引用变动的对象。

3. 最终标记（Final Marking）：

   对用户线程做另一个短暂的暂停，用于处理并发阶段结束后仍遗留下来的最后那少量的SATB记录。

4. 筛选回收（Live Data Counting and Evacuation）：

   负责更新Region的统计数据，对各个Region的回收价值和成本进行排序，根据用户所期望的停顿时间来制定回收计划，可以自由选择任意多个Region构成回收集，然后把决定回收的那一部分Region的存活对象复制到空的Region中，再清理掉整个旧Region的全部空间。这里的操作涉及存活对象的移动，是必须暂停用户线程，由多条收集器线程并行完成的。

从上述阶段的描述可以看出，G1收集器除了并发标记外，其余阶段也是要完全暂停用户线程的。

![](https://img2018.cnblogs.com/blog/764719/201910/764719-20191024140138680-334445497.png)

G1收集器默认的停顿目标为两百毫秒，如果我们把停顿时间调得非常低，会导致每次选出来的回收集只占堆内存很小的一部分，收集器收集的速度逐渐跟不上分配器分配的速度，导致垃圾慢慢堆积。最终占满堆引发Full GC反而降低性能，所以通常把期望停顿时间设置为一两百毫秒或者两三百毫秒会是比较合理的。

从G1开始，最先进的垃圾收集器的设计导向都不约而同地变为追求能够应付应用的内存分配速率（Allocation Rate），而不追求一次把整个Java堆全部清理干净。这样，应用在分配，同时收集器在收集，只要收集的速度能跟得上对象分配的速度，那一切就能运作得很完美。这种新的收集器设计思路从工程实现上看是从G1开始兴起的，所以说G1是收集器技术发展的一个里程碑。

相比CMS，G1的优点有很多：

1. 可以指定最大停顿时间、

2. 分Region的内存布局、

3. 按收益动态确定回收集

4. 不会产生内存空间碎片

   与CMS的“标记-清除”算法不同，G1从整体来看是基于“标记-整理”算法实现的收集器，但从局部（两个Region之间）上看又是基于“标记-复制”算法实现。

不过比起CMS，G1的弱项也可以列举出不少：

1. 内存占用高

   虽然G1和CMS都使用卡表来处理跨代指针，但G1的卡表实现更为复杂，而且堆中每个Region都必须有一份卡表，这导致G1的记忆集可能会占整个堆容量的20%乃至更多的内存空间；相比起来CMS的卡表就相当简单，只有唯一一份，而且只需要处理老年代到新生代的引用，反过来则不需要。

2. 额外执行负载高

   CMS和G1都使用到写屏障，CMS用写后屏障来更新维护卡表；而G1除了使用写后屏障来进行同样的卡表维护操作外，为了实现原始快照搜索（SATB）算法，还需要使用写前屏障来跟踪并发时的指针变化情况。相比起增量更新算法，原始快照搜索能够减少并发标记和重新标记阶段的消耗，避免CMS那样在最终标记阶段停顿时间过长的缺点，但是在用户程序运行过程中确实会产生由跟踪引用变化带来的额外负担。由于G1对写屏障的复杂操作要比CMS消耗更多的运算资源，所以CMS的写屏障实现是直接的同步操作，而G1就不得不将其实现为类似于消息队列的结构，把写前屏障和写后屏障中要做的事情都放到队列里，然后再异步处理。



## 虚拟机常用参数表

| 参数                              | 含义                                                         | 默认值                                                       | 示例                                 |
| --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------ |
| -Xms(1.8 -XX:InitialHeapSize)     | 堆内存初始大小，也是最小值。                                 |                                                              | -Xms3072M/-XX:InitialHeapSize=3072M  |
| -Xmx(1.8 -XX:MaxHeapSize)         | 堆内存最大值，一般结合 Xms 一期使用，一般两个设置为相同值，即堆内存不可扩展 |                                                              | -Xmx3072M/-XX:MaxHeapSize=3072M      |
| -Xmn(1.8 -XX:NewSize)             | 年轻代内存大小                                               |                                                              | -Xmn2048M/-XX:NewSize=2048M          |
| -XX:MaxNewSize                    | JDK1.8 年轻代内存最大值                                      |                                                              | -XX:MaxNewSize=10M                   |
| -Xss(1.8 -XX:ThreadStackSize)     | 单个线程的虚拟机栈的内存                                     |                                                              | -Xss1M/-XX:ThreadStackSize=1M        |
| -XX:PermSize                      | 永久代初始内存，最小内存                                     |                                                              | -XX:PermSize=256M                    |
| -XX:MaxPermSize                   | 永久代最大内存，同对内存一样，也是结合永久代初始内存参数一起使用，且两值相等，不可扩展。 |                                                              | -XX:MaxPermSize=256M                 |
| -XX:MetaspaceSize                 | 原空间初始容量                                               |                                                              | -XX:MetaspaceSize=256M               |
| -XX:MaxMetaspaceSize              | 原空间最大容量                                               |                                                              | -XX:MaxMetaspaceSize=256M            |
| -XX:SurvivorRatio                 | Eden 区内存与 Survivor 区内存的比例                          | 8                                                            | -XX:SurvivorRatio=8                  |
| -XX:MaxTenuringThreshold          | 最大生存年龄，超过这个年龄就移入老年代                       | 15                                                           | -XX:MaxTenuringThreshold=5           |
| -XX:ParallelGCThreads             | ParNew 收集器并发线程数                                      | 与处理器核心数量相同                                         | -XX:ParallelGCThreads=4              |
| -XX:PretenureSizeThreshold        | 大对象下限，超过这个值的大对象直接进入老年代                 |                                                              | -XX:PretenureSizeThreshold=1M        |
| -XX:+UseParNewGC                  | 使用 Par New 收集器对年轻代进行垃圾收集                      |                                                              | -XX:+UseParNewGC                     |
| -XX:+UseConcMarkSweepGC           | 使用 CMS 收集器对老年代进行垃圾收集                          |                                                              | -XX:+UseConcMarkSweepGC              |
| -XX:CMSInitiatingOccupancyFaction | CMS 垃圾收集器，老年代触发 Full GC 时垃圾对象总和占比        | 92                                                           | -XX:CMSInitiatingOccupancyFaction=92 |
| -XX:CMSFullGCsBeforeCompaction    | CMS 垃圾收集器，多少次执行一次内存整理                       |                                                              | -XX:CMSFullGCsBeforeCompaction=0     |
| -XX:MaxGCPauseMillis              | G1收集器，最大停顿时间                                       | 200                                                          | -XX:MaxGCPauseMillis=200             |
| -XX:G1HeapRegionSize              | G1收集器每个Region的大小                                     | 用堆大小除以2048<br />JVM最多可以有2048个Region<br />取值范围为1MB～32MB，且应为2的N次幂 | -XX:G1HeapRegionSize=2M              |
| -XX:G1NewSizePercent              | 新生代初始占比                                               | 5                                                            |                                      |
| -XX:G1MaxNewSizePercent           | 新生代最大占比                                               | 60                                                           |                                      |
| -XX:+PrintGCDetils                | 打印详细的gc日志                                             |                                                              | -XX:+PrintGCDetils                   |
| -XX:+PrintGCTimeStamps            | 打印出来每次GC发生的时间                                     |                                                              | -XX:+PrintGCTimeStamps               |
| -Xloggc                           | 将gc日志写入一个磁盘文件                                     |                                                              | -Xloggc:gc.log                       |
| -XX:DisableExplicitGC             | 禁止显式执行GC，不允许你来通过代码 `System.gc()` 触发GC。    |                                                              | -XX:+DisableExplicitGC               |
| -XX:HeapDumpOnOutOfMemoryError    | 系统 OOM 时 dump 堆内存快照                                  |                                                              | -XX:+HeapDumpOnOutOfMemoryError      |
| -XX:HeapDumpPath                  | 堆内存快照存放地址                                           |                                                              | -XX:HeapDumpPath=/usr/local/app/oom  |



## 虚拟机性能监控、故障处理工具

### jps

> 4.2.1 jps：虚拟机进程状况工具 |《深入理解Java虚拟机 第三版》

| 选项 | 作用                                                 |
| ---- | ---------------------------------------------------- |
| -q   | 只输入出 LVMId，省略主类的名称                       |
| -m   | 输出虚拟机进程启动时传递给主类 main() 函数的参数     |
| -l   | 输出主类的全名，如果进程执行的是JAR包，则输出JAR路径 |
| -v   | 输出虚拟机进程启动时的JVM参数                        |

```bat
C:\Program Files\Java\jdk1.8.0_281\bin>jps -l
24100 org.jetbrains.jps.cmdline.Launcher
18760 com.gzhennaxia.bill.BillApplication
22892 sun.tools.jps.Jps
```



### jstat

> 4.2.2 jstat：虚拟机统计信息监视工具 |《深入理解Java虚拟机 第三版》
>
> jstat（JVM Statistics Monitoring Tool）

jstat命令格式为：`jstat [ option vmid [interval[s|ms] [count]] ]`

e.g. 每 250 毫秒查询一次进程 2764 垃圾收集状况，一共查询20次，命令：`jstat -gc 2764 250 20`。

![](https://programmer.help/images/blog/2053b38b7307ea8a791c4ba44a194c08.jpg)

```bat
C:\Program Files\Java\jdk1.8.0_281\bin>jstat -gc 18760
 S0C    S1C    S0U    S1U      EC       EU        OC         OU       MC     MU    CCSC   CCSU   YGC     YGCT    FGC    FGCT     GCT
13312.0 512.0   0.0   224.0  626688.0 24491.6   59904.0    22543.7   46328.0 43150.0 6400.0 5789.2     17    0.108   2      0.065    0.173
```

- S0C：这是From Survivor区的大小
- S1C：这是To Survivor区的大小
- S0U：这是From Survivor区当前使用的内存大小
- S1U：这是To Survivor区当前使用的内存大小
- EC：这是Eden区的大小
- EU：这是Eden区当前使用的内存大小
- OC：这是老年代的大小
- OU：这是老年代当前使用的内存大小
- MC：这是方法区（永久代、元数据区）的大小
- MU：这是方法区（永久代、元数据区）的当前使用的内存大小
- YGC：这是系统运行迄今为止的Young GC次数
- YGCT：这是Young GC的耗时
- FGC：这是系统运行迄今为止的Full GC次数
- FGCT：这是Full GC的耗时

### jmap

> 4.2.4 jmap：Java内存映像工具 |《深入理解Java虚拟机 第三版》
>
> jmap（Memory Map for Java）

jmap命令格式：`jmap [ option ] pid`

![](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/6998e7b5799c4224b2d9aa664b6edfde~tplv-k3u1fbpfcp-watermark.image)

e.g. `jmap -histo PID`

```bat
C:\Program Files\Java\jdk1.8.0_281\bin>jmap -histo 18760

 num     #instances         #bytes  class name
----------------------------------------------
   1:        553214       97308904  [C
   2:        336622        8078928  java.lang.String
   3:         10309        4049568  [I
   4:         56974        2278960  java.util.LinkedHashMap$Entry
   5:         25435        2238280  java.lang.reflect.Method
   6:         45650        1826000  org.springframework.boot.devtools.filewatch.FileSnapshot
   7:         52796        1689472  java.io.File
   8:         18977        1253960  [Ljava.lang.String;
   9:          8031        1252208  [Ljava.util.HashMap$Node;
  10:         21034        1124136  [Ljava.lang.Object;
  11:          9349        1046280  java.lang.Class
```

该命令会按照各种对象占用内存空间的大小降序排列。

e.g. 2：`jmap -dump:live,format=b,file=dump.hprof PID`

这个命令会在当前目录下生成一个二进制格式的dump.hrpof文件。可以使用jhat去分析堆快照，jhat内置了web服务器，支持通过浏览器来以图形化的方式分析堆转储快照。

### jhat

> 4.2.5 jhat：虚拟机堆转储快照分析工具 |《深入理解Java虚拟机 第三版》
>
> jhat（JVM Heap Analysis Tool）
>
> 在实际工作中，除非手上真的没有别的工具可用，否则多数人是不会直接使用jhat命令 来分析堆转储快照文件的。

使用如下命令即可启动jhat服务器，还可以指定自己想要的http端口号，默认是7000端口号：`jhat dump.hprof -port 7000`

### MAT

> 可视化堆快照分析工具

## 锁优化

> 13.3　锁优化 |《深入理解Java虚拟机 第三版》

在 Java 早期版本中，`synchronized` 属于 **重量级锁**，效率低下。

`synchronized` 同步语句块的实现使用的是 `monitorenter` 和 `monitorexit` 指令。

`synchronized` 修饰的方法并没有 `monitorenter` 指令和 `monitorexit` 指令，取而代之的是 `ACC_SYNCHRONIZED` 标识，该标识指明了该方法是一个同步方法。

**不过两者的本质都是对对象监视器 monitor 的获取。**而监视器是依赖于底层的操作系统的 `Mutex Lock` 来实现的，Java 的线程是映射到操作系统的原生线程之上的。如果要挂起或者唤醒一个线程，都需要操作系统帮忙完成，而操作系统实现线程之间的切换时需要从用户态转换到内核态，这个状态之间的转换需要相对比较长的时间，时间成本相对较高。

高效并发是从JDK 5升级到JDK 6后一项重要的改进项，HotSpot虚拟机开发团队在这个版本上花费了大量的资源去实现各种锁优化技术，如

- 适应性自旋（Adaptive Spinning）
- 锁消除（Lock Elimination）
- 锁膨胀（Lock Coarsening）
- 轻量级锁（Lightweight Locking）
- 偏向锁（Biased Locking）

### 自旋锁与自适应自旋

挂起线程和恢复线程的操作都需要转入内核态中完成，这些操作给Java虚拟机的并发性能带来了很大的压力。但在许多应用上，共享数据的锁定状态只会持续很短的一段时间，为了这段时间去挂起和恢复线程并不值得。如果物理机器有多个处理器核心，则可以使得多个线程同时并行执行，就可以让后面请求锁的那个线程“稍等一会”，但不放弃处理器的执行时间，看看持有锁的线程是否很快就会释放锁。为了让线程等待，我们只须让线程执行一个忙循环（自旋），这项技术就是所谓的自旋锁。

自旋锁可以使用 `-XX:+UseSpinning` 参数来开启，在JDK 6中就已经改为默认开启了。

自旋等待不能代替阻塞，且先不说对处理器数量的要求，自旋等待本身虽然避免了线程切换的开销，但它是要占用处理器时间的，所以如果锁被占用的时间很
短，自旋等待的效果就会非常好，反之如果锁被占用的时间很长，那么自旋的线程只会白白消耗处理器资源，而不会做任何有价值的工作，这就会带来性能的浪费。因此自旋等待的时间必须有一定的限度，如果自旋超过了限定的次数仍然没有成功获得锁，就应当使用传统的方式去挂起线程。自旋次数的默认值是十次，用户也可以使用参数 `-XX:PreBlockSpin` 来自行更改。

JDK 6中对自旋锁的优化，引入了自适应的自旋。自适应意味着自旋的时间不再是固定的了，而是由前一次在同一个锁上的自旋时间及锁的拥有者的状态来决定的。有了自适应自旋，随着程序运行时间的增长及性能监控信息的不断完善，虚拟机对程序锁的状况预测就会越来越精准，虚拟机就会变得越来越“聪明”了。

### 锁消除

锁消除是指虚拟机即时编译器在运行时，对一些代码要求同步，但是对被检测到不可能存在共享数据竞争的锁进行消除。锁消除的主要判定依据来源于逃逸分析的数据支持，如果判断到一段代码中，在堆上的所有数据都不会逃逸出去被其他线程访问到，那就可以把它们当作栈上数据对待，认为它们是线程私有的，同步加锁自然就无须再进行。

也许读者会有疑问，变量是否逃逸，对于虚拟机来说是需要使用复杂的过程间分析才能确定的，但是程序员自己应该是很清楚的，怎么会在明知道不存在数据争用的情况下还要求同步呢？这个问题的答案是：有许多同步措施并不是程序员自己加入的，同步的代码在Java程序中出现的频繁程度也许超过了大部分读者的想象。

```java
public String concatString(String s1, String s2, String s3) {
 return s1 + s2 + s3;
}
```

Javac编译器会对String连接做自动优化。在JDK 5之前，字符串加法会转化为StringBuffer对象的连续append()操作，在JDK 5及以后的版本中，会转化为StringBuilder对象的连续append()操作。即上述代码可能会变成下面的样子

```java
public String concatString(String s1, String s2, String s3) {
 StringBuffer sb = new StringBuffer();
 sb.append(s1);
 sb.append(s2);
 sb.append(s3);
 return sb.toString();
}
```

每个StringBuffer.append()方法中都有一个同步块，锁就是sb对象。虚拟机观察变量sb，经过逃逸分析后会发现它的动态作用域被限制在concatString()方法内部。也就是sb的所有引用都永远不会逃逸到concatString()方法之外，其他线程无法访问到它，所以这里虽然有锁，但是可以被安全地消除掉。在解释执行时这里仍然会加锁，但在经过服务端编译器的即时编译之后，这段代码就会忽略所有的同步措施而直接执行。

### 锁粗化

为了等待锁的线程能尽可能快地拿到锁，所以一般总数将同步块的作用范围限制得尽量小——只在共享数据的实际作用域中才进行同步。但是如果一系列的连续操作都对同一个对象反复加锁和解锁，甚至加锁操作是出现在循环体之中的，那频繁地进行互斥同步操作会导致不必要的性能损耗。

例如上面锁消除代码中连续的append()方法就属于这类情况。如果虚拟机探测到有这样一串零碎的操作都对同一个对象加锁，将会把加锁同步的范围扩展（粗化）到整个操作序列的外部，就是扩展到第一个append()操作之前直至最后一个append()操作之后，这样只需要加锁一次就可以了。

### 轻量级锁

轻量级锁中的“轻量级”是相对于使用操作系统互斥量来实现的传统锁而言的，因此传统的锁机制就被称为“重量级”锁。

轻量级锁设计的初衷是在没有多线程竞争的前提下，减少传统的重量级锁使用操作系统互斥量产生的性能消耗。

要理解轻量级锁、偏向锁的原理和运作过程，必须要对HotSpot虚拟机对象的内存布局（尤其是对象头部分）有所了解。HotSpot虚拟机的对象头（Object Header）分为两部分，第一部分用于存储对象自身的运行时数据，如哈希码（HashCode）、GC分代年龄（Generational GC Age）等。官方称它为“Mark
Word”。这部分是实现轻量级锁和偏向锁的关键。另外一部分用于存储指向方法区对象类型数据的指针，如果是数组对象，还会有一个额外的部分用于存储数组长度。

Mark Word被设计成一个非固定的动态数据结构，以便在极小的空间内存储尽量多的信息。它会根据对象的状态复用自己的存储空间。

![](https://img-blog.csdnimg.cn/20200619123714116.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM2NDM0NzQy,size_16,color_FFFFFF,t_70)

#### 轻量级锁的加锁过程

1. 在代码即将进入同步块的时候，如果此同步对象没有被锁定，虚拟机首先将在当前线程的栈帧中建立一个锁记录（Lock Record），用于存储锁对象目前的Mark Word的拷贝（Displaced Mark Word）。

![](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tbWJpei5xcGljLmNuL21tYml6L3NyRmdHTjNreGJHYTNJd1luUnI0V1VNd3l3TkxqZnpQYVZ2bFNOM09meVRrbWJyQ0pDQ3F1OXV6V0p4M3dndGJyaWJpYmREc0hFSHhhekg3YTI3clVDUGcvNjQw?x-oss-process=image/format,png)

2. 然后，虚拟机将使用CAS操作尝试把对象的Mark Word更新为指向Lock Record的指针。更新成功，则代表该线程拥有了这个对象的锁，对象的锁标志位将转变为“00”，表示此对象处于轻量级锁定状态。

![](https://images2015.cnblogs.com/blog/820406/201604/820406-20160424105540163-1019388398.png)

3. 如果更新失败了，那就意味着至少存在一条线程与当前线程竞争获取该对象的锁。虚拟机首先会检查对象的Mark Word是否指向当前线程的栈帧，如果是，说明当前线程已经拥有了这个对象的锁，那直接进入同步块继续执行就可以了，否则就说明这个锁对象已经被其他线程抢占了。如果出现两条以上的线程争用同一个锁的情况，那轻量级锁就不再有效，必须要膨胀为重量级锁，锁标志的状态值变为“10”，此时Mark Word中存储的就是指向重量级锁（互斥量）的指针，后面等待锁的线程也必须进入阻塞状态。

#### 轻量级锁的解锁过程

解锁过程也同样是通过CAS操作来进行的，如果对象的Mark Word仍然指向线程的锁记录，那就用CAS操作把对象当前的Mark Word和线程中复制的Displaced Mark Word替换回来。假如能够成功替换，那整个同步过程就顺利完成了；如果替换失败，则说明有其他线程尝试过获取该锁，就要在释放锁的同时，唤醒被挂起的线程。

#### 轻量级锁能提升程序同步性能的依据

轻量级锁能提升程序同步性能的依据是“对于绝大部分的锁，在整个同步周期内都是不存在竞争的”这一经验法则。如果没有竞争，轻量级锁便通过CAS操作成功避免了使用互斥量的开销；但如果确实存在锁竞争，除了互斥量的本身开销外，还额外发生了CAS操作的开销。因此在有竞争的情况下，轻量级锁反而会比传统的重量级锁更慢。

### 偏向锁

偏向锁的目的是消除数据在无竞争情况下的同步原语，进一步提高程序的运行性能。如果说轻量级锁是在无竞争的情况下使用CAS操作去消除同步使用的互
斥量，那偏向锁就是在无竞争的情况下把整个同步都消除掉，连CAS操作都不去做了。

偏向锁中的“偏”，就是偏心的“偏”、偏袒的“偏”。它的意思是这个锁会偏向于第一个获得它的线程，如果在接下来的执行过程中，该锁一直没有被其他的线程获取，则持有偏向锁的线程将永远不需要再进行同步。

启用参数：`-XX:+UseBiasedLocking`，默认开启。

#### 偏向锁的原理

当锁对象第一次被线程获取的时候，虚拟机将会把对象头中的标志位设置为“01”、把偏向模式设置为“1”，表示进入偏向模式。同时使用CAS操作把获取到这个锁的线程的ID记录在对象的Mark Word之中。如果CAS操作成功，持有偏向锁的线程以后每次进入这个锁相关的同步块时，虚拟机都可以不再进行任何同步操作（例如加锁、解锁及对Mark Word的更新操作等）。

一旦出现另外一个线程去尝试获取这个锁的情况，偏向模式就马上宣告结束。根据锁对象目前是否处于被锁定的状态决定是否撤销偏向（偏向模式设置为“0”），撤销后标志位恢复到未锁定（标志位为“01”）或轻量级锁定（标志位为“00”）的状态，后续的同步操作就按照上面介绍的轻量级锁那样去执行。

#### 偏向状态的对象，被线程ID占用的原有哈希码怎么办？

在Java语言里面一个对象如果计算过哈希码，就应该一直保持该值不变（强烈推荐但不强制，因为用户可以重载hashCode()方法按自己的意愿返回哈希码），否则很多依赖对象哈希码的API都可能存在出错风险。`Object::hashCode()` 方法返回的是对象的一致性哈希码，这个值是能强制保证不变的。因此，**当一个对象已经计算过一致性哈希码后，它就再也无法进入偏向锁状态了**；而当一个对象当前正处于偏向锁状态，又收到需要计算其一致性哈希码请求时（这里说的计算请求应来自于对Object::hashCode()或者System::identityHashCode(Object)方法的调用，如果重写了对象的hashCode()方法，计算哈希码时并不会产生这里所说的请求），它的偏向状态会被立即撤销，并且锁会膨胀为重量级锁。在重量级锁的实现中，对象头指向了重量级锁的位置，代表重量级锁的ObjectMonitor类里有字段可以记录非加锁状态（标志位为“01”）下的Mark Word，其中自然可以存储原来的哈希码。

偏向锁可以提高带有同步但无竞争的程序性能，但它并非总是对程序运行有利。如果程序中大多数的锁都总是被多个不同的线程访问，那偏向模式就是多余的。在具体问题具体分析的前提下，有时候使用参数 `-XX:-UseBiasedLocking` 来禁止偏向锁优化反而可以提升性能。

### 偏向锁、轻量级锁的状态转化及对象Mark Word的关系图

![](https://www.linuxidc.com/upload/2018_02/180206215372339.jpg)

## Q&A

### JVM 优化原则

JVM优化的目标时降低 Full GC 频率，Full GC优化的前提是Minor GC的优化，Minor GC的优化的前提是合理分配内存空间，合理分配内存空间的前提是对系统运行期间的内存使用模型进行预估。

其实对很多普通的Java系统而言，只要对系统运行期间的内存使用模型做好预估，然后分配好合理的内存空间，尽量让Minor GC之后的存活对象留在Survivor里不要去老年代，然后其余的GC参数不做太多优化，系统性能基本上就不会太差。

### 为啥老年代的Full GC要比新生代的Minor GC慢很多倍，一般在10倍以上？

新生代执行速度其实很快，因为直接从GC Roots出发就追踪哪些对象是活的就行了，新生代存活对象是很少的，这个速度是极快的， 不需要追踪多少对象。 然后直接把存活对象放入Survivor中，就一次性直接回收Eden和之前使用的Survivor了。

但是 Full GC，在并发标记阶段，他需要去追踪所有存活对象，老年代存活对象很多，这个过程就会很慢； 其次并发清理阶段，他不是一次性回收一大片内存，而是找到零零散散在各个地方的垃圾对象，速度也很慢； 最后还得执行一次内存碎片整理，把大量的存活对象给挪在一起，空出来连续内存空间，这个过程还得“Stop the World”，那就更慢了。 另外在并发清理期间，剩余内存空间不足以存放要进入老年代的对象了，引发了“Concurrent Mode Failure”问题，还得用“Serial Old”垃圾回收器，“Stop the World”之后慢慢重新来一遍回收的过程，这更是耗时了。 所以综上所述，老年代的垃圾回收，就是一个字：慢

### 触发 Minor GC 的时机

当年轻代的 Eden 区满了时触发。

### 对象进入老年代的情况有哪几种？

1. 超过一定年龄(默认15)后进入老年代
2. 大对象直接进入老年代
3. Young GC过后存活对象太多了，导致Survivor区域放不下了，这批对象会进入老年代
4. 动态年龄判定规则：几次Young GC过后，Surviovr区域中的对象占用了超过50%的内存，此时会判断如果年龄1+年龄2+年龄N的对象总和超过 了Survivor区域的50%，此时年龄N以及之上的对象都进入老年代

### 触发 Full GC 的时机

1. 老年代可用内存小于新生代全部对象的大小，如果没开启空间担保参数（`-XX： HandlePromotionFailure`），会直接触发Full GC，所以一般空间担保参数都会打开；

   在JDK 6 Update 24之后，`-XX：HandlePromotionFailure` 参数不会再影响到虚拟机的空间分配担保策略，虽然OpenJDK源码中还定义了 `-XX：HandlePromotionFailure` 参数，但是在实际虚拟机中已经不会再使用它。JDK 6 Update 24之后的规则变为只要老年代的连续空间大于新生代对象总大小或者历次晋升的平均大小，就会进行Minor GC，否则将进行Full GC。——《深入理解Java虚拟机 3.8.5 空间分配担保》

2. 老年代可用内存小于历次新生代GC后进入老年代的平均对象大小，此时会提前Full GC；

3. 新生代Minor GC后的存活对象大于Survivor，那么就会进入老年代，此时如果老年代内存不足，就会触发 Full GC。

4. CMS 收集器中，如果老年代已经使用的内存空间超过了参数`-XX:CMSInitiatingOccupancyFaction` 指定的比例（默认92%），也会自动触发Full GC。

频繁触发 Full GC 的原因，一般可能性有4个：

1. 内存分配不合理，导致对象频繁进入老年代，进而引发频繁的Full GC；
2. 存在内存泄漏等问题，内存里驻留了大量的对象塞满了老年代，导致稍微有一些对象进入老年代就会引发Full GC；
3. 永久代里的类太多，触发了Full GC
4. 错误调用 System.gc() 触发 Full GC

### 如何保证只做 ygc，jvm 参数如何配置？

- 加大分代年龄，比如默认15加到30;
- 修改新生代老年代比例，比如新生代老年代比例改成2:1
- 修改e区和s区比例，比如改成6:2:2

其实要做到仅仅young gc，而几乎没有full gc是不难的，只要结合自己系统的运行，根据他的内存占用情况，GC后的对象存活情况， 合理分配Eden、Survivor、老年代的内存大小，合理设置一些参数，即可做到。

### JDK 8 默认收集器是什么？

jdk8环境下，默认使用 Parallel Scavenge（新生代）+ Parallel Old（老年代）

-XX:+PrintCommandLineFlags 参数可查看默认设置收集器类型
-XX:+PrintGCDetails 也可通过打印的GC日志的新生代、老年代名称判断

### SoftReference对象到底在GC的时候要不要回收是通过什么公式来判断的呢？

公式：`clock - timestamp <= freespace * SoftRefLRUPolicyMSPerMB`。 `clock - timestamp` 代表了一个软引用对象多久没被访问过了，freespace代表JVM中的空闲内存空间，SoftRefLRUPolicyMSPerMB代表每一MB空闲内存空间可以允许SoftReference对象存活多久。

### 谈谈 synchronized 和 ReentrantLock 的区别

1. 两者都是可重入锁

2. synchronized 依赖于 JVM 而 ReentrantLock 依赖于 JDK API

3. ReentrantLock 比 synchronized 增加了一些高级功能

   相比`synchronized`，`ReentrantLock`增加了一些高级功能。主要有三点：

   - **等待可中断**：`ReentrantLock`提供了一种能够中断等待锁的线程的机制，通过 `lock.lockInterruptibly()` 来实现这个机制。也就是说正在等待的线程可以选择放弃等待，改为处理其他事情。
   - **可实现公平锁**：`ReentrantLock`可以指定是公平锁还是非公平锁。而`synchronized`只能是非公平锁。所谓的公平锁就是先等待的线程先获得锁。`ReentrantLock`默认情况是非公平的，可以通过 `ReentrantLock`类的`ReentrantLock(boolean fair)`构造方法来制定是否是公平的。
   - **可实现选择性通知（锁可以绑定多个条件）**：`synchronized`关键字与`wait()`和`notify()`/`notifyAll()`方法相结合可以实现等待/通知机制。`ReentrantLock`类当然也可以实现，但是需要借助于`Condition`接口与`newCondition()`方法。