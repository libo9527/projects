package com.gzhennaxia.bill.param;

import com.gzhennaxia.bill.enums.BillStatusEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
@Data
@ToString
public class BillParam {
    private Integer id;
    private Integer userId;
    private String desc;
    private String note;
    private BillStatusEnum status;
    private Date consumeTime;
    private BigDecimal payableOrReceivableAmount;
    private BigDecimal paymentOrReceiptsAmount;

    /**
     * 折扣
     */
    private Integer discountId;
    private String discountDesc;
//    private DiscountVo discount;

    /**
     * 账单分类
     */
    private Integer categoryId;
    private String categoryName;
//    private BillCategoryVo category;

    /**
     * 交易对方
     */
    private Integer counterpartyId;
    private String counterpartyName;
//    private CounterpartyVo counterparty;

    /**
     * 交易平台
     */
    private Integer platformId;
    private String platformName;
//    private PlatformVo platform;

    /**
     * 收支类型
     */
    private BillTypeEnum type;

    /**
     * 支付类型
     */
    private Integer payTypeId;
    private String payTypeName;
//    private PayTypeVo payType;

    /**
     * 标记ID/NAME
     */
    private List<Object> tagIds;

    /**
     * 折扣ID/NAME
     */
    private List<Object> discountIds;

    private List<BillItemParam> billItems;
}
