package com.gzhennaxia.bill.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemDiscountRelDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billItemId;

    private Integer discountId;


}
