package com.gzhennaxia.bill.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 饿了么配置
 *
 * @author gzhennaxia
 */
@Data
@Component
@ConfigurationProperties("ele.response-file-path")
public class EleConfig {

    /**
     * 订单响应文件路径
     */
    private String orders;
    /**
     * 订单详情响应文件路径
     */
    private String orderDetail;
    /**
     * 饿了么果蔬订单详情响应文件路径
     */
    private String fruitOrderDetail;

}
