package com.gzhennaxia.bill.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.serialize.BigDecimalSerializer;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class QuickBillVo {

    private Integer id;
    private String desc;
    private Date consumeTime;
    /**
     * 账单类型
     * 0：收入
     * 1：支出
     */
    private BillTypeEnum type;
    private String categoryName;
    @JsonFormat(pattern = "0.00", shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal money;
    private String payTypeName;
    private String counterpartyName;
    private String platformName;
    private String note;
    private QuickBillStatusEnum status;
    /**
     * 对账时间
     */
    private Date reconciliationTime;
    /**
     * 对账信息
     */
    private String reconciliationMessage;

    /**
     * 正式账单ID
     */
    private Integer billId;

    public QuickBillVo(QuickBill quickBill) {
        BeanUtils.copyProperties(quickBill, this);
    }
}
