package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.ProductCategory;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.vo.ProductCategoryNodeVo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
public interface ProductCategoryMapper extends BaseMapper<ProductCategory> {

    IPage<ProductCategoryNodeVo> selectProductCategoryNodeVoPage(Page<ProductCategoryNodeVo> page, ProductCategoryQuery query);
}
