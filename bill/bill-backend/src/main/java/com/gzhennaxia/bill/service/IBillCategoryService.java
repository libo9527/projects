package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.vo.BillCategoryNodeVo;
import com.gzhennaxia.bill.vo.BillCategoryVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;

import java.util.List;

public interface IBillCategoryService extends IService<BillCategory> {

    List<BillCategoryNodeVo> getBillCategoryNodesByParentId(Integer parentId);

    List<BillCategoryNodeVo> getBillCategoryNodesByPath(String path);

    IPage<BillCategoryNodeVo> selectBillCategoryNodeVoPage(ProductCategoryQuery query);

    void save(BillCategoryVo billCategoryVo);

    void delete(BillCategoryVo billCategoryVo);

    List<TreeNodeVo> getSelectOptions(Integer id);
}
