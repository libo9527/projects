package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.BillTag;
import com.gzhennaxia.bill.mapper.BillTagMapper;
import com.gzhennaxia.bill.query.BillTagQuery;
import com.gzhennaxia.bill.service.IBillTagService;
import com.gzhennaxia.bill.vo.BillTagVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Service
public class BillTagServiceImpl extends ServiceImpl<BillTagMapper, BillTag> implements IBillTagService {

    @Override
    public List<BillTagVo> search(BillTagQuery billTagQuery) {
        return list().stream().map(BillTagVo::new).collect(Collectors.toList());
    }

    @Override
    public List<BillTag> getTagsByBillId(Integer billId) {
        return baseMapper.getTagsByBillId(billId);
    }

    @Override
    public List<BillTag> getTagsByKeyword(String keyword) {
        return list(Wrappers.<BillTag>lambdaQuery().like(BillTag::getName, keyword));
    }
}
