package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.BillDiscountRel;
import com.gzhennaxia.bill.mapper.BillDiscountRelMapper;
import com.gzhennaxia.bill.service.IBillDiscountRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.BillDiscountRelQuery;
import com.gzhennaxia.bill.vo.BillDiscountRelVo;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Service
public class BillDiscountRelServiceImpl extends ServiceImpl<BillDiscountRelMapper, BillDiscountRel> implements IBillDiscountRelService {

    @Override
    public List<BillDiscountRelVo> search(BillDiscountRelQuery billDiscountRelQuery) {
        return list().stream().map(BillDiscountRelVo::new).collect(Collectors.toList());
    }
}
