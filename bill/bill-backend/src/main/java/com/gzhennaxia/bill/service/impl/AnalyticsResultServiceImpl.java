package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.gzhennaxia.bill.mapper.AnalyticsResultMapper;
import com.gzhennaxia.bill.service.IAnalyticsResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.AnalyticsResultQuery;
import com.gzhennaxia.bill.vo.AnalyticsResultVo;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Service
public class AnalyticsResultServiceImpl extends ServiceImpl<AnalyticsResultMapper, AnalyticsResult> implements IAnalyticsResultService {

    @Override
    public List<AnalyticsResultVo> search(AnalyticsResultQuery analyticsResultQuery) {
        return list().stream().map(AnalyticsResultVo::new).collect(Collectors.toList());
    }
}
