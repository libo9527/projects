package com.gzhennaxia.bill.strategy.analytics;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.dto.AnalyticsResultDto;
import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.enums.AnalyticsTypeEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.service.IAnalyticsResultService;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;

import java.util.List;

/**
 * @author lib
 */
public abstract class AbstractAnalyticsStrategy implements AnalyticsStrategy {

    protected final IBillService billService;

    protected final IAnalyticsResultService analyticsResultService;

    /**
     * 最新一条账单
     */
    protected final Bill latestOne;

    private final AnalyticsTypeEnum analyticsType;

    List<AnalyticsResultDto> analyticsResultDtos;

    public AbstractAnalyticsStrategy() {
        this.billService = SpringBeanUtils.getBean(IBillService.class);
        this.analyticsResultService = SpringBeanUtils.getBean(IAnalyticsResultService.class);
        this.latestOne = billService.getOne(Wrappers.<Bill>lambdaQuery().eq(Bill::getDelFlag, IsEnum.NO).orderByDesc(Bill::getConsumeTime).last("LIMIT 1"));
        this.analyticsType = getType();
        this.analyticsResultDtos = analytics();
    }

    @Override
    public void doAnalytics() {
        AnalyticsResult analyticsResult = analyticsResultService.getOne(Wrappers.<AnalyticsResult>lambdaQuery().eq(AnalyticsResult::getType, analyticsType));
        if (analyticsResult == null) {
            analyticsResult = new AnalyticsResult();
        }
        analyticsResult.setType(analyticsType);
        analyticsResult.setLabel(getLabel());
        for (AnalyticsResultDto analyticsResultDto : analyticsResultDtos) {
            if (analyticsResultDto.getType().equals(BillTypeEnum.INCOME)) {
                analyticsResult.setIncome(analyticsResultDto.getValue());
            } else if (analyticsResultDto.getType().equals(BillTypeEnum.EXPENSES)) {
                analyticsResult.setExpense(analyticsResultDto.getValue());
            }
        }
        analyticsResultService.saveOrUpdate(analyticsResult);
    }

    protected abstract AnalyticsTypeEnum getType();

    /**
     * 构建统计标签
     */
    protected abstract String getLabel();

    protected abstract List<AnalyticsResultDto> analytics();

}
