package com.gzhennaxia.bill.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.TimeUnit;

/**
 * @author gzhennaxia
 */
public class TestUtil {

    public static void main(String[] args) {
//        test01();
//        test02();
//        test03();
        test04();
    }

    /**
     * 执行终端命令
     */
    private static void test04() {
        try {
            Runtime.getRuntime().exec("open https://www.baidu.com");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Java打开浏览器的几种方式
     * 这种方式可以，会调用默认浏览器打开网页
     * 但是在Spring Boot项目中Desktop.getDesktop()会报空指针
     */
    private static void test03() {
        URI uri = URI.create("https://h5.ele.me/order/detail/#/8115636588767635801");
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)){
            try {
                desktop.browse(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Java打开浏览器的几种方式
     * error=13, Permission denied
     */
    private static void test02() {
        ProcessBuilder pb = new ProcessBuilder("/Applications/Google Chrome.app", "https://h5.ele.me/order/detail/#/8115636588767635801");
        try {
            pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static void test01() {
        System.setProperty("webdriver.chrome.driver", "/Users/gzhennaxia/Documents/CODE/GitLab/projects/bill/bill-backend/SeleniumWebDriver/ChromeDriver 96.0.4664.45/chromedriver_mac64/chromedriver");
        WebDriver driver = new ChromeDriver();
        // 全屏
        driver.manage().window().maximize();
        // driver 全局等待时间，每一次driver操作的等待时间
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 打开登录页面
        // redirect=https://h5.ele.me/order
        driver.get("https://www.selenium.dev/documentation/webdriver/browser/windows/");
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("window.scrollBy(0,500)");
    }

}
