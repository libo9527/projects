package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 饿了么无门槛代金券对账策略
 *
 * @author lib
 */
public class EleNoThresholdVouchersReconciliationStrategy extends AbstractReconciliationStrategy {

    private final Bill eleNoThresholdVouchersBill;
    private final IProductService productService;
    private final IBillItemService billItemService;

    public EleNoThresholdVouchersReconciliationStrategy() {
        this.billItemService = SpringBeanUtils.getBean(IBillItemService.class);
        this.productService = SpringBeanUtils.getBean(IProductService.class);
        ICounterpartyService counterpartyService = SpringBeanUtils.getBean(ICounterpartyService.class);
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, "上海拉扎斯信息科技有限公司"));
        this.eleNoThresholdVouchersBill = billService.getOne(Wrappers.<Bill>lambdaQuery().likeLeft(Bill::getDesc, "无门槛代金券").eq(Bill::getCounterpartyId, counterparty.getId()).last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        return quickBill.getDesc().endsWith("无门槛代金券")
                && ("上海拉扎斯信息科技有限公司".equals(quickBill.getCounterpartyName()) || quickBill.getCounterpartyName().startsWith("t-"));
    }

    @Override
    public Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setCategoryId(eleNoThresholdVouchersBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setPayTypeId(eleNoThresholdVouchersBill.getPayTypeId());
        bill.setPlatformId(eleNoThresholdVouchersBill.getPlatformId());
        bill.setCounterpartyId(eleNoThresholdVouchersBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {
        BillItemParam billItemParam = new BillItemParam();
        billItemParam.setBillId(bill.getId());
        String productName = "饿了么 " + bill.getDesc();
        Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, productName));
        if (product == null) {
            product = new Product();
            product.setName(productName);
            productService.save(product);
        }
        billItemParam.setProductIdOrName(product.getId());
        billItemParam.setProductCategoryId(product.getCategoryId());
        billItemParam.setPaymentOrReceiptsAmount(bill.getPaymentOrReceiptsAmount());
        billItemService.save(billItemParam);
    }

    @Override
    public void doReconciliation(List<QuickBill> quickBills) {
        List<Bill> list = quickBills.stream().map(this::buildBill).collect(Collectors.toList());
        billService.saveBatch(list);
        quickBillService.update(Wrappers.<QuickBill>lambdaUpdate().set(QuickBill::getStatus, QuickBillStatusEnum.RECONCILED).in(QuickBill::getId, quickBills.stream().map(QuickBill::getId).collect(Collectors.toList())));
    }
}
