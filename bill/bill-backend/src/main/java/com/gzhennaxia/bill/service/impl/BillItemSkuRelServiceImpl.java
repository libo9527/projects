package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.BillItemSkuRel;
import com.gzhennaxia.bill.mapper.BillItemSkuRelMapper;
import com.gzhennaxia.bill.service.IBillItemSkuRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.BillItemSkuRelQuery;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Service
public class BillItemSkuRelServiceImpl extends ServiceImpl<BillItemSkuRelMapper, BillItemSkuRel> implements IBillItemSkuRelService {

    @Override
    public List<BillItemSkuRelVo> search(BillItemSkuRelQuery billItemSkuRelQuery) {
        return list().stream().map(BillItemSkuRelVo::new).collect(Collectors.toList());
    }
}
