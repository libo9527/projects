package com.gzhennaxia.bill.vo;

import java.io.Serializable;
import org.springframework.beans.BeanUtils;
import com.gzhennaxia.bill.entity.BillItemDiscountRel;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemDiscountRelVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billItemId;

    private Integer discountId;


    public BillItemDiscountRelVo(BillItemDiscountRel billItemDiscountRel) {
        BeanUtils.copyProperties(billItemDiscountRel, this);
    }
}
