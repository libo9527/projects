package com.gzhennaxia.bill.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */

@Getter
@AllArgsConstructor
public enum BillFileTypeEnum {

    ALIPAY(0, "支付宝账单"),
    WECHAT(1, "微信账单");

    private Integer code;

    private String message;
}
