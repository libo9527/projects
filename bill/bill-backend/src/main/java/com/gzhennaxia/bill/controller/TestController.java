package com.gzhennaxia.bill.controller;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.dto.EleBillDiscountDto;
import com.gzhennaxia.bill.dto.EleBillDto;
import com.gzhennaxia.bill.dto.EleBillItemDto;
import com.gzhennaxia.bill.entity.DailySummaryReport;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.entity.ThirdpartyBill;
import com.gzhennaxia.bill.entity.User;
import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.TestSqliteMapper;
import com.gzhennaxia.bill.mapper.UserMapper;
import com.gzhennaxia.bill.query.QuickBillQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IDailySummaryReportService;
import com.gzhennaxia.bill.service.IQuickBillService;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestSqliteMapper testSqliteMapper;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IBillService billService;
    @Autowired
    private IQuickBillService quickBillService;
    @Autowired
    private IDailySummaryReportService dailySummaryReportService;

    @org.springframework.beans.factory.annotation.Value("${chrome.driver.path}")
    private String chromeDriverPath;

    @GetMapping("/")
    public String test() {
        return "success";
    }

    @GetMapping("/datetime")
    public String datetime() {
//        List<Bill> bills = billService.list(Wrappers.<Bill>lambdaQuery().select(Bill::getId, Bill::getConsumeTime, Bill::getCreateTime, Bill::getUpdateTime));
//        billService.updateBatchById(bills);
//        List<QuickBill> bills = quickBillService.list(Wrappers.<QuickBill>lambdaQuery().select(QuickBill::getId, QuickBill::getConsumeTime, QuickBill::getCreateTime, QuickBill::getUpdateTime));
//        quickBillService.updateBatchById(bills);
        QueryWrapper<DailySummaryReport> wrapper = new QueryWrapper<>();
        wrapper.select("id, consume_time, create_time, update_time");
        List<DailySummaryReport> bills = dailySummaryReportService.list(wrapper);
        dailySummaryReportService.updateBatchById(bills);
        return "success";
    }

    @PostMapping("/page")
    public String page(@RequestBody QuickBillQuery quickBillQuery) {
        List<QuickBill> bills = quickBillService.list(Wrappers.<QuickBill>lambdaQuery()
                .select(QuickBill::getId, QuickBill::getConsumeTime, QuickBill::getCreateTime, QuickBill::getUpdateTime)
                .between(QuickBill::getConsumeTime, quickBillQuery.getConsumeTimeRange().get(0), quickBillQuery.getConsumeTimeRange().get(1)));
        System.out.println("############ " + bills.size());
        return "success";
    }

    @GetMapping("/sqlite/insert")
    public void testInsert() {
        testSqliteMapper.insertUser(new User(1, "test", "test", "test"));
    }

    @GetMapping("/mybatis-plus")
    public void testMybatisPlus() {
        List<User> users = userMapper.selectList(null);
        System.out.println(users);
    }

    @GetMapping("/logback")
    public void logback() {
        log.debug("test Because the standard `logback.xml` configuration file is loaded too early, you cannot use extensions in it. You need to either use `logback-spring.xml` or define a `logging.config` property.");
    }

    @PostMapping("/valid")
    public void validation(@Validated(User.Update.class) User user) {
        System.out.println(user);
    }

    @PostMapping("/valid2")
    public void validation2(@Valid User user) {
        System.out.println(user);
    }

    /**
     * 编程式校验
     */
    @Autowired
    private Validator validator;

    @PostMapping("/valid3")
    public void validation3(User user) {
        Set<ConstraintViolation<User>> violations = validator.validate(user, User.Update.class);
        if (!violations.isEmpty()) {
            for (ConstraintViolation<User> violation : violations) {
                System.out.println(violation);
            }
        }
    }

    @GetMapping("/selenium1")
    public String selenium1() {
        System.setProperty("webdriver.chrome.driver", "D:\\GitLab\\projects\\bill\\bill-backend\\SeleniumWebDriver\\ChromeDriver_91.0.4472.101\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
//        driver.get("https://www.baidu.com//");
//        driver.get("https://h5.ele.me/");
        driver.get("https://h5.ele.me/order//");
        // 全屏
        driver.manage().window().maximize();
        for (int i = 0; i < 10; i++) {
            WebElement inputElement = driver.findElement(By.id("kw"));
            String text = inputElement.getText();
            if (StringUtils.isEmpty(text)) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                return text;
            }
        }
//        driver.close();
        return "no input text";
    }

    @GetMapping("/selenium2")
    public String selenium2() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\GitLab\\projects\\bill\\bill-backend\\SeleniumWebDriver\\ChromeDriver_91.0.4472.101\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
//        driver.get("https://www.baidu.com//");
//        driver.get("https://h5.ele.me/");
        driver.get("https://tb.ele.me/wow/msite/act/login?redirect=https%3A%2F%2Fh5.ele.me%2Forder%2F%2F");
        // 全屏
        driver.manage().window().maximize();
        for (int i = 0; i < 10; i++) {
            WebElement button = driver.findElement(By.xpath("//button"));
            button.click();
            Thread.sleep(10000);
            WebElement phoneInput = driver.findElement(By.id("fm-sms-login-id"));
            // 输入手机号
            phoneInput.sendKeys("18598046892");
            WebElement sendMessageButton = driver.findElement(By.xpath("//a[@class=\"send-btn-link\"]"));
            // 发送短信验证码
            sendMessageButton.click();
            List<WebElement> delButtons = driver.findElements(By.xpath("//div[@class=\"input-del-btn\"]"));
            int timeout = 60;
            while (delButtons.size() < 2) {
                if (timeout == 0) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("短信验证码已过期"));
                delButtons = driver.findElements(By.xpath("//div[@class=\"input-del-btn\"]"));
                Thread.sleep(1000);
                timeout--;
            }
            Thread.sleep(3000);
            WebElement loginButton = driver.findElement(By.xpath("//div/button[@type=\"submit\"]"));
            loginButton.click();
            Thread.sleep(1000);
            driver.navigate().to("https://h5.ele.me/order//");
            List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
            for (int j = 0; j < orderCards.size(); j++) {
                WebElement orderCard = orderCards.get(j);
                WebElement titleNameContent = orderCard.findElement(By.cssSelector(".title>.name.content"));
                log.info("###### 第 {} 笔订单 ####", j + 1);
                log.info("店家：" + titleNameContent.getText());
                List<WebElement> details = orderCard.findElements(By.cssSelector(".ordercard-detail>.detail>span"));
                StringBuilder products = new StringBuilder();
                for (WebElement detail : details) {
                    products.append(detail.getText());
                }
                log.info("商品：" + products.toString());
                WebElement price = orderCard.findElement(By.cssSelector(".ordercard-detail>.price"));
                log.info("价格：" + price.getText());
            }
        }
//        driver.close();
        return "no input text";
    }

    @GetMapping("/selenium3")
    public String selenium3() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "D:\\GitLab\\projects\\bill\\bill-backend\\SeleniumWebDriver\\ChromeDriver_91.0.4472.101\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        // 全屏
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.get("https://tb.ele.me/wow/msite/act/login?redirect=https%3A%2F%2Fh5.ele.me%2Forder%2F%2F");
        Thread.sleep(3000);

        driver.switchTo().frame("alibaba-login-box");

        WebElement phoneInput = driver.findElement(By.id("fm-sms-login-id"));
        // 输入手机号
        phoneInput.sendKeys("18598046892");
        WebElement sendMessageButton = driver.findElement(By.xpath("//a[@class=\"send-btn-link\"]"));
        // 发送短信验证码
        sendMessageButton.click();
        List<WebElement> delButtons = driver.findElements(By.xpath("//div[@class=\"input-del-btn\"]"));
        int timeout = 60;
        while (delButtons.size() < 2) {
            if (timeout == 0) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("短信验证码已过期"));
            delButtons = driver.findElements(By.xpath("//div[@class=\"input-del-btn\"]"));
            Thread.sleep(1000);
            timeout--;
        }
        Thread.sleep(3000);
        WebElement loginButton = driver.findElement(By.xpath("//div/button[@type=\"submit\"]"));
        loginButton.click();
        Thread.sleep(1000);
        driver.navigate().to("https://h5.ele.me/order//");
        List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
        for (int j = 0; j < orderCards.size(); j++) {
            WebElement orderCard = orderCards.get(j);
            WebElement titleNameContent = orderCard.findElement(By.cssSelector(".title>.name>.content"));
            log.info("###### 第 {} 笔订单 ####", j + 1);
            log.info("店家：" + titleNameContent.getText());
            List<WebElement> details = orderCard.findElements(By.cssSelector(".ordercard-detail>.detail>span"));
            StringBuilder products = new StringBuilder();
            for (WebElement detail : details) {
                products.append(detail.getText());
            }
            log.info("商品：" + products.toString());
            WebElement price = orderCard.findElement(By.cssSelector(".ordercard-detail>.price"));
            log.info("价格：" + price.getText());
        }
//        driver.close();
        return "no input text";
    }

    @GetMapping("/selenium4")
    public String selenium4() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        WebDriver driver = new ChromeDriver();
        // 全屏
        driver.manage().window().maximize();
        // driver 全局等待时间，每一次driver操作的等待时间
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 打开登录页面
        // redirect=https://h5.ele.me/order
        driver.get("https://tb.ele.me/wow/msite/act/login?redirect=https%3A%2F%2Fh5.ele.me%2Forder");
        // 登录页包含在 Frame 中，需要先切换到 Frame 里才可以获取到页面元素
        driver.switchTo().frame("alibaba-login-box");
        // 获取【手机号】输入框
        WebElement phoneInput = driver.findElement(By.id("fm-sms-login-id"));
        // 输入手机号
        phoneInput.sendKeys("18598046892");
        // 获取【发送短信验证码】按钮
        WebElement sendMessageButton = driver.findElement(By.xpath("//a[@class=\"send-btn-link\"]"));
        // 发送短信验证码
//        sendMessageButton.click();
        // 获取【短信验证码】输入框
        WebElement smsCodeInput = driver.findElement(By.id("fm-smscode"));
        // 获取手机号和短信验证码输入框中的【x】清除按钮
        List<WebElement> delButtons = driver.findElements(By.xpath("//div[@class=\"input-del-btn\"]"));
        // 短信验证码1分钟后过期
        int timeout = 20;
        while (delButtons.size() < 2) {
            String value = smsCodeInput.getAttribute("value");
            WebElement smsCodeInput1 = driver.findElement(By.id("fm-smscode"));
            String value1 = smsCodeInput1.getAttribute("value");
            log.info("### value = {}, value1 = {}", value, value1);
            if (timeout == 0) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("短信验证码已过期"));
            Thread.sleep(3000);
            timeout--;
        }
//        WebElement loginButton = driver.findElement(By.xpath("//div/button[@type=\"submit\"]"));
        return "success";
    }

    @GetMapping("/selenium")
    public String selenium() {
        try {
            System.setProperty("webdriver.chrome.driver", chromeDriverPath);
            WebDriver driver = new ChromeDriver();
            // 全屏
            driver.manage().window().maximize();
            // driver 全局等待时间，每一次driver操作的等待时间
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            // 跳转到订单列表页面
            driver.get("http://localhost:8080/#/test");
            // 获取【订单卡片】列表
            List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            for (int j = 0; j < orderCards.size(); j++) {
                WebElement orderCard = orderCards.get(j);
                EleBillDto eleBill = new EleBillDto();
                eleBill.setType(ThirdpartyBillTypeEnum.ELE);
                // 获取商家名称
                WebElement titleNameContent = orderCard.findElement(By.cssSelector(".title>.name>.content"));
                String counterpartyName = titleNameContent.getText();
                eleBill.setCounterpartyName(counterpartyName);
                // 获取消费时间
                WebElement datetime = orderCard.findElement(By.cssSelector(".datetime"));
                Date consumeTime = df.parse(datetime.getText());
                eleBill.setConsumeTime(consumeTime);
                // 获取账单描述
                List<WebElement> details = orderCard.findElements(By.cssSelector(".ordercard-detail>.detail>span"));
                StringBuilder products = new StringBuilder();
                for (WebElement detail : details) {
                    products.append(detail.getText());
                }
                String desc = products.toString();
                eleBill.setDesc(desc);
                // 获取账单价格
                WebElement priceElement = orderCard.findElement(By.cssSelector(".ordercard-detail>.price"));
                BigDecimal price = new BigDecimal(priceElement.getText().substring(1));
                eleBill.setPrice(price);
                List<ThirdpartyBill> list = Collections.emptyList();
                if (CollectionUtils.isEmpty(list)) {
                    // 获取账单详情div元素
                    WebElement orderDetailElement = orderCard.findElement(By.cssSelector(".ordercard-detail"));
                    orderDetailElement.click();

                    // 获取订单详情
                    WebElement restaurantElement = driver.findElement(By.xpath("//div[@class=\"restaurant-card\"]"));
                    // 商家名称
                    WebElement counterpartyNameElement = restaurantElement.findElement(By.cssSelector(".head>.name-wrap>.name"));
                    counterpartyName = counterpartyNameElement.getText();
                    eleBill.setCounterpartyName(counterpartyName);

                    // 商品列表
                    List<WebElement> productElements = restaurantElement.findElements(By.cssSelector(".product-list>.cart-item>.product-item"));
                    List<EleBillItemDto> eleBillItems = new ArrayList<>(productElements.size());
                    for (WebElement productElement : productElements) {
                        EleBillItemDto eleBillItem = new EleBillItemDto();
                        // 商品名称
                        WebElement productNameElement = productElement.findElement(By.cssSelector(".profile>.name"));
                        String productName = productNameElement.getText();
                        eleBillItem.setProductName(productName);
                        // 商品数量
                        WebElement quantityElement = productElement.findElement(By.cssSelector(".quantity"));
                        Integer quantity = Integer.valueOf(quantityElement.getText().substring(1));
                        eleBillItem.setQuantity(quantity);
                        // 商品金额
                        WebElement numberElement = productElement.findElement(By.cssSelector(".number"));
                        BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
                        eleBillItem.setAmount(amount);

                        eleBillItems.add(eleBillItem);
                    }

                    // 餐盒费/配送费等额外信息，在账单系统中均被认为是商品
                    List<WebElement> otherElements = restaurantElement.findElements(By.cssSelector(".listitem>.product-item"));
                    for (WebElement otherElement : otherElements) {
                        EleBillItemDto eleBillItem = new EleBillItemDto();
                        // 名称
                        WebElement nameElement = otherElement.findElement(By.cssSelector(".name"));
                        String name = nameElement.getText();
                        eleBillItem.setProductName(name);

                        List<WebElement> quantityPriceElements = otherElement.findElements(By.cssSelector(".price-wrap>.quantity"));
                        if (CollectionUtils.isNotEmpty(quantityPriceElements)) {
                            // 数量
                            Integer quantity = Integer.valueOf(quantityPriceElements.get(0).getText().substring(1));
                            eleBillItem.setQuantity(quantity);
                            // 金额
                            WebElement numberElement = otherElement.findElement(By.cssSelector(".price-wrap>.number"));
                            BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
                            eleBillItem.setAmount(amount);
                        } else {
                            List<WebElement> numberElement = otherElement.findElements(By.cssSelector(".number"));
                            if (CollectionUtils.isNotEmpty(numberElement)) {
                                // 金额
                                BigDecimal amount = new BigDecimal(numberElement.get(0).getText().substring(1));
                                eleBillItem.setAmount(amount);
                            }
                        }
                    }
                    eleBill.setEleBillItems(eleBillItems);

                    // 折扣
                    List<WebElement> discountElements = restaurantElement.findElements(By.cssSelector(".price>.product-item"));
                    if (CollectionUtils.isNotEmpty(discountElements)) {
                        List<EleBillDiscountDto> eleBillDiscounts = new ArrayList<>(discountElements.size());
                        for (WebElement discountElement : discountElements) {
                            EleBillDiscountDto eleBillDiscount = new EleBillDiscountDto();
//                            WebElement nameElement = discountElement.findElement(By.cssSelector(".name")).findElement(By.xpath("//span"));
                            WebElement nameElement = discountElement.findElement(By.xpath("//span[@class=\"name\"]/span"));
                            String name = nameElement.getText();
                            eleBillDiscount.setDesc(name);

                            StringBuilder sb = new StringBuilder();
                            WebElement amountElement = discountElement.findElement(By.cssSelector(".discount"));
                            List<WebElement> spanElements = amountElement.findElements(By.xpath("span"));
                            for (WebElement spanElement : spanElements) {
                                sb.append(spanElement.getText());
                            }
                            sb.append(amountElement.getText());
                            eleBillDiscount.setAmount(sb.toString());
                            eleBillDiscounts.add(eleBillDiscount);
                        }
                        eleBill.setEleBillDiscounts(eleBillDiscounts);
                    }

                    // 配送信息
                    WebElement deliveryElement = driver.findElement(By.cssSelector(".detailcard>.detailcard-delivery"));
                    String deliveryHTML = deliveryElement.getAttribute("outerHTML");
                    eleBill.setDeliveryHtml(deliveryHTML);

                    // 订单信息
                    WebElement orderElement = driver.findElement(By.cssSelector(".detailcard>.detailcard-order"));
                    String orderHTML = orderElement.getAttribute("outerHTML");
                    eleBill.setOrderHtml(orderHTML);

                    List<WebElement> orderElements = orderElement.findElements(By.cssSelector(".cardlist>.listitem"));
                    for (WebElement orderElement1 : orderElements) {
                        WebElement span = orderElement1.findElement(By.xpath("//span"));
                        if ("订单号：".equals(span.getText())) {
                            eleBill.setThirdpartyId(orderElement1.getText().trim());
                            break;
                        }
                    }
                }
                String retS = new ObjectMapper().writeValueAsString(eleBill);
                log.info("###### 爬取结果：" + retS);
                return retS;
            }
        } catch (ParseException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return "success";
    }

}
