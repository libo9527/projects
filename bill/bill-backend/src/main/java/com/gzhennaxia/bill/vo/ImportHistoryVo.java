package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.ImportHistory;
import com.gzhennaxia.bill.enums.ImportHistoryStatusEnum;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * @author lib
 */
@Data
public class ImportHistoryVo {

    private Integer id;

    private String fileName;

    /**
     * 文件上传时间
     */
    private Date uploadTime;

    /**
     * 导入时间
     */
    private Date importTime;

    /**
     * 导入成功总数
     */
    private Integer successCount;

    /**
     * 重复总数：有多少条记录是数据库已经存在的
     */
    private Integer repeatCount;

    /**
     * 失败条数
     */
    private Integer failedCount;

    /**
     * 导入总数
     */
    private Integer total;

    private ImportHistoryStatusEnum status;

    public ImportHistoryVo(ImportHistory importHistory) {
        BeanUtils.copyProperties(importHistory, this);
    }
}
