package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillItem;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Object productIdOrName;

    private Integer productId;

    private String productName;

    private Integer productCategoryId;

    private String productCategoryName;

    /**
     * 应付/应收金额
     */
    private BigDecimal payableOrReceivableAmount;

    /**
     * 实付/实收金额
     */
    private BigDecimal paymentOrReceiptsAmount;

    private List<BillItemSkuRelVo> billItemSkus;

    private List<Integer> discountIds;

    public BillItemVo() {
    }

    public BillItemVo(BillItem billItem) {
        BeanUtils.copyProperties(billItem, this);
    }
}
