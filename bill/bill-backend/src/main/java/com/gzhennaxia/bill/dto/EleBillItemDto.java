package com.gzhennaxia.bill.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 饿了么账单项
 *
 * @author lib
 */
@Data
public class EleBillItemDto {

    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品数量
     */
    private Integer quantity;
    /**
     * 商品金额
     */
    private BigDecimal amount;
}
