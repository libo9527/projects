package com.gzhennaxia.bill.utils;

import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.exception.ReconciliationException;
import com.gzhennaxia.bill.mapper.QuickBillMapper;
import com.gzhennaxia.bill.result.CodeMessageEnum;

import java.util.Date;

/**
 * 参数校验工具类
 *
 * @author lib
 */
public class ValidatorUtils {

    public static void assertNotNull(Object o, CodeMessageEnum codeMessageEnum, String errorMessage, QuickBillMapper quickBillMapper, Integer id) {
        if (o == null) {
            QuickBill quickBill = new QuickBill();
            quickBill.setId(id);
            quickBill.setStatus(QuickBillStatusEnum.RECONCILIATION_FAILED);
            quickBill.setReconciliationTime(new Date());
            quickBill.setReconciliationMessage(errorMessage);
            quickBillMapper.updateById(quickBill);
            throw new ReconciliationException(codeMessageEnum, errorMessage);
        }
    }

    public static void assertNull(Object o, CodeMessageEnum reconciliationFailed, String errorMessage, QuickBillMapper baseMapper, Integer id) {
        if (o != null) {
            assertNotNull(null, reconciliationFailed, errorMessage, baseMapper, id);
        }
    }

}
