package com.gzhennaxia.bill.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ProductCategoryQuery extends PageParam{

    private Integer parentId;
}
