package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.ProductQuery;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.vo.ProductVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/product")
public class ProductController {

    private final IProductService productService;

    @PostMapping("/search")
    public List<ProductVo> search(@RequestBody ProductQuery productQuery) {
        return productService.search(productQuery);
    }

    /**
     * 解决重名商品
     */
    @GetMapping("/resolveDuplicateName")
    public void resolveDuplicateName() {
        productService.resolveDuplicateName();
    }

}
