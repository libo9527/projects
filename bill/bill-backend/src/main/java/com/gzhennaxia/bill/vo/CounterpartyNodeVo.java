package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
public class CounterpartyNodeVo {

    private Integer id;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Boolean hasChildren;

    private List<CounterpartyNodeVo> children;

    public CounterpartyNodeVo(Counterparty counterparty) {
        BeanUtils.copyProperties(counterparty, this);
        this.isLeaf = IsEnum.YES.getCode().equals(counterparty.getIsLeaf().getCode());
    }

    public Boolean getHasChildren() {
        return Optional.ofNullable(isLeaf).map(i -> !i).orElse(null);
    }
}
