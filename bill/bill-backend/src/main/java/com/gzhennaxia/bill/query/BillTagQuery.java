package com.gzhennaxia.bill.query;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Data
public class BillTagQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private Integer status;


}
