package com.gzhennaxia.bill.dto;

import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
public class ThirdpartyBillDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private ThirdpartyBillTypeEnum type;

    private Date consumeTime;

    /**
     * 第三方主键ID
     */
    private Integer thirdpartyId;

    /**
     * 交易对方
     */
    private String counterpartyName;

    /**
     * 描述
     */
    private String desc;

    /**
     * 价格
     */
    private BigDecimal price;

    private String jsonData;

    private Integer status;
}
