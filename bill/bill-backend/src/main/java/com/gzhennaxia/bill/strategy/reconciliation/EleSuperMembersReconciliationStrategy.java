package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.entity.Sku;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.service.ISkuService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 饿了么超级会员加量包对账策略
 *
 * @author lib
 */
public class EleSuperMembersReconciliationStrategy extends AbstractReconciliationStrategy {

    private final Bill eleSuperMembersBill;
    private final IProductService productService;
    private final IBillItemService billItemService;
    private final ISkuService skuService;

    public EleSuperMembersReconciliationStrategy() {
        this.billItemService = SpringBeanUtils.getBean(IBillItemService.class);
        this.productService = SpringBeanUtils.getBean(IProductService.class);
        this.skuService = SpringBeanUtils.getBean(ISkuService.class);
        this.eleSuperMembersBill = billService.getOne(Wrappers.<Bill>lambdaQuery().likeRight(Bill::getDesc, "超级会员加量包").last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        BigDecimal money = quickBill.getMoney();
        // 如果不是5的倍数需要手动对账
        BigDecimal divide = money.divide(BigDecimal.valueOf(5), BigDecimal.ROUND_HALF_DOWN);
        if (BigDecimal.valueOf(divide.intValue()).compareTo(divide) != 0) return false;
        return quickBill.getDesc().startsWith("超级会员加量包") && "饿了么".equals(quickBill.getCounterpartyName());
    }

    @Override
    public Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setCategoryId(eleSuperMembersBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setPayTypeId(eleSuperMembersBill.getPayTypeId());
        bill.setPlatformId(eleSuperMembersBill.getPlatformId());
        bill.setCounterpartyId(eleSuperMembersBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {
        BillItemParam billItemParam = new BillItemParam();
        billItemParam.setBillId(bill.getId());
        Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, "超级会员加量包"));
        if (product != null) {
            billItemParam.setProductIdOrName(product.getId());
            billItemParam.setProductCategoryId(product.getCategoryId());
            billItemParam.setPaymentOrReceiptsAmount(bill.getPaymentOrReceiptsAmount());
            Sku sku = skuService.getOne(Wrappers.<Sku>lambdaQuery().eq(Sku::getUnit, "张"));
            List<BillItemSkuRelVo> list = new ArrayList<>(1);
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            billItemSkuRelVo.setSkuIdOrUnit(sku.getId());
            billItemSkuRelVo.setSkuPrice(BigDecimal.valueOf(2.5));
            billItemSkuRelVo.setSkuQuantity(bill.getPaymentOrReceiptsAmount().divide(BigDecimal.valueOf(2.5), RoundingMode.DOWN).intValue());
            list.add(billItemSkuRelVo);
            billItemParam.setBillItemSkus(list);
            billItemService.save(billItemParam);
        }
    }

    @Override
    public void doReconciliation(List<QuickBill> quickBills) {
        List<Bill> list = quickBills.stream().map(this::buildBill).collect(Collectors.toList());
        billService.saveBatch(list);
        quickBillService.update(Wrappers.<QuickBill>lambdaUpdate().set(QuickBill::getStatus, QuickBillStatusEnum.RECONCILED).in(QuickBill::getId, quickBills.stream().map(QuickBill::getId).collect(Collectors.toList())));
    }
}
