package com.gzhennaxia.bill.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Discount;
import com.gzhennaxia.bill.mapper.DiscountMapper;
import com.gzhennaxia.bill.query.DiscountQuery;
import com.gzhennaxia.bill.service.IDiscountService;
import com.gzhennaxia.bill.vo.DiscountVo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lib
 */
@Service
@AllArgsConstructor
public class DiscountServiceImpl extends ServiceImpl<DiscountMapper, Discount> implements IDiscountService {

    private final DiscountMapper discountMapper;

    @Override
    public List<DiscountVo> getAllDiscounts() {
        return discountMapper.selectAll();
    }

    @Override
    public IPage<DiscountVo> pageSearch(DiscountQuery query) {
        Page<DiscountVo> pageRet = new Page<>(query.getCurrent(), query.getSize());
        if (!StringUtils.isEmpty(query.getDesc()))
            query.setDesc("%" + query.getDesc().trim() + "%");
        return baseMapper.pageSearch(pageRet, query);
    }

    @Override
    public List<Discount> searchByBillId(Integer billId) {
        return baseMapper.searchByBillId(billId);
    }
}
