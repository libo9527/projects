package com.gzhennaxia.bill.vo;

import lombok.NoArgsConstructor;
import java.io.Serializable;
import org.springframework.beans.BeanUtils;
import com.gzhennaxia.bill.entity.BillDiscountRel;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
@NoArgsConstructor
public class BillDiscountRelVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Integer discountId;


    public BillDiscountRelVo(BillDiscountRel billDiscountRel) {
        BeanUtils.copyProperties(billDiscountRel, this);
    }
}
