package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BillTagRel extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer billId;

    private Integer tagId;


    public BillTagRel(Integer billId, Integer tagId) {
        this.billId = billId;
        this.tagId = tagId;
    }
}
