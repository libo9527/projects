package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.gzhennaxia.bill.enums.AnalyticsTypeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Data
@NoArgsConstructor
public class AnalyticsResultVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private AnalyticsTypeEnum type;

    private String label;

    private BigDecimal income;

    private BigDecimal expense;

    public AnalyticsResultVo(AnalyticsResult analyticsResult) {
        BeanUtils.copyProperties(analyticsResult, this);
    }
}
