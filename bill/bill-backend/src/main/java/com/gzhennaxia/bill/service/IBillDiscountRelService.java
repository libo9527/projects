package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.BillDiscountRel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.BillDiscountRelQuery;
import com.gzhennaxia.bill.vo.BillDiscountRelVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
public interface IBillDiscountRelService extends IService<BillDiscountRel> {

    List<BillDiscountRelVo> search(BillDiscountRelQuery billDiscountRelQuery);
}
