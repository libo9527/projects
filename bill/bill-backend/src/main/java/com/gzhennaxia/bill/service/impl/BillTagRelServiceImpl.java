package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.BillTagRel;
import com.gzhennaxia.bill.mapper.BillTagRelMapper;
import com.gzhennaxia.bill.service.IBillTagRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.BillTagRelQuery;
import com.gzhennaxia.bill.vo.BillTagRelVo;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Service
public class BillTagRelServiceImpl extends ServiceImpl<BillTagRelMapper, BillTagRel> implements IBillTagRelService {

    @Override
    public List<BillTagRelVo> search(BillTagRelQuery billTagRelQuery) {
        return list().stream().map(BillTagRelVo::new).collect(Collectors.toList());
    }
}
