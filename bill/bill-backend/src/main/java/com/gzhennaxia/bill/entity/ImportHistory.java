package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.ImportHistoryStatusEnum;
import com.gzhennaxia.bill.type_handler.MybatisPlusDateTimeTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ImportHistory extends CommonColumn {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String filePath;

    /**
     * 文件上传时间
     */
    @TableField(typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date uploadTime;

    /**
     * 导入时间
     */
    @TableField(typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date importTime;

    /**
     * 导入成功总数
     */
    private Integer successCount;

    /**
     * 重复总数：有多少条记录是数据库已经存在的
     */
    private Integer repeatCount;

    /**
     * 失败条数
     */
    private Integer failedCount;

    /**
     * 导入总数
     */
    private Integer total;

    private ImportHistoryStatusEnum status;

    public ImportHistory(String fileAbsolutePath) {
        this.filePath = fileAbsolutePath;
    }
}
