package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */

@Getter
@AllArgsConstructor
public enum ImportHistoryStatusEnum {

    WAIT_IMPORT(0, "待导入"),
    IMPORTED(1, "已导入");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private final Integer code;
    private final String message;
}
