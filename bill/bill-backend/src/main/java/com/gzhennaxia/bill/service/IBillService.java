package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.dto.AnalyticsResultDto;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.BillQuery;
import com.gzhennaxia.bill.vo.BillVo;
import com.gzhennaxia.bill.vo.DailySummaryVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
public interface IBillService extends IService<Bill> {

    BillVo getDetailById(Integer id);

    void save(BillParam billParam);

    IPage<BillVo> pageSearch(BillQuery billQuery);

    List<AnalyticsResultDto> analytics(Date start, Date end);

    /**
     * 解决重复账单
     */
    void resolveDuplicateBill();

    /**
     * 汇总支出
     */
    BigDecimal sumExpenses(Date startTime, Date endTime);

    List<DailySummaryVo> selectLastDays(Integer days);
}
