package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum QuickBillStatusEnum implements BaseEnum {

    TO_BE_RECONCILED(0, "待对账"),
    RECONCILIATION_FAILED(1, "对账失败"),
    RECONCILED(2, "已对账");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将改枚举类型的字段序列化为 code 的值
     */
    @EnumValue
    @JsonValue
    private Integer code;

    private String message;

//    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
//    public static QuickBillStatusEnum getQuickBillStatusEnum(Integer code) {
//        if (code != null) {
//            for (QuickBillStatusEnum item : values()) {
//                if (item.getCode().equals(code)) {
//                    return item;
//                }
//            }
//        }
//        return null;
//    }
}
