package com.gzhennaxia.bill.exception.config;

import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.exception.ReconciliationException;
import com.gzhennaxia.bill.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import static com.gzhennaxia.bill.result.CodeMessageEnum.*;


@RestControllerAdvice("com.gzhennaxia.bill")
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 捕获一般异常
     * 捕获未知异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(Result.error(UN_KNOWN), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 处理404异常
     * spring boot 默认不会抛异常，这里是捕获不到的。 https://www.cnblogs.com/54chensongxia/p/14007696.html
     *
     * @return
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException e) {
        return new ResponseEntity<>(Result.error(NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    /**
     * 捕获运行时异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handleRuntimeException(RuntimeException e) {
        log.error("handleRuntimeException:", e);
        return new ResponseEntity<>(Result.error(RUNTIME_ERROR, e.getMessage().replace("java.lang.RuntimeException: ", "")), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 捕获业务异常
     * 捕获自定义异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BaseException.class)
    public Result<Object> handleCustomException(BaseException e) {
        e.printStackTrace();
        return Result.error(e);
    }

    /**
     * 捕获参数校验异常
     * javax.validation.constraints
     *
     * @param e
     * @return
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        e.printStackTrace();
        return new ResponseEntity<>(Result.error(e), HttpStatus.BAD_REQUEST);
    }

    /**
     * 捕获参数校验异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler({ReconciliationException.class})
    public ResponseEntity<Object> handleReconciliationException(ReconciliationException e) {
        return new ResponseEntity<>(Result.error(e), HttpStatus.OK);
    }

    /**
     * 捕获参数校验异常
     *
     * @param e
     * @return
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({BindException.class})
    public Result<Object> handleReconciliationException(BindException e) {
        e.printStackTrace();
        return Result.error(INVALIDATION_ERROR);
    }

//    /**
//     * 捕获参数校验异常
//     * javax.validation.constraints
//     *
//     * @param e
//     * @return
//     */
//    @ExceptionHandler({MethodArgumentNotValidException.class})
//    public ResponseEntity<Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
//        String msg = "参数校验失败";
//        List<FieldFailedValidate> fieldFailedValidates = this.extractFailedMessage(e.getBindingResult().getFieldErrors());
//        if (null != fieldFailedValidates && fieldFailedValidates.size() > 0) {
//            msg = fieldFailedValidates.get(0).getMessage();
//        }
//        return new ResponseEntity<>(
//                new GlobalResponseEntity<>(false, "arg555", msg, null),
//                HttpStatus.BAD_REQUEST);
//    }
//
//    /**
//     * 组装validate错误信息
//     *
//     * @param fieldErrors
//     * @return
//     */
//    private List<FieldFailedValidate> extractFailedMessage(List<FieldError> fieldErrors) {
//        List<FieldFailedValidate> fieldFailedValidates = new ArrayList<>();
//        if (null != fieldErrors && fieldErrors.size() > 0) {
//            FieldFailedValidate fieldFailedValidate = null;
//            for (FieldError fieldError : fieldErrors) {
//                fieldFailedValidate = new FieldFailedValidate();
//                fieldFailedValidate.setMessage(fieldError.getDefaultMessage());
//                fieldFailedValidate.setName(fieldError.getField());
//
//                fieldFailedValidates.add(fieldFailedValidate);
//            }
//        }
//
//        return fieldFailedValidates;
//    }
}