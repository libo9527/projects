package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 统计类型常量类
 *
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum AnalyticsTypeEnum implements BaseEnum {

    LATEST_ANNUAL_TOTAL(1, "最近一年的年度总收入/支出"),

    LATEST_MONTHLY_TOTAL(2, "最近一个月的月度总收入/支出"),

    LATEST_WEEKEND_TOTAL(3, "最近一周的周度总收入/支出"),

    LATEST_DAY_TOTAL(4, "最近一天的天度总收入/支出");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private final Integer code;

    private final String message;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static AnalyticsTypeEnum getAnalyticsTypeEnum(Integer code) {
        for (AnalyticsTypeEnum item : values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        return null;
    }

}
