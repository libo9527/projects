package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class BillCategoryVo {

    private Integer id;
    private Integer parentId;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Integer status;

    public BillCategoryVo(BillCategory billCategory) {
        BeanUtils.copyProperties(billCategory, this);
        this.isLeaf = IsEnum.YES.getCode().equals(billCategory.getIsLeaf().getCode());
    }
}
