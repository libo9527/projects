package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.service.IPayTypeService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

/**
 * 微信乘车码对账策略
 *
 * @author lib
 */
public class CarCodeWXReconciliationStrategy extends AbstractReconciliationStrategy {

    private final Bill latestBill;
    private final IProductService productService;
    private final IBillItemService billItemService;
    private final IPayTypeService payTypeService;

    public CarCodeWXReconciliationStrategy() {
        this.billItemService = SpringBeanUtils.getBean(IBillItemService.class);
        this.productService = SpringBeanUtils.getBean(IProductService.class);
        ICounterpartyService counterpartyService = SpringBeanUtils.getBean(ICounterpartyService.class);
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, "深圳市地铁相关运营主体"));
        Integer counterpartyId = counterparty.getId();
        this.latestBill = billService.getOne(Wrappers.<Bill>lambdaQuery().eq(Bill::getCounterpartyId, counterpartyId).last("LIMIT 1"));
        this.payTypeService = SpringBeanUtils.getBean(IPayTypeService.class);
    }

    @Override
    Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setCategoryId(latestBill.getCategoryId());
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().eq(PayType::getName, quickBill.getPayTypeName()));
        bill.setPayTypeId(payType.getId());
        bill.setPlatformId(latestBill.getPlatformId());
        bill.setCounterpartyId(latestBill.getCounterpartyId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        return bill;
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        return "深圳市地铁相关运营主体".equals(quickBill.getCounterpartyName()) && quickBill.getDesc().startsWith("深圳地铁：");
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {
        BillItemParam billItemParam = new BillItemParam();
        billItemParam.setBillId(bill.getId());
        Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, bill.getDesc()));
        if (product != null) {
            billItemParam.setProductIdOrName(product.getId());
            billItemParam.setProductCategoryId(product.getCategoryId());
            billItemParam.setPaymentOrReceiptsAmount(bill.getPaymentOrReceiptsAmount());
            billItemService.save(billItemParam);
        }
    }
}
