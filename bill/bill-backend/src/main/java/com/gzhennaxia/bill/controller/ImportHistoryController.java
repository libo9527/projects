package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.service.ImportHistoryService;
import com.gzhennaxia.bill.vo.ImportHistoryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lib
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/importHistory")
public class ImportHistoryController {

    private final ImportHistoryService importHistoryService;

    @GetMapping("/{id}}")
    public ImportHistoryVo getImportHistoryById(@PathVariable Integer id) {
        return new ImportHistoryVo(importHistoryService.getById(id));
    }
}
