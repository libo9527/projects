package com.gzhennaxia.bill.service;

import java.io.File;

/**
 * @author bo li
 * @date 2020-07-22 12:08
 */
public interface IOrderService {

    void saveWeChatOrders(File file);

    void saveWeChatOrders(String filePath);
}
