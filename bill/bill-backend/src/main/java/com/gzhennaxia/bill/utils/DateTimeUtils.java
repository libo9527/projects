package com.gzhennaxia.bill.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtils {

    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat FORMAT1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public static final SimpleDateFormat FORMAT2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static String format(Date date) {
        return FORMAT.format(date);
    }

    public static Date parse(String s) {
        Date date = null;
        try {
            date = FORMAT.parse(s);
        } catch (ParseException e) {
            try {
                date = FORMAT1.parse(s);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }
        }
        return date;
    }

    public static Date parse4Db(String s) {
        Date date = null;
        try {
            date = FORMAT2.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date parse(String s, SimpleDateFormat format) {
        Date date;
        try {
            date = format.parse(s);
        } catch (ParseException e) {
            return null;
        }
        return date;
    }

    /**
     * 拼接，取{@param date}的日期，取{@param time}的时间
     */
    public static Date concat(Date date, Date time) {
        if (date == null || time == null) return null;
        try {
            return FORMAT.parse(FORMAT.format(date).substring(0, 10) + FORMAT.format(time).substring(10));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 设置时间为 00:00:00
     */
    public static void setDayStartTime(Calendar calendar) {
        //这是将当天的【秒】设置为0
        calendar.set(Calendar.SECOND, 0);
        //这是将当天的【分】设置为0
        calendar.set(Calendar.MINUTE, 0);
        //这是将当天的【时】设置为0
        calendar.set(Calendar.HOUR_OF_DAY, 0);
    }

    /**
     * 设置时间为 23:59:59
     */
    public static void setDayEndTime(Calendar calendar) {
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
    }

    /**
     * 获取当年开始时间
     */
    public static Date getYearStart(Date date) {
        final Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        final int last = instance.getActualMinimum(Calendar.DAY_OF_YEAR);
        instance.set(Calendar.DAY_OF_YEAR, last);
        setDayStartTime(instance);
        return instance.getTime();
    }

    /**
     * 获取当年结束时间
     */
    public static Date getYearEnd(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        final int last = instance.getActualMaximum(Calendar.DAY_OF_YEAR);
        instance.set(Calendar.DAY_OF_YEAR, last);
        setDayEndTime(instance);
        return instance.getTime();
    }

    /**
     * 获取当月开始时间
     */
    public static Date getMonthStart(Date date) {
        final Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        final int last = instance.getActualMinimum(Calendar.DAY_OF_MONTH);
        instance.set(Calendar.DAY_OF_MONTH, last);
        setDayStartTime(instance);
        return instance.getTime();
    }

    /**
     * 获取当月结束时间
     */
    public static Date getMonthEnd(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        final int last = instance.getActualMaximum(Calendar.DAY_OF_MONTH);
        instance.set(Calendar.DAY_OF_MONTH, last);
        setDayEndTime(instance);
        return instance.getTime();
    }

    /**
     * 获取指定日期所在周的周一，且时间为零点
     */
    public static Date getMondayStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        setDayStartTime(calendar);
        return calendar.getTime();
    }

    /**
     * 获取指定日期所在周的周天
     */
    public static Date getSundayEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        setDayEndTime(calendar);
        return calendar.getTime();
    }

    /**
     * 获取指定日期所在周的周一，且时间为零点
     */
    public static Date getWeekStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        setDayStartTime(calendar);
        return calendar.getTime();
    }

    /**
     * 获取指定日期所在周的周天
     */
    public static Date getWeekEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        setDayEndTime(calendar);
        return calendar.getTime();
    }

    /**
     * 获取指定日期零点
     */
    public static Date getDayStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        setDayStartTime(calendar);
        return calendar.getTime();
    }

    /**
     * 获取指定日期24点
     */
    public static Date getDayEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        setDayEndTime(calendar);
        return calendar.getTime();
    }

    /**
     * 增加{@param num}分钟
     */
    public static Date plusMinute(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + num);
        return cal.getTime();
    }

    /**
     * 减少{@param num}分钟
     */
    public static Date minusMinute(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) - num);
        return cal.getTime();
    }

    /**
     * 增加{@param num}天
     */
    public static Date plusDay(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + num);
        return cal.getTime();
    }

    /**
     * 减少{@param num}天
     */
    public static Date minusDay(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - num);
        return cal.getTime();
    }

    /**
     * 前30天
     */
    public static Date pre30Day() {
        return minusDay(new Date(), 30);
    }

    /**
     * 是否是同一天
     */
    public static boolean isSameDay(Date d1, Date d2) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(d1).equals(fmt.format(d2));
    }


    public static void main(String[] args) {
        System.out.println(FORMAT.format(pre30Day()));
    }
}
