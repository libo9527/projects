package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.query.PayTypeQuery;
import com.gzhennaxia.bill.vo.PayTypeNodeVo;

import java.util.List;

/**
 * @author lib
 */
public interface PayTypeMapper extends BaseMapper<PayType> {

    /**
     * 获取账单支付方式节点
     *
     * @param parentId 父级分类ID
     */
    List<PayType> getPayTypeNodesByParentId(Integer parentId);

    IPage<PayTypeNodeVo> selectPayTypeNodeVoPage(Page<PayTypeNodeVo> page, PayTypeQuery query);
}
