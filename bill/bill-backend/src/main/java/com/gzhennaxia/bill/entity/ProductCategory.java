package com.gzhennaxia.bill.entity;

import com.gzhennaxia.bill.vo.ProductCategoryVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductCategory extends TreeEntityCommonColumn {
    public ProductCategory(ProductCategoryVo productCategoryVo) {
        BeanUtils.copyProperties(productCategoryVo, this);
    }
}
