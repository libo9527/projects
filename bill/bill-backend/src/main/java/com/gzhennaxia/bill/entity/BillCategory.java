package com.gzhennaxia.bill.entity;

import com.gzhennaxia.bill.vo.BillCategoryVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BillCategory extends TreeEntityCommonColumn {
    public BillCategory(BillCategoryVo billCategoryVo) {
        BeanUtils.copyProperties(billCategoryVo, this);
    }
}
