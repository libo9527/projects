package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.gzhennaxia.bill.enums.AnalyticsTypeEnum;
import com.gzhennaxia.bill.query.AnalyticsResultQuery;
import com.gzhennaxia.bill.service.IAnalyticsResultService;
import com.gzhennaxia.bill.vo.AnalyticsResultVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/analytics-result")
public class AnalyticsResultController {

    private final IAnalyticsResultService analyticsResultService;

    @PostMapping("/search")
    public List<AnalyticsResultVo> search(AnalyticsResultQuery analyticsResultQuery) {
        return analyticsResultService.search(analyticsResultQuery);
    }

    @PostMapping("/analytics")
    public List<AnalyticsResultVo> analytics(@RequestBody List<AnalyticsTypeEnum> analyticsTypes) {
        return analyticsResultService.list(Wrappers.<AnalyticsResult>lambdaQuery().in(AnalyticsResult::getType, analyticsTypes))
                .stream().map(AnalyticsResultVo::new).collect(Collectors.toList());
    }
}
