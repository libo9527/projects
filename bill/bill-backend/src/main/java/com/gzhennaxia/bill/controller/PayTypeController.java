package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.query.PayTypeQuery;
import com.gzhennaxia.bill.result.Result;
import com.gzhennaxia.bill.service.IPayTypeService;
import com.gzhennaxia.bill.vo.PayTypeNodeVo;
import com.gzhennaxia.bill.vo.PayTypeVo;
import com.gzhennaxia.bill.vo.TreeNodeMoveVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pay-type")
public class PayTypeController {

    private final IPayTypeService payTypeService;

    @GetMapping("/children/{parentId}")
    public Result<?> getPayTypeNodesByParentId(@PathVariable Integer parentId) {
        return Result.success(payTypeService.getPayTypeNodesByParentId(parentId));
    }

    @GetMapping("/path/{path}")
    public Result<?> getPayTypeNodesByPath(@PathVariable String path) {
        return Result.success(payTypeService.getPayTypeNodesByPath(path));
    }

    @PostMapping("/search")
    public IPage<PayTypeNodeVo> search(@RequestBody PayTypeQuery query) {
        return payTypeService.selectPayTypeNodeVoPage(query);
    }

    @PostMapping("/save")
    public void save(@RequestBody PayTypeVo payTypeVo) {
        payTypeService.save(payTypeVo);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody PayTypeVo payTypeVo) {
        payTypeService.delete(payTypeVo);
    }

    @GetMapping("/{id}")
    public PayType getById(@PathVariable Integer id) {
        return payTypeService.getById(id);
    }

    /**
     * 根据叶子节点获取整条链路上的结点集合
     */
    @GetMapping("/getSelectOptions/{id}")
    public List<TreeNodeVo> getSelectOptions(@PathVariable Integer id) {
        return payTypeService.getSelectOptions(id);
    }

    /**
     * 移动节点
     */
    @PostMapping("/move")
    public void move(@RequestBody TreeNodeMoveVo treeNodeMoveVo) {
        payTypeService.move(treeNodeMoveVo);
    }
}
