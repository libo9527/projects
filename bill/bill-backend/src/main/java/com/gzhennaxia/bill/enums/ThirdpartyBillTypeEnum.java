package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum ThirdpartyBillTypeEnum {

    ELE(1, "饿了么"),
    ELE_ORDER(11, "饿了么-订单"),
    ELE_ORDER_DETAIL(12, "饿了么-订单详情");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private final Integer code;
    private final String message;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ThirdpartyBillTypeEnum getThirdpartyBillTypeEnum(Integer code) {
        if (code != null) {
            for (ThirdpartyBillTypeEnum item : values()) {
                if (item.getCode().equals(code)) {
                    return item;
                }
            }
        }
        return null;
    }
}
