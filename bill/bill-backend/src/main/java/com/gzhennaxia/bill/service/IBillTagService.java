package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.BillTag;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.query.BillTagQuery;
import com.gzhennaxia.bill.vo.BillTagVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
public interface IBillTagService extends IService<BillTag> {

    List<BillTagVo> search(BillTagQuery billTagQuery);

    List<BillTag> getTagsByBillId(Integer billId);

    List<BillTag> getTagsByKeyword(String keyword);
}
