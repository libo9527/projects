package com.gzhennaxia.bill.query;

import lombok.Data;

/**
 * @author lib
 */
@Data
public class PageParam {
    protected long size = 10;
    protected long current = 1;
}
