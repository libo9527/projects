package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillItemSkuRel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
@NoArgsConstructor
public class BillItemSkuRelVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billItemId;

    private Integer productId;

    private Object skuIdOrUnit;

    private Integer skuId;

    private String skuUnit;

    private BigDecimal skuPrice;

    private Integer skuQuantity;

    public BillItemSkuRelVo(BillItemSkuRel billItemSkuRel) {
        BeanUtils.copyProperties(billItemSkuRel, this);
    }
}
