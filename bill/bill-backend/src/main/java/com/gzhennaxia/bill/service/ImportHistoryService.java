package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.ImportHistory;

/**
 * @author lib
 */
public interface ImportHistoryService extends IService<ImportHistory> {

    void insert(ImportHistory importHistory);
}
