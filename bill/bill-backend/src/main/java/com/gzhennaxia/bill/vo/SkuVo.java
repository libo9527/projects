package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Sku;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
@NoArgsConstructor
public class SkuVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String desc;

    private String unit;

    private BigDecimal price;

    private Integer quantity;

    public SkuVo(Sku sku) {
        BeanUtils.copyProperties(sku, this);
    }
}
