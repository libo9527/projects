package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.query.ProductQuery;
import com.gzhennaxia.bill.vo.ProductVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
public interface ProductMapper extends BaseMapper<Product> {

    List<ProductVo> search(@Param("productQuery") ProductQuery productQuery);

    List<String> selectDuplicateName();
}
