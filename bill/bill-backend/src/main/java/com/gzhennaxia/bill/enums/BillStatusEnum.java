package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum BillStatusEnum implements BaseEnum {

    TO_BE_FILLED(0, "待填充账单项"),
    TO_BE_PERFECTED(1, "待完善账单项"),
    TO_BE_ARCHIVED(2, "待归档"),
    ARCHIVED(3, "已归档");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private Integer code;

    private String message;
}
