package com.gzhennaxia.bill.demo.apache.commons.csv;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * 目标日历生成器
 * 2021/4/22 ~ 2021/9/21 将俯卧撑从一次10个练到100个
 *
 * @author lib
 */
public class GoalCalendar {

    public static void main(String[] args) throws IOException, ParseException {
        genPushUps();
        genPullUps();
    }

    private static void genPushUps() throws IOException, ParseException {
        String outFilePath = "D:\\GitLab\\Gzhennaxia-github-io\\source\\excel\\2021\\pushUps.csv";
        String startTime = "2021/4/22 10:00";
        String endTime = "2021/9/21 10:00";
        int startValue = 10;
        int endValue = 100;
        fun(outFilePath, startTime, endTime, startValue, endValue);
    }

    private static void genPullUps() throws IOException, ParseException {
        String outFilePath = "D:\\GitLab\\Gzhennaxia-github-io\\source\\excel\\2021\\pullUps.csv";
        String startTime = "2021/4/22 10:00";
        String endTime = "2021/9/21 10:00";
        int startValue = 3;
        int endValue = 50;
        fun(outFilePath, startTime, endTime, startValue, endValue);
    }


    private static void fun(String outFilePath, String startTime, String endTime, int startValue, int endValue) throws IOException, ParseException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFilePath), "gbk"));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        Date start = dateFormat.parse(startTime);
        Date end = dateFormat.parse(endTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(start);

        BigDecimal sv = BigDecimal.valueOf(startValue);
        BigDecimal ev = BigDecimal.valueOf(endValue);
//        BigDecimal interval = BigDecimal.valueOf(0.5932);

        long days = ChronoUnit.DAYS.between(start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

        System.out.println(startTime + " (-) " + endTime + " = " + days);
        BigDecimal interval = ev.subtract(sv).divide(BigDecimal.valueOf(days - 1), 6, BigDecimal.ROUND_HALF_UP);
        System.out.println("interval = " + interval);

        int d = 0;
        ArrayList<String> week = new ArrayList<>(7);
        while (start.getTime() < end.getTime()) {

            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            int daysOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            // 刚开始的第一天
            // 每个月第一天如果不是星期天
            // 需要补零
            if ((d == 0 || dayOfMonth == 1) && dayOfWeek > 1) {
                for (int j = 1; j < dayOfWeek; j++) {
                    week.add("");
                }
            }
//            week.add(calendar.get(Calendar.DAY_OF_MONTH) + "\n " + sv.add(interval.multiply(BigDecimal.valueOf(d))).toString() + "\n " + sv.add(interval.multiply(BigDecimal.valueOf(d))).setScale(0, BigDecimal.ROUND_HALF_UP));
            week.add(calendar.get(Calendar.DAY_OF_MONTH) + "\n " + sv.add(interval.multiply(BigDecimal.valueOf(d))).setScale(0, BigDecimal.ROUND_HALF_UP));
            calendar.add(Calendar.DATE, 1);
            start = calendar.getTime();
            d++;

            // 等到星期六，一行结束，打印
            if (dayOfWeek == 7) {
//                System.out.println(calendar.getTime() + "星期" + (dayOfWeek - 1));
                csvPrinter.printRecord(week);
                week = new ArrayList<>(7);
            }
            if (dayOfMonth == daysOfMonth) {
//                System.out.println("本月共" + daysOfMonth + "天");
                csvPrinter.printRecord(week);
                week = new ArrayList<>(7);
                csvPrinter.printRecord();
                csvPrinter.printRecord("星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");
            }
        }
        csvPrinter.printRecord(week);
        csvPrinter.flush();
    }
}
