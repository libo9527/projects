package com.gzhennaxia.bill.result;

import lombok.Getter;

/**
 * @author lib
 */
@Getter
public enum CodeMessageEnum {

    SUCCESS(20000, "success"),
    INVALIDATION_ERROR(4001, "参数校验失败"),
    FILE_ALREADY_EXIST(4002, "文件已存在"),
    FILE_NOT_EXIST(4003, "文件不存在"),
    UNMATCHED_ENUM(4004, "无法匹配相应的枚举类型"),
    QUICK_BILL_IMPORT_FAILED(4005, "快捷账单导入失败"),
    RECONCILIATION_FAILED(4006, "对账失败"),
    INVALID_PARAM(4007, "无效参数"),
    NOT_FOUND(4040, "请求的资源不存在"),
    RUNTIME_ERROR(5000, "运行时异常"),
    UN_KNOWN(5404, "未知异常");

    private Integer code;
    private String message;

    CodeMessageEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public CodeMessageEnum replaceMessage(String message) {
        this.message = message;
        return this;
    }
}
