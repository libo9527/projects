package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.service.IPayTypeService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

/**
 * 摩拜单车对账策略
 *
 * @author lib
 */
public class MobikeReconciliationStrategy extends AbstractReconciliationStrategy {

    private final Bill mobikeBill;
    private static final String DESC = "车费代扣";
    private static final String COUNTERPARTY_NAME = "北京摩拜科技有限公司";


    public MobikeReconciliationStrategy() {
        ICounterpartyService counterpartyService = SpringBeanUtils.getBean(ICounterpartyService.class);
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, COUNTERPARTY_NAME));
        this.mobikeBill = this.billService.getOne(Wrappers.<Bill>lambdaQuery()
                .eq(Bill::getDesc, DESC)
                .eq(Bill::getCounterpartyId, counterparty.getId())
                .last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        String desc = quickBill.getDesc();
        if (!DESC.equals(desc)) return false;
        String counterpartyName = quickBill.getCounterpartyName();
        return COUNTERPARTY_NAME.equals(counterpartyName);
    }

    @Override
    Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setCategoryId(mobikeBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        IPayTypeService payTypeService = SpringBeanUtils.getBean(IPayTypeService.class);
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().like(PayType::getName, quickBill.getPayTypeName()).last("LIMIT 1"));
        bill.setPayTypeId(payType.getId());
        bill.setPlatformId(mobikeBill.getPlatformId());
        bill.setCounterpartyId(mobikeBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {

    }
}
