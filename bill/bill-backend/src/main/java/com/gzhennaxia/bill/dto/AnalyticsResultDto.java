package com.gzhennaxia.bill.dto;

import com.gzhennaxia.bill.enums.BillTypeEnum;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Data
public class AnalyticsResultDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private BillTypeEnum type;

    private BigDecimal value;


}
