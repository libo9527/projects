package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.type_handler.MybatisPlusDateTimeTypeHandler;
import lombok.Data;

import java.util.Date;

/**
 * 公共列
 *
 * @author lib
 */
@Data
public class CommonColumn {

    private Integer createBy;

    @TableField(fill = FieldFill.INSERT, typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date createTime;

    private Integer updateBy;

    @TableField(fill = FieldFill.INSERT_UPDATE, typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date updateTime;

    private IsEnum delFlag;
}
