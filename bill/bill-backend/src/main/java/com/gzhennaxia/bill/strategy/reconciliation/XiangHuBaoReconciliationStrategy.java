package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 相互宝对账策略
 *
 * @author lib
 */
public class XiangHuBaoReconciliationStrategy extends AbstractReconciliationStrategy {

    private final IBillService billService;
    private final Bill xiangHuBaoBill;
    private final IQuickBillService quickBillService;
    private final IProductService productService;
    private final IBillItemService billItemService;


    public XiangHuBaoReconciliationStrategy() {
        this.billService = SpringBeanUtils.getBean(IBillService.class);
        this.quickBillService = SpringBeanUtils.getBean(IQuickBillService.class);
        this.productService = SpringBeanUtils.getBean(IProductService.class);
        this.billItemService = SpringBeanUtils.getBean(IBillItemService.class);
        this.xiangHuBaoBill = billService.getOne(Wrappers.<Bill>lambdaQuery().likeRight(Bill::getDesc, "相互宝分摊").last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        return quickBill.getDesc().startsWith("相互宝分摊") && "蚂蚁会员（北京）网络技术服务有限公司".equals(quickBill.getCounterpartyName());
    }

    @Override
    public Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(xiangHuBaoBill.getType());
        bill.setCategoryId(xiangHuBaoBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setPayTypeId(xiangHuBaoBill.getPayTypeId());
        bill.setPlatformId(xiangHuBaoBill.getPlatformId());
        bill.setCounterpartyId(xiangHuBaoBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {
        BillItemParam billItemParam = new BillItemParam();
        billItemParam.setBillId(bill.getId());
        Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, "相互宝分摊"));
        if (product != null) {
            billItemParam.setProductIdOrName(product.getId());
            billItemParam.setProductCategoryId(product.getCategoryId());
            billItemParam.setPaymentOrReceiptsAmount(bill.getPaymentOrReceiptsAmount());
            billItemService.save(billItemParam);
        }
    }

    @Override
    public void doReconciliation(List<QuickBill> quickBills) {
        List<Bill> list = quickBills.stream().map(this::buildBill).collect(Collectors.toList());
        billService.saveBatch(list);
        quickBillService.update(Wrappers.<QuickBill>lambdaUpdate().set(QuickBill::getStatus, QuickBillStatusEnum.RECONCILED).in(QuickBill::getId, quickBills.stream().map(QuickBill::getId).collect(Collectors.toList())));
    }
}
