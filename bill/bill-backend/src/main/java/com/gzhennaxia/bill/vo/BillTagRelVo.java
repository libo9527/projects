package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillTagRel;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Data
public class BillTagRelVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Integer tagId;

    public BillTagRelVo(BillTagRel billTagRel) {
        BeanUtils.copyProperties(billTagRel, this);
    }
}
