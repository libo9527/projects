package com.gzhennaxia.bill.query;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Data
public class AnalyticsResultQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer type;

    private BigDecimal value;


}
