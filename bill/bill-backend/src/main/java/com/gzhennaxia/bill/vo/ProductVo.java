package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Product;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Data
@NoArgsConstructor
public class ProductVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private Integer categoryId;

    private String categoryName;

    public ProductVo(Product product) {
        BeanUtils.copyProperties(product, this);
    }
}
