package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class PlatformVo {

    private Integer id;
    private Integer parentId;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Integer status;

    public PlatformVo(Platform platform) {
        BeanUtils.copyProperties(platform, this);
        this.isLeaf = IsEnum.YES.getCode().equals(platform.getIsLeaf().getCode());
    }
}
