package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.TreeEntityCommonColumn;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TreeNodeVo {

    private Integer value;
    private String label;
    private Boolean leaf;

    private List<TreeNodeVo> children;

    public TreeNodeVo(TreeEntityCommonColumn treeEntity) {
        this.value = treeEntity.getId();
        this.label = treeEntity.getName();
        this.leaf = treeEntity.getIsLeaf().equals(IsEnum.YES);
    }
}
