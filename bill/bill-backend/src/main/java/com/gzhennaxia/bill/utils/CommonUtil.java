package com.gzhennaxia.bill.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

public class CommonUtil {

    /**
     * 判断对象是否为空
     *
     * @param obj 对象
     * @return 为空返回true，不为空返回false
     */
    public static boolean isNull(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof String) {
            String string = obj.toString();
            if (string.trim().replaceAll("\\s", "").equals("")) {
                return true;
            }
        }
        if (obj instanceof Collection) {
            Collection collection = (Collection) obj;
            if (collection.isEmpty()) {
                return true;
            }
        }
        if (obj.getClass().isArray() && Array.getLength(obj) == 0) {
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (map.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断对象是不为空
     *
     * @param obj 对象
     * @return 为空返回false，不为空返回true
     */
    public static boolean isNotNull(Object obj) {
        return !isNull(obj);
    }

}
