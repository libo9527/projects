package com.gzhennaxia.bill.query;

import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QuickBillQuery extends PageParam {

    private String desc;

    private String counterpartyName;

    /**
     * 消费时间区间
     */
    private List<Date> consumeTimeRange;

    /**
     * 收支类型
     */
    private BillTypeEnum type;

    /**
     * 状态
     */
    private QuickBillStatusEnum status;
}
