package com.gzhennaxia.bill.dto;

import lombok.Data;

/**
 * @author lib
 */
@Data
public class EleBillDiscountDto {

    private String desc;

    private String amount;
}
