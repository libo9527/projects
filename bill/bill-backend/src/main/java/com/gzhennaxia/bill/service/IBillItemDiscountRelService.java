package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.BillItemDiscountRel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.BillItemDiscountRelQuery;
import com.gzhennaxia.bill.vo.BillItemDiscountRelVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface IBillItemDiscountRelService extends IService<BillItemDiscountRel> {

    List<BillItemDiscountRelVo> search(BillItemDiscountRelQuery billItemDiscountRelQuery);
}
