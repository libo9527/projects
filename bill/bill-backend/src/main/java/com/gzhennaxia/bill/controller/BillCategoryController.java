package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.result.Result;
import com.gzhennaxia.bill.service.IBillCategoryService;
import com.gzhennaxia.bill.vo.BillCategoryNodeVo;
import com.gzhennaxia.bill.vo.BillCategoryVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-category")
public class BillCategoryController {

    private final IBillCategoryService billCategoryService;

    @GetMapping("/children/{parentId}")
    public Result<?> getBillCategoryNodesByParentId(@PathVariable Integer parentId) {
        return Result.success(billCategoryService.getBillCategoryNodesByParentId(parentId));
    }

    @GetMapping("/path/{path}")
    public Result<?> getCategoryNodesByPath(@PathVariable String path) {
        return Result.success(billCategoryService.getBillCategoryNodesByPath(path));
    }

    @PostMapping("/search")
    public IPage<BillCategoryNodeVo> search(@RequestBody ProductCategoryQuery query) {
        return billCategoryService.selectBillCategoryNodeVoPage(query);
    }

    @PostMapping("/save")
    public void save(@RequestBody BillCategoryVo billCategoryVo) {
        billCategoryService.save(billCategoryVo);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody BillCategoryVo billCategoryVo) {
        billCategoryService.delete(billCategoryVo);
    }

    @GetMapping("/namelike/{name}")
    public List<BillCategory> namelike(@PathVariable String name) {
        return billCategoryService.list(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getIsLeaf, IsEnum.YES).like(BillCategory::getName, name));
    }

    @GetMapping("/{id}")
    public BillCategory getById(@PathVariable Integer id) {
        return billCategoryService.getById(id);
    }

    /**
     * 根据叶子节点获取整条链路上的结点集合
     */
    @GetMapping("/getSelectOptions/{id}")
    public List<TreeNodeVo> getSelectOptions(@PathVariable Integer id) {
        return billCategoryService.getSelectOptions(id);
    }
}
