package com.gzhennaxia.bill.enums;

import lombok.Getter;

/**
 * @author lib
 */
@Getter
public enum AliPayStatusEnum {
    Trading_closed(0, "交易关闭"),
    PAY_SUCCESS(1, "支付成功"),
    WAITING_SHIPMENT(2, "等待对方发货"),
    WAITING_CONFIRM_DELIVERY(3, "等待确认收货"),
    TRANSACTION_SUCCESS(4, "交易成功"),
    REFUND_SUCCESS(5, "退款成功"),
    repay_SUCCESS(6, "还款成功"),
    CREDIT_SERVICE_SUCCESS(7, "信用服务使用成功"),
    THAW_SUCCESS(8, "解冻成功");

    private Integer code;
    private String message;

    AliPayStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 判断哪些状态的账单可以保存
     */
    public static boolean savable(String status) {

        return false;
    }
}
