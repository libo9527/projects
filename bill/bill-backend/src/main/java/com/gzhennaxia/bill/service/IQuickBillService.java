package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.QuickBillQuery;
import com.gzhennaxia.bill.vo.ImportResultVo;
import com.gzhennaxia.bill.vo.QuickBillVo;

import java.io.File;
import java.util.List;

/**
 * @author lib
 */
public interface IQuickBillService extends IService<QuickBill> {

    ImportResultVo importFromFile(Integer importHistoryId, IsEnum rightNow);

    IPage<QuickBillVo> search(QuickBillQuery quickBillQuery);

    /**
     * 自动对账
     */
    void autoReconciliation(Integer quickBillId);

    void autoReconciliation(List<Integer> quickBillIds);

    void reconciliation(Integer quickBillId, BillParam bill);

    QuickBillVo selectQuickBillDetailById(Integer id);

    /**
     * 自动对账：微信乘车码乘车记录
     */
    void autoWXCarCodeReconciliation(File file);
}
