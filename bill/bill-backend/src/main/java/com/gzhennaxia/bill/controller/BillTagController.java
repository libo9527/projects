package com.gzhennaxia.bill.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.BillTag;
import com.gzhennaxia.bill.query.BillTagQuery;
import com.gzhennaxia.bill.service.IBillTagService;
import com.gzhennaxia.bill.vo.BillTagVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-tag")
public class BillTagController {

    private final IBillTagService billTagService;

    @PostMapping("/search")
    public List<BillTagVo> search(BillTagQuery billTagQuery) {
        return billTagService.search(billTagQuery);
    }

    @PostMapping("/getByIds")
    public List<BillTag> getByIds(@RequestBody List<Integer> ids) {
        return billTagService.list(Wrappers.<BillTag>lambdaQuery().in(BillTag::getId, ids));
    }

    @GetMapping("/billId/{billId}")
    public List<BillTag> getTagsByBillId(@PathVariable Integer billId) {
        return billTagService.getTagsByBillId(billId);
    }

    @GetMapping("/keyword/{keyword}")
    public List<BillTag> getTagsByKeyword(@PathVariable String keyword) {
        return billTagService.getTagsByKeyword(keyword);
    }

}
