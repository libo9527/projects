package com.gzhennaxia.bill.converter;

import com.gzhennaxia.bill.enums.BaseEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author lib
 */
public class StringCodeToEnumConverterFactory implements ConverterFactory<String, BaseEnum> {

    private static Map<Class, StringCodeToEnumConverter> CONVERTERS = new HashMap<>();

    @Override
    public <T extends BaseEnum> Converter<String, T> getConverter(Class<T> aClass) {
        return getConverter1(aClass);
    }

    public static <T extends BaseEnum> Converter<String, T> getConverter1(Class<T> aClass) {
        StringCodeToEnumConverter converter = CONVERTERS.get(aClass);
        if (Objects.isNull(converter)) {
            converter = new StringCodeToEnumConverter<>(aClass);
            CONVERTERS.put(aClass, converter);
        }
        return converter;
    }
}
