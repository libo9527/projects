package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.BillStatusEnum;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.*;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.query.BillItemQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import com.gzhennaxia.bill.vo.BillItemVo;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Service
@RequiredArgsConstructor
public class BillItemServiceImpl extends ServiceImpl<BillItemMapper, BillItem> implements IBillItemService {

    private final ProductMapper productMapper;
    private final SkuMapper skuMapper;
    private final BillItemDiscountRelMapper billItemDiscountRelMapper;
    private final BillItemSkuRelMapper billItemSkuRelMapper;
    private final BillMapper billMapper;

    @Override
    public List<BillItemVo> search(BillItemQuery billItemQuery) {
        return list().stream().map(BillItemVo::new).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(BillItemParam billItemParam) {
        Integer billId = billItemParam.getBillId();
        if (billId == null) throw new BaseException(CodeMessageEnum.INVALID_PARAM.replaceMessage("账单ID不能为空"));
        BillItem billItem = new BillItem(billItemParam);

        // 1. 处理商品信息
        processProduct(billItemParam, billItem);

        // 2. 保存账单项
        // 商品如果是新商品，需要先保存商品后再保存账单项
        saveOrUpdate(billItem);
        billItemParam.setId(billItem.getId());

        // 3. 处理规格信息
        // 由于规格信息需要关联商品和账单项，因此在两者之后处理
        processBillItemSku(billItemParam, billItem);

        // 4. 处理折扣信息
        processDiscount(billItemParam, billId);

        // 5. 校对账单状态
        proofreadBillStatus(billId);
    }

    /**
     * 校对账单状态
     */
    private void proofreadBillStatus(Integer billId) {
        Bill bill = billMapper.selectById(billId);
        List<BillItem> billItems = baseMapper.selectList(Wrappers.<BillItem>lambdaQuery().eq(BillItem::getBillId, billId).eq(BillItem::getDelFlag, IsEnum.NO));
        if (CollectionUtils.isEmpty(billItems)) {
            bill.setStatus(BillStatusEnum.TO_BE_FILLED);
        } else {
            BigDecimal totalAmount = billItems.stream().map(BillItem::getPaymentOrReceiptsAmount).filter(amount -> amount != null && BigDecimal.ZERO.compareTo(amount) <= 0).collect(Collectors.toList()).stream().reduce(BigDecimal.ZERO, BigDecimal::add);
            if (totalAmount.compareTo(bill.getPaymentOrReceiptsAmount()) == 0) {
                bill.setStatus(BillStatusEnum.TO_BE_ARCHIVED);
            } else {
                bill.setStatus(BillStatusEnum.TO_BE_PERFECTED);
            }
        }
        billMapper.updateById(bill);
    }

    /**
     * 处理折扣信息
     */
    private void processDiscount(BillItemParam billItemParam, Integer billId) {
        List<Integer> discountIds = billItemParam.getDiscountIds();
        if (CollectionUtils.isNotEmpty(discountIds)) {
            List<BillItemDiscountRel> billItemDiscountRels = billItemDiscountRelMapper.selectList(Wrappers.<BillItemDiscountRel>lambdaQuery().eq(BillItemDiscountRel::getBillItemId, billId));
            Iterator<BillItemDiscountRel> iterator = billItemDiscountRels.iterator();
            while (iterator.hasNext()) {
                BillItemDiscountRel next = iterator.next();
                if (discountIds.contains(next.getId())) {
                    iterator.remove();
                    discountIds.removeIf(integer -> integer.equals(next.getId()));
                }
            }
            billItemDiscountRelMapper.deleteBatchIds(billItemDiscountRels.stream().map(BillItemDiscountRel::getId).collect(Collectors.toList()));
            for (Integer discountId : discountIds) {
                BillItemDiscountRel billItemDiscountRel = new BillItemDiscountRel();
                billItemDiscountRel.setBillItemId(billId);
                billItemDiscountRel.setDiscountId(discountId);
                billItemDiscountRelMapper.insert(billItemDiscountRel);
            }
        }
    }

    /**
     * 处理规格信息
     */
    private void processBillItemSku(BillItemParam billItemParam, BillItem billItem) {
        List<BillItemSkuRelVo> billItemSkus = billItemParam.getBillItemSkus();
        if (CollectionUtils.isNotEmpty(billItemSkus)) {
            for (BillItemSkuRelVo itemSku : billItemSkus) {
                itemSku.setBillItemId(billItem.getId());
                itemSku.setProductId(billItem.getProductId());
                Object skuIdOrUnit = itemSku.getSkuIdOrUnit();
                if (skuIdOrUnit != null) {
                    if (skuIdOrUnit instanceof Integer) {
                        itemSku.setSkuId((Integer) skuIdOrUnit);
                    } else if (skuIdOrUnit instanceof String) {
                        Sku sku = new Sku();
                        sku.setUnit((String) skuIdOrUnit);
                        skuMapper.insert(sku);
                        itemSku.setSkuId(sku.getId());
                    } else {
                        throw new BaseException(CodeMessageEnum.INVALID_PARAM.replaceMessage("规格ID/NAME类型无法识别"));
                    }
                    BillItemSkuRel billItemSkuRel = new BillItemSkuRel(itemSku);
                    if (billItemSkuRel.getId() == null) {
                        billItemSkuRelMapper.insert(billItemSkuRel);
                    } else {
                        billItemSkuRelMapper.updateById(billItemSkuRel);
                    }
                }
            }
        }
    }

    /**
     * 处理商品信息
     */
    private void processProduct(BillItemParam billItemParam, BillItem billItem) {
        Object productIdOrName = billItemParam.getProductIdOrName();
        Integer productCategoryId = billItemParam.getProductCategoryId();

        if (productIdOrName == null) {
            throw new BaseException(CodeMessageEnum.INVALID_PARAM.replaceMessage("商品ID/NAME不能为空"));
        } else if (productIdOrName instanceof Integer) {
            billItem.setProductId((Integer) productIdOrName);
            if (productCategoryId != null) {
                Product product = new Product();
                product.setId((Integer) productIdOrName);
                product.setCategoryId(productCategoryId);
                productMapper.updateById(product);
            }
        } else if (productIdOrName instanceof String) {
            Product product = new Product();
            product.setName((String) productIdOrName);
            product.setCategoryId(productCategoryId);
            productMapper.insert(product);
            billItemParam.setProductId(product.getId());
            billItem.setProductId(product.getId());
        } else {
            throw new BaseException(CodeMessageEnum.INVALID_PARAM.replaceMessage("商品ID/NAME类型无法识别"));
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveBatch(Bill bill, List<BillItemParam> billItems) {
        if (bill == null || bill.getId() == null)
            throw new BaseException(CodeMessageEnum.INVALID_PARAM.replaceMessage("账单不能为空"));
        Integer billId = bill.getId();
        for (BillItemParam billItemParam : billItems) {
            billItemParam.setBillId(billId);
            save(billItemParam);
        }
    }
}
