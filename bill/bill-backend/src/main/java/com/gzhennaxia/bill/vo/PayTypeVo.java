package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class PayTypeVo {

    private Integer id;
    private Integer parentId;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Integer status;

    public PayTypeVo(PayType paytype) {
        BeanUtils.copyProperties(paytype, this);
        this.isLeaf = IsEnum.YES.getCode().equals(paytype.getIsLeaf().getCode());
    }
}
