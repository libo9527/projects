package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.DailySummaryReport;
import com.gzhennaxia.bill.mapper.DailySummaryReportMapper;
import com.gzhennaxia.bill.service.IDailySummaryReportService;
import com.gzhennaxia.bill.utils.CommonUtil;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class IDailySummaryReportServiceImpl extends ServiceImpl<DailySummaryReportMapper, DailySummaryReport> implements IDailySummaryReportService {

    @Override
    public List<DailySummaryReport> selectLast30Days() {
        Date pre30Day = DateTimeUtils.pre30Day();
        List<DailySummaryReport> list = list(Wrappers.<DailySummaryReport>lambdaQuery().ge(DailySummaryReport::getDate, pre30Day).orderByAsc(DailySummaryReport::getDate));
        List<DailySummaryReport> result = new ArrayList<>(32);
        for (int i = 0; i < 30; i++) {
            Date date = DateTimeUtils.plusDay(pre30Day, i);
            boolean hasRecord = false;
            for (DailySummaryReport DailySummaryReport : list) {
                if (DateTimeUtils.isSameDay(DailySummaryReport.getDate(), date)) {
                    BigDecimal income = DailySummaryReport.getIncome();
                    if (CommonUtil.isNull(income)) {
                        DailySummaryReport.setIncome(BigDecimal.ZERO);
                    }
                    BigDecimal expense = DailySummaryReport.getExpense();
                    if (CommonUtil.isNull(expense)) {
                        DailySummaryReport.setExpense(BigDecimal.ZERO);
                    }
                    result.add(DailySummaryReport);
                    hasRecord = true;
                }
            }
            if (hasRecord) continue;
            DailySummaryReport DailySummaryReport = new DailySummaryReport();
            DailySummaryReport.setDate(date);
            DailySummaryReport.setIncome(BigDecimal.ZERO);
            DailySummaryReport.setExpense(BigDecimal.ZERO);
            result.add(DailySummaryReport);
        }
        return result;
    }

    @Override
    public void refresh() {

        List<DailySummaryReport> expenses = baseMapper.sumExpense();

        List<DailySummaryReport> incomes = baseMapper.sumIncome();

    }

}
