package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.AnalyticsTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AnalyticsResult extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private AnalyticsTypeEnum type;

    private String label;

    private BigDecimal income;

    private BigDecimal expense;

}
