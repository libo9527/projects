package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.Sku;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.SkuQuery;
import com.gzhennaxia.bill.vo.SkuVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface ISkuService extends IService<Sku> {

    List<SkuVo> search(SkuQuery skuQuery);
}
