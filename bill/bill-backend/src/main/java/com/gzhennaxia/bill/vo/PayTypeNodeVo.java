package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class PayTypeNodeVo {

    private Integer id;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Boolean hasChildren;
    private List<PayTypeNodeVo> children;

    public PayTypeNodeVo(PayType payType) {
        BeanUtils.copyProperties(payType, this);
        this.isLeaf = IsEnum.YES.getCode().equals(payType.getIsLeaf().getCode());
    }

    public Boolean getHasChildren() {
        return Optional.ofNullable(isLeaf).map(i -> !i).orElse(null);
    }
}
