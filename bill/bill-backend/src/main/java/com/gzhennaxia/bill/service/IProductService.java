package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.query.ProductQuery;
import com.gzhennaxia.bill.vo.ProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
public interface IProductService extends IService<Product> {

    List<ProductVo> search(ProductQuery productQuery);

    void resolveDuplicateName();
}
