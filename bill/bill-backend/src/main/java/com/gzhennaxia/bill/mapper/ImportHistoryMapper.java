package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzhennaxia.bill.entity.ImportHistory;

/**
 * @author lib
 */
public interface ImportHistoryMapper extends BaseMapper<ImportHistory> {


}
