package com.gzhennaxia.bill.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.query.PlatformQuery;
import com.gzhennaxia.bill.result.Result;
import com.gzhennaxia.bill.service.IPlatformService;
import com.gzhennaxia.bill.vo.PlatformNodeVo;
import com.gzhennaxia.bill.vo.PlatformVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/platform")
public class PlatformController {

    private final IPlatformService platformService;

    @GetMapping("/children/{parentId}")
    public Result<?> getPlatformNodesByParentId(@PathVariable Integer parentId) {
        return Result.success(platformService.getPlatformNodesByParentId(parentId));
    }

    @GetMapping("/path/{path}")
    public Result<?> getSourceNodesByPath(@PathVariable String path) {
        return Result.success(platformService.getSourceNodesByPath(path));
    }

    @PostMapping("/search")
    public IPage<PlatformNodeVo> search(@RequestBody PlatformQuery query) {
        return platformService.selectPlatformNodeVoPage(query);
    }

    @PostMapping("/save")
    public void save(@RequestBody PlatformVo platformVo) {
        platformService.save(platformVo);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody PlatformVo platformVo) {
        platformService.delete(platformVo);
    }

    @GetMapping("/{id}")
    public Platform getById(@PathVariable Integer id) {
        return platformService.getById(id);
    }

    /**
     * 根据叶子节点获取整条链路上的结点集合
     */
    @GetMapping("/getSelectOptions/{id}")
    public List<TreeNodeVo> getSelectOptions(@PathVariable Integer id) {
        return platformService.getSelectOptions(id);
    }
}
