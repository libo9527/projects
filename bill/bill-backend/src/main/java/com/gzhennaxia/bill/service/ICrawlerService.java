package com.gzhennaxia.bill.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.param.BillItemParam;

import java.util.Date;
import java.util.List;

public interface ICrawlerService {

    /**
     * 对账饿了么外卖订单
     */
    void reconciliationEleOrder(String jsonString);

    void eleSelenium(Date startTime, Date endTime);

    /**
     * 对账饿了么果蔬外卖订单
     */
    void reconciliationEleFruitOrder(JsonNode jsonNode);
}
