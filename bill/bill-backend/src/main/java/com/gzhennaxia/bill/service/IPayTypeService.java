package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.query.PayTypeQuery;
import com.gzhennaxia.bill.vo.PayTypeNodeVo;
import com.gzhennaxia.bill.vo.PayTypeVo;
import com.gzhennaxia.bill.vo.TreeNodeMoveVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;

import java.util.List;

/**
 * @author lib
 */
public interface IPayTypeService extends IService<PayType> {

    List<PayTypeNodeVo> getPayTypeNodesByParentId(Integer parentId);

    List<PayTypeNodeVo> getPayTypeNodesByPath(String path);

    IPage<PayTypeNodeVo> selectPayTypeNodeVoPage(PayTypeQuery query);

    void save(PayTypeVo payTypeVo);

    void delete(PayTypeVo payTypeVo);

    List<TreeNodeVo> getSelectOptions(Integer id);

    void move(TreeNodeMoveVo treeNodeMoveVo);
}
