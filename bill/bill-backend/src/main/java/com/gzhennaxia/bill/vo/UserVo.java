package com.gzhennaxia.bill.vo;

import lombok.Data;

import java.util.List;

@Data
public class UserVo {


    private String name;

    private String introduction;

    private String avatar;

    private List<String> roles;


}
