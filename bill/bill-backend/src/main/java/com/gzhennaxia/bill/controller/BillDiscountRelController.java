package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.BillDiscountRelQuery;
import com.gzhennaxia.bill.service.IBillDiscountRelService;
import com.gzhennaxia.bill.vo.BillDiscountRelVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-discount-rel")
public class BillDiscountRelController {

    private final IBillDiscountRelService billDiscountRelService;

    @PostMapping("/search")
    public List<BillDiscountRelVo> search(BillDiscountRelQuery billDiscountRelQuery) {
       return billDiscountRelService.search(billDiscountRelQuery);
    }
}
