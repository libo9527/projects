package com.gzhennaxia.bill.query;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
public class ThirdpartyBillQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer type;

    private LocalDateTime consumeTime;

    /**
     * 消费时间区间
     */
    private List<Date> consumeTimeRange;

    private String jsonData;

    private Integer status;


}
