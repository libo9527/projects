package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.BillTagRel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.BillTagRelQuery;
import com.gzhennaxia.bill.vo.BillTagRelVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
public interface IBillTagRelService extends IService<BillTagRel> {

    List<BillTagRelVo> search(BillTagRelQuery billTagRelQuery);
}
