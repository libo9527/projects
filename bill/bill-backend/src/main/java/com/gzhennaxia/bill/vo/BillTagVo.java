package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillTag;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Data
public class BillTagVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private Integer status;


    public BillTagVo(BillTag billTag) {
        BeanUtils.copyProperties(billTag, this);
    }
}
