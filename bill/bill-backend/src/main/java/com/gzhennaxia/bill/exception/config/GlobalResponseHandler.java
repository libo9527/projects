package com.gzhennaxia.bill.exception.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.result.Result;
import lombok.SneakyThrows;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Map;

/**
 * @author lib
 */
@RestControllerAdvice
public class GlobalResponseHandler implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    @SneakyThrows
    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        ServletServerHttpResponse res = (ServletServerHttpResponse) response;
        int status = res.getServletResponse().getStatus();
        if (status != HttpStatus.OK.value()) {
            if (body instanceof Map) {
                ((Map<?, ?>) body).remove("trace");
            }
            return body;
        }

        if (body instanceof Result) {
            return body;
        }

        if (body instanceof String) {
            return new ObjectMapper().writeValueAsString(Result.success(body));
        }

        return Result.success(body);
    }

}
