package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.query.QuickBillQuery;
import com.gzhennaxia.bill.vo.QuickBillVo;

public interface QuickBillMapper extends BaseMapper<QuickBill> {

    IPage<QuickBillVo> pageSearch(Page<QuickBillVo> pageRet, QuickBillQuery query);
}
