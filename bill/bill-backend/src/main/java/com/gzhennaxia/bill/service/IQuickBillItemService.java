package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.QuickBillItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-05-08
 */
public interface IQuickBillItemService extends IService<QuickBillItem> {

}
