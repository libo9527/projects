package com.gzhennaxia.bill.entity;

import com.gzhennaxia.bill.validation.EncryptId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @EncryptId
    @NotNull(groups = {Update.class})
    private Integer id;

    @NotNull
    private String username;

    private String password;

    private String token;

    public interface Update {
    }


}
