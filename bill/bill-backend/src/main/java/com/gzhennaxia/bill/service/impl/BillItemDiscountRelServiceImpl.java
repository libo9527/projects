package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.BillItemDiscountRel;
import com.gzhennaxia.bill.mapper.BillItemDiscountRelMapper;
import com.gzhennaxia.bill.service.IBillItemDiscountRelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.BillItemDiscountRelQuery;
import com.gzhennaxia.bill.vo.BillItemDiscountRelVo;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Service
public class BillItemDiscountRelServiceImpl extends ServiceImpl<BillItemDiscountRelMapper, BillItemDiscountRel> implements IBillItemDiscountRelService {

    @Override
    public List<BillItemDiscountRelVo> search(BillItemDiscountRelQuery billItemDiscountRelQuery) {
        return list().stream().map(BillItemDiscountRelVo::new).collect(Collectors.toList());
    }
}
