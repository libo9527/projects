package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.mapper.CounterpartyMapper;
import com.gzhennaxia.bill.query.CounterpartyQuery;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.vo.CounterpartyNodeVo;
import com.gzhennaxia.bill.vo.CounterpartyVo;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@Service
public class CounterpartyServiceImpl extends ServiceImpl<CounterpartyMapper, Counterparty> implements ICounterpartyService {

    @Override
    public List<CounterpartyNodeVo> getCounterpartyNodesByParentId(Integer parentId) {
        return baseMapper.getCounterpartyNodesByParentId(parentId).stream().map(CounterpartyNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public List<CounterpartyNodeVo> getCounterpartyNodesByPath(String path) {
        List<Integer> categoryIds = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<Counterparty> list = baseMapper.selectBatchIds(categoryIds);
        return list.stream().map(CounterpartyNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public IPage<CounterpartyNodeVo> selectCounterpartyNodeVoPage(CounterpartyQuery query) {
        Page<CounterpartyNodeVo> page = new Page<>(query.getCurrent(), query.getSize());
        return baseMapper.selectCounterpartyNodeVoPage(page, query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(CounterpartyVo counterpartyVo) {
        Integer currentId = counterpartyVo.getId();
        Integer parentId = counterpartyVo.getParentId();
        Counterparty current = new Counterparty(counterpartyVo);
        // 如果 id 为空，并且 parentId 不为空，则表示新增；否则表示更新
        if (currentId == null && parentId != null) {
            Counterparty parent = baseMapper.selectById(parentId);
            baseMapper.insert(current);
            currentId = current.getId();
            if (parent == null) {
                current.setPath("0," + current.getId());
            } else {
                parent.setIsLeaf(IsEnum.NO);
                baseMapper.updateById(parent);
                current.setPath(parent.getPath() + "," + current.getId());
            }
        }
        // 新增后需要更新path，因此新增和修改都需要该语句
        baseMapper.updateById(current);
    }

    @Override
    public void delete(CounterpartyVo counterpartyVo) {
        assert counterpartyVo.getId() != null;
        Counterparty counterparty = new Counterparty(counterpartyVo);
        counterparty.setDelFlag(IsEnum.YES);
        baseMapper.updateById(counterparty);
        deleteChildren(counterparty.getId());
    }

    @Async
    void deleteChildren(Integer parentId) {
        List<Counterparty> children = baseMapper.selectList(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getParentId, parentId));
        children.forEach(child -> deleteChildren(child.getId()));
        baseMapper.update(new Counterparty(), Wrappers.<Counterparty>lambdaUpdate().set(Counterparty::getDelFlag, IsEnum.YES).eq(Counterparty::getId, parentId));
    }
}
