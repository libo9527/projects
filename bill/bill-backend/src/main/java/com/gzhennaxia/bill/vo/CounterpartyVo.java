package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class CounterpartyVo {

    private Integer id;
    private Integer parentId;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Integer status;

    public CounterpartyVo(Counterparty counterparty) {
        BeanUtils.copyProperties(counterparty, this);
        this.isLeaf = IsEnum.YES.getCode().equals(counterparty.getIsLeaf().getCode());
    }
}
