package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzhennaxia.bill.entity.Sku;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface SkuMapper extends BaseMapper<Sku> {

}
