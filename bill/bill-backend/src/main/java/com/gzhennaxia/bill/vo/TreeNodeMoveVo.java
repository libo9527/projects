package com.gzhennaxia.bill.vo;

import lombok.Data;

import java.util.List;

/**
 * @author lib
 */
@Data
public class TreeNodeMoveVo {
    private Integer parentId;
    private List<Integer> childIds;
}
