package com.gzhennaxia.bill.query;

import com.gzhennaxia.bill.enums.BillStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BillQuery extends PageParam {

    private String desc;

    private Integer categoryId;

    private Integer counterpartyId;

    /**
     * 消费时间区间
     */
    private List<Date> consumeTimeRange;

    /**
     * 状态
     */
    private BillStatusEnum status;
}
