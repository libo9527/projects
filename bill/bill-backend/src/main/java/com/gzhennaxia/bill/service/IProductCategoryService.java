package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.ProductCategory;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.vo.ProductCategoryNodeVo;
import com.gzhennaxia.bill.vo.ProductCategoryVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
public interface IProductCategoryService extends IService<ProductCategory> {

    List<ProductCategoryNodeVo> getProductCategoryNodesByParentId(Integer parentId);

    void save(ProductCategoryVo productCategoryVo);

    IPage<ProductCategoryNodeVo> selectProductCategoryNodeVoPage(ProductCategoryQuery query);

    void delete(ProductCategoryVo productCategoryVo);
}
