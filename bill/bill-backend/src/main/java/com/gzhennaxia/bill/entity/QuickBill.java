package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gzhennaxia.bill.dto.QuickBillDto;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.type_handler.MybatisPlusDateTimeTypeHandler;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 快捷账单
 *
 * @author lib
 */
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName(autoResultMap = true)
public class QuickBill extends CommonColumn {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 消费时间
     */
    @TableField(typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date consumeTime;

    /**
     * 账单类型
     * 0：收入
     * 1：支出
     */
    private BillTypeEnum type;

    /**
     * 分类名
     */
    private String categoryName;

    /**
     * 描述
     */
    private String desc;

    /**
     * 金额
     */
    private BigDecimal money;

    /**
     * 支付方式名称
     */
    private String payTypeName;

    /**
     * 支付方订单号
     */
    private String payOrderId;

    /**
     * 交易对方
     */
    private String counterpartyName;

    /**
     * 交易对方订单号
     */
    private String counterpartyOrderId;

    /**
     * 交易平台
     */
    private String platformName;

    /**
     * 备注
     */
    private String note;

    /**
     * 状态
     */
    private QuickBillStatusEnum status;

    /**
     * 对账时间
     */
    private Date reconciliationTime;

    /**
     * 对账信息
     */
    private String reconciliationMessage;

    /**
     * 正式账单ID
     */
    private Integer billId;

    public QuickBill(QuickBillDto quickBillDto) {
        BeanUtils.copyProperties(quickBillDto, this);
        this.consumeTime = DateTimeUtils.concat(quickBillDto.getDate(), quickBillDto.getTime());
        this.money = new BigDecimal(quickBillDto.getMoney());
    }
}
