package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.query.PlatformQuery;
import com.gzhennaxia.bill.vo.PlatformNodeVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
public interface PlatformMapper extends BaseMapper<Platform> {
    /**
     * 获取账单分类节点
     *
     * @param parentId 父级分类ID
     */
    List<Platform> getPlatformNodesByParentId(Integer parentId);

    IPage<PlatformNodeVo> selectPlatformNodeVoPage(Page<PlatformNodeVo> page, PlatformQuery query);
}
