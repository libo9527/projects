package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.query.CounterpartyQuery;
import com.gzhennaxia.bill.vo.CounterpartyNodeVo;
import com.gzhennaxia.bill.vo.CounterpartyVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
public interface ICounterpartyService extends IService<Counterparty> {

    List<CounterpartyNodeVo> getCounterpartyNodesByParentId(Integer parentId);
    
    List<CounterpartyNodeVo> getCounterpartyNodesByPath(String path);

    IPage<CounterpartyNodeVo> selectCounterpartyNodeVoPage(CounterpartyQuery query);

    void save(CounterpartyVo counterpartyVo);

    void delete(CounterpartyVo counterpartyVo);
}
