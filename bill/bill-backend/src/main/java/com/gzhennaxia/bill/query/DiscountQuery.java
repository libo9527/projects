package com.gzhennaxia.bill.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lib
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DiscountQuery extends PageParam {

    private String desc;

}

