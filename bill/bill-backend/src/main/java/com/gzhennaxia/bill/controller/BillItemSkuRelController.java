package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.BillItemSkuRelQuery;
import com.gzhennaxia.bill.service.IBillItemSkuRelService;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-item-sku-rel")
public class BillItemSkuRelController {

    private final IBillItemSkuRelService billItemSkuRelService;

    @PostMapping("/search")
    public List<BillItemSkuRelVo> search(BillItemSkuRelQuery billItemSkuRelQuery) {
       return billItemSkuRelService.search(billItemSkuRelQuery);
    }
}
