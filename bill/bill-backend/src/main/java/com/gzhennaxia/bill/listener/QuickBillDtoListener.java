package com.gzhennaxia.bill.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.dto.QuickBillDto;
import com.gzhennaxia.bill.entity.ImportHistory;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.ImportHistoryStatusEnum;
import com.gzhennaxia.bill.mapper.QuickBillMapper;
import com.gzhennaxia.bill.service.ImportHistoryService;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * @author lib
 */
@Slf4j
public class QuickBillDtoListener extends AnalysisEventListener<QuickBillDto> {

    private final QuickBillMapper quickBillMapper;
    private final ImportHistoryService importHistoryService;
    private final ImportHistory importHistory;
    /**
     * 导入成功总数
     */
    private int successCount;

    /**
     * 重复总数：有多少条记录是数据库已经存在的
     */
    private int repeatCount;

    /**
     * 失败条数
     */
    private int failedCount;

    /**
     * 导入总数
     */
    private int total;

    public QuickBillDtoListener(QuickBillMapper quickBillMapper, ImportHistoryService importHistoryService, ImportHistory importHistory) {
        this.quickBillMapper = quickBillMapper;
        this.importHistoryService = importHistoryService;
        this.importHistory = importHistory;
    }

    @Override
    public void invoke(QuickBillDto quickBillDto, AnalysisContext analysisContext) {
        if (quickBillDto.getDate() != null) {
            try {
                QuickBill newBill = new QuickBill(quickBillDto);
                QuickBill oldBill = quickBillMapper.selectOne(Wrappers.<QuickBill>lambdaQuery()
                        .select(QuickBill::getId, QuickBill::getConsumeTime, QuickBill::getDesc, QuickBill::getMoney)
                        .eq(QuickBill::getConsumeTime, newBill.getConsumeTime())
                        .eq(QuickBill::getDesc, newBill.getDesc())
                        .eq(QuickBill::getMoney, newBill.getMoney()));
                if (oldBill == null) {
                    quickBillMapper.insert(newBill);
                    successCount++;
                } else {
                    repeatCount++;
                    log.warn("重复导入：quickBillDto {}", quickBillDto);
                }
                total++;
            } catch (Exception e) {
                log.error("导入失败：quickBillDto {}", quickBillDto);
                failedCount++;
            }
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("所有数据解析完成！");
        this.importHistory.setSuccessCount(successCount);
        this.importHistory.setRepeatCount(repeatCount);
        this.importHistory.setFailedCount(failedCount);
        this.importHistory.setTotal(total);
        this.importHistory.setUploadTime(new Date());
        this.importHistory.setStatus(ImportHistoryStatusEnum.IMPORTED);
        importHistoryService.updateById(this.importHistory);
    }
}
