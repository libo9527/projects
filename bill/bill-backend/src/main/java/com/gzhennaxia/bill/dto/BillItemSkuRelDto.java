package com.gzhennaxia.bill.dto;

import java.math.BigDecimal;
import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemSkuRelDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billItemId;

    private Integer productId;

    private Integer skuId;

    private BigDecimal price;

    private Integer quantity;


}
