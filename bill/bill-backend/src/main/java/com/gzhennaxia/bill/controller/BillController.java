package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.BillQuery;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.vo.BillVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bill")
public class BillController {

    private final IBillService billService;

    @PostMapping("/pageSearch")
    public IPage<BillVo> pageSearch(@RequestBody BillQuery billQuery) {
        return billService.pageSearch(billQuery);
    }

    @PostMapping("/save")
    public void save(@RequestBody BillParam billParam) {
        billService.save(billParam);
    }

    @GetMapping("/{id}")
    public BillVo getDetailById(@PathVariable Integer id) {
        return billService.getDetailById(id);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody List<Integer> ids) {
        billService.update(Wrappers.<Bill>lambdaUpdate().set(Bill::getDelFlag, IsEnum.YES).in(Bill::getId, ids));
    }

    @GetMapping("/resolveDuplicateBill")
    public void resolveDuplicateBill() {
        billService.resolveDuplicateBill();
    }
}
