package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.vo.BillCategoryNodeVo;

import java.util.List;

/**
 * @author lib
 */
public interface BillCategoryMapper extends BaseMapper<BillCategory> {

    /**
     * 获取账单分类节点
     *
     * @param parentId 父级分类ID
     */
    List<BillCategory> getBillCategoryNodesByParentId(Integer parentId);


    IPage<BillCategoryNodeVo> selectBillCategoryNodeVoPage(Page<BillCategoryNodeVo> page, ProductCategoryQuery query);
}
