package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.dto.AnalyticsResultDto;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.query.BillQuery;
import com.gzhennaxia.bill.vo.BillVo;
import com.gzhennaxia.bill.vo.DailySummaryVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface BillMapper extends BaseMapper<Bill> {

    BillVo getDetailById(Integer id);

    IPage<BillVo> pageSearch(Page<BillVo> page, BillQuery query);

    List<AnalyticsResultDto> analytics(@Param("start") Date start, @Param("end") Date end);

    List<Date> selectDuplicateBillConsumeTime();

    List<DailySummaryVo> sumExpense(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

    List<DailySummaryVo> sumIncome(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
}
