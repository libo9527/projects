package com.gzhennaxia.bill.service.impl;

import com.gzhennaxia.bill.entity.QuickBillItem;
import com.gzhennaxia.bill.mapper.QuickBillItemMapper;
import com.gzhennaxia.bill.service.IQuickBillItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-05-08
 */
@Service
public class QuickBillItemServiceImpl extends ServiceImpl<QuickBillItemMapper, QuickBillItem> implements IQuickBillItemService {

}
