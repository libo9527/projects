package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BillDiscountRel extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer billId;

    private Integer discountId;

    public BillDiscountRel(Integer billId, Integer discountId) {
        this.billId = billId;
        this.discountId = discountId;
    }
}
