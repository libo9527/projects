package com.gzhennaxia.bill.constants;

/**
 * 爬虫常量类
 *
 * @author lib
 */
public class CrawlConstant {

    /**
     * 饿了么订单分页查找URL
     * e.g. https://h5.ele.me/restapi/bos/v2/users/2029194321/orders?limit=8&offset=0
     */
    public final static String ELE_ORDERS_PAGE_SEARCH_URL = "https://h5.ele.me/restapi/bos/v2/users/2029194321/orders";

    /**
     * 拼多多H5端地址
     */
    public final static String PIN_DUO_DUO = "http://yangkeduo.com";

}