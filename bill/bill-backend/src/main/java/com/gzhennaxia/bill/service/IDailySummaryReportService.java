package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.DailySummaryReport;

import java.util.List;

public interface IDailySummaryReportService extends IService<DailySummaryReport> {
    List<DailySummaryReport> selectLast30Days();

    void refresh();
}
