package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.ThirdpartyBill;
import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import com.gzhennaxia.bill.query.ThirdpartyBillQuery;
import com.gzhennaxia.bill.vo.ThirdpartyBillVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
public interface IThirdpartyBillService extends IService<ThirdpartyBill> {

    List<ThirdpartyBillVo> search(ThirdpartyBillQuery thirdpartyBillQuery);

    void syncEle();

    /**
     * 导入饿了么订单响应文件
     */
    void importEleResponse();
}
