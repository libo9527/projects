package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.BillItem;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.query.BillItemQuery;
import com.gzhennaxia.bill.vo.BillItemVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface IBillItemService extends IService<BillItem> {

    List<BillItemVo> search(BillItemQuery billItemQuery);

    void save(BillItemParam billItemParam);

    void saveBatch(Bill bill, List<BillItemParam> billItems);
}
