package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.entity.Sku;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.ICrawlerService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.service.ISkuService;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import com.gzhennaxia.bill.vo.BillItemVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 爬虫
 *
 * @author lib
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/crawler")
public class CrawlerController {

    private final IQuickBillService quickBillService;
    private final ISkuService skuService;
    private final IProductService productService;
    private final ICrawlerService crawlerService;

    /**
     * 拼多多订单详情
     * OPERATION_MANUAL.md#手动录入拼多多订单详情
     */
    @PostMapping("/pdd/order")
    public void pddOrder(@RequestBody String jsonString) {
        QuickBill quickBill = new QuickBill();
        ObjectMapper om = new ObjectMapper();
        try {
            JsonNode jsonNode = om.readTree(jsonString);
            JsonNode orderDescList = jsonNode.get("data").get("orderDescList");
            if (orderDescList.isArray()) {
                for (JsonNode orderDesc : orderDescList) {
                    JsonNode key = orderDesc.get("key");
                    if ("下单时间：".equals(key.asText())) {
                        JsonNode value = orderDesc.get("value");
                        quickBill.setConsumeTime(DateTimeUtils.parse(value.asText()));
                    }
                    if ("支付方式：".equals(key.asText())) {
                        JsonNode value = orderDesc.get("value");
                        quickBill.setPayTypeName(value.asText());
                    }
                }
            }
            JsonNode orderGoods = jsonNode.get("data").get("orderGoods");
            List<BillItemVo> billItems = null;
            if (orderGoods.isArray()) {
                quickBill.setDesc("共" + orderGoods.size() + "件商品：" + orderGoods.get(0).get("goodsName").asText());
                billItems = buildBillItems(orderGoods);
                quickBill.setNote(om.writeValueAsString(billItems));
            }
            quickBill.setType(BillTypeEnum.EXPENSES);
            quickBill.setMoney(getMoney(jsonNode.get("data").get("orderAmount").asDouble()));
            quickBill.setCounterpartyName("拼多多平台商户");
            quickBill.setPlatformName("拼多多");
            quickBill.setStatus(QuickBillStatusEnum.TO_BE_RECONCILED);
            saveQuickBill(quickBill);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void saveQuickBill(QuickBill quickBill) {
        QuickBill oldQuickBill = quickBillService.getOne(Wrappers.<QuickBill>lambdaQuery()
                .eq(QuickBill::getType, BillTypeEnum.EXPENSES)
                .eq(QuickBill::getStatus, QuickBillStatusEnum.TO_BE_RECONCILED)
                .between(QuickBill::getConsumeTime, quickBill.getConsumeTime(), DateTimeUtils.plusMinute(quickBill.getConsumeTime(), 1)));
        if (oldQuickBill == null) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("没找到快捷账单"));
        if (oldQuickBill.getMoney().compareTo(quickBill.getMoney()) == 0) {
            quickBill.setId(oldQuickBill.getId());
            quickBillService.updateById(quickBill);
        }
    }

    private List<BillItemVo> buildBillItems(JsonNode orderGoods) {
        List<BillItemVo> billItemArrayList = new ArrayList<>(orderGoods.size());
        for (JsonNode orderGood : orderGoods) {
            BillItemVo billItem = new BillItemVo();
            billItem.setProductName(orderGood.get("goodsName").asText());
            billItem.setPaymentOrReceiptsAmount(getMoney(orderGood.get("goodsPrice").asDouble()));
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            billItemSkuRelVo.setSkuUnit(orderGood.get("spec").asText());
            billItemSkuRelVo.setSkuQuantity(orderGood.get("goodsNumber").asInt());
            billItem.setBillItemSkus(Collections.singletonList(billItemSkuRelVo));
            billItemArrayList.add(billItem);
        }
        return billItemArrayList;
    }

    private BigDecimal getMoney(double money) {
        return BigDecimal.valueOf(money / 100);
    }

    /**
     * 淘宝订单详情
     * OPERATION_MANUAL.md#手动录入淘宝订单详情
     */
    @PostMapping("/tb/order")
    public void tbOrder(@RequestBody String jsonString) {
        QuickBill quickBill = new QuickBill();
        ObjectMapper om = new ObjectMapper();
        try {
            JsonNode jsonNode = om.readTree(jsonString);
            JsonNode basicLists = jsonNode.get("basic").get("lists");
            if (basicLists.isArray()) {
                for (JsonNode basic : basicLists) {
                    if ("订单编号".equals(basic.get("key").asText())) {
                        JsonNode contents = basic.get("content");
                        for (JsonNode content : contents) {
                            if ("more".equals(content.get("type").asText())) {
                                JsonNode moreList = content.get("moreList");
                                for (JsonNode node : moreList) {
                                    if ("付款时间".equals(node.get("key").asText())) {
                                        quickBill.setConsumeTime(DateTimeUtils.parse(node.get("content").get(0).get("text").asText()));
                                    } else if ("支付宝交易号".equals(node.get("key").asText())) {
                                        quickBill.setPayOrderId(node.get("content").get(0).get("text").asText());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            buildBillItems(jsonNode, quickBill);
            JsonNode amounts = jsonNode.get("amount").get("count").get(0);
            for (JsonNode amount : amounts) {
                JsonNode data = amount.get("content").get(0).get("data");
                if ("实付款".equals(data.get("titleLink").get("text").asText())) {
                    quickBill.setMoney(new BigDecimal(data.get("money").get("text").asText().substring(1)));
                }
            }
            quickBill.setType(BillTypeEnum.EXPENSES);
            quickBill.setStatus(QuickBillStatusEnum.TO_BE_RECONCILED);
            saveQuickBill(quickBill);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private void buildBillItems(JsonNode jsonNode, QuickBill quickBill) throws JsonProcessingException {
        JsonNode list = jsonNode.get("orders").get("list");
        if (list.size() > 1) throw new BaseException(CodeMessageEnum.UN_KNOWN, "不支持订单拆分为多个订单的情况");
        JsonNode status = list.get(0).get("status");
        List<BillItemVo> billItemArrayList = new ArrayList<>(status.size());
        quickBill.setDesc("共" + status.size() + "件商品：" + status.get(0).get("subOrders").get(0).get("itemInfo").get("title").asText());
        for (JsonNode node : status) {
            BillItemVo billItem = new BillItemVo();
            JsonNode subOrders = node.get("subOrders");
            if (subOrders.size() > 1) throw new BaseException(CodeMessageEnum.UN_KNOWN, "不支持组合商品的情况");
            JsonNode subOrder = subOrders.get(0);
            billItem.setProductName(subOrder.get("itemInfo").get("title").asText());
            if (subOrder.get("priceInfo").size() > 1)
                throw new BaseException(CodeMessageEnum.UN_KNOWN, "不支持商品有多个价格的情况");
            billItem.setPaymentOrReceiptsAmount(new BigDecimal(subOrder.get("priceInfo").get(0).get("text").asText().substring(1)));
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            JsonNode skus = subOrder.get("itemInfo").get("skuText");
            if (skus.isArray() && skus.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (JsonNode sku : skus) {
                    sb.append(sku.get("key")).append("：").append(sku.get("content").get(0).get("text")).append("；");
                }
                billItemSkuRelVo.setSkuUnit(sb.toString());
            }
            billItemSkuRelVo.setSkuQuantity(subOrder.get("quantity").asInt());
            billItem.setBillItemSkus(Collections.singletonList(billItemSkuRelVo));
            billItemArrayList.add(billItem);
        }
        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        quickBill.setNote(om.writerWithDefaultPrettyPrinter().writeValueAsString(billItemArrayList));
    }

    /**
     * 饿了么美食订单详情
     * OPERATION_MANUAL.md#手动录入饿了么订单详情
     */
    @PostMapping("/ele/order")
    public void eleOrder(@RequestBody String jsonString) {
        crawlerService.reconciliationEleOrder(jsonString);
    }

    /**
     * 饿了么果蔬订单详情
     * OPERATION_MANUAL.md#手动录入饿了么订单详情
     */
    @PostMapping("/ele/order/fruit")
    public void eleFruitOrder(@RequestBody String jsonString) {
        ObjectMapper om = new ObjectMapper();
        try {
            JsonNode jsonNode = om.readTree(jsonString);
            crawlerService.reconciliationEleFruitOrder(jsonNode);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 构建饿了么果蔬订单项，填充到快捷账单的备注中，并返回订单项集合
     */
    private List<BillItemParam> buildEleFruitBillItems(JsonNode jsonNode, QuickBill quickBill) throws JsonProcessingException {
        JsonNode goods = jsonNode.get("basket").get("group");
        quickBill.setDesc("共" + goods.size() + "件商品：" + goods.get(0).get("name").asText());
        List<BillItemParam> billItemArrayList = new ArrayList<>(goods.size());
        for (JsonNode good : goods) {
            BillItemParam billItem = new BillItemParam();
            String productName = good.get("name").asText();
            Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, productName));
            if (product != null) {
                billItem.setProductIdOrName(product.getId());
            } else {
                billItem.setProductIdOrName(productName);
            }
            BigDecimal price = null;
            if (good.get("price") != null) {
                price = new BigDecimal(good.get("price").asText());
                if (price.compareTo(BigDecimal.ZERO) <= 0) continue;
                billItem.setPayableOrReceivableAmount(price);
            }
            billItem.setPaymentOrReceiptsAmount(new BigDecimal(good.get("currentPrice").asText()));
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            billItemSkuRelVo.setSkuPrice(price);
            Sku sku = skuService.getOne(Wrappers.<Sku>lambdaQuery().eq(Sku::getUnit, "份"));
            billItemSkuRelVo.setSkuIdOrUnit(sku.getId());
            billItemSkuRelVo.setSkuQuantity(good.get("quantity").asInt());
            billItem.setBillItemSkus(Collections.singletonList(billItemSkuRelVo));
            billItemArrayList.add(billItem);
        }
        ObjectMapper om = new ObjectMapper();
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        quickBill.setNote(om.writerWithDefaultPrettyPrinter().writeValueAsString(billItemArrayList));
        return billItemArrayList;
    }

    private void saveEleQuickBill(QuickBill quickBill) {
        QuickBill oldQuickBill = quickBillService.getOne(Wrappers.<QuickBill>lambdaQuery()
                .eq(QuickBill::getType, BillTypeEnum.EXPENSES)
                .eq(QuickBill::getStatus, QuickBillStatusEnum.TO_BE_RECONCILED)
                .eq(QuickBill::getDesc, quickBill.getCounterpartyName() + "外卖订单")
                .between(QuickBill::getConsumeTime, quickBill.getConsumeTime(), DateTimeUtils.plusMinute(quickBill.getConsumeTime(), 1)));
        if (oldQuickBill == null) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("没找到快捷账单"));
        if (oldQuickBill.getMoney().compareTo(quickBill.getMoney()) == 0) {
            quickBill.setId(oldQuickBill.getId());
            quickBill.setStatus(QuickBillStatusEnum.RECONCILED);
            quickBillService.updateById(quickBill);
        }
    }

    /**
     * 使用 Selenium 爬取饿了么账单
     */
    @GetMapping("/selenium/ele")
    public void eleSelenium(Date startTime, Date endTime) {
        crawlerService.eleSelenium(startTime, endTime);
    }
}
