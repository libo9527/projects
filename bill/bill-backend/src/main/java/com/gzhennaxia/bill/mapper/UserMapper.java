package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper extends BaseMapper<User> {
    void insertBill(@Param("bill") Bill bill);
}
