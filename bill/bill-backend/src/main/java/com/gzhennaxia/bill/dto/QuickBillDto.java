package com.gzhennaxia.bill.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 对应之前手动记账时用的 excel
 *
 * @author lib
 */
@Data
@ToString
public class QuickBillDto {

    // 日期	时间	类别	名称	金额(¥)	数量	支付方式	渠道	备注
    private Date date;
    private Date time;
    private String categoryName;
    private String desc;
    private String money;
    private String amount;
    private String payTypeName;
    private String counterpartyName;
    private String note;

}
