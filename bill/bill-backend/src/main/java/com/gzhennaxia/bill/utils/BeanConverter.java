package com.gzhennaxia.bill.utils;

import com.gzhennaxia.bill.entity.AliPayLedgerItem;
import com.gzhennaxia.bill.entity.Order;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.entity.WeChatLedgerItem;
import com.gzhennaxia.bill.enums.AliPayStatusEnum;
import com.gzhennaxia.bill.enums.BillFileTypeEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.exception.ReconciliationException;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import org.apache.commons.csv.CSVRecord;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

/**
 * @author bo li
 * @date 2020-07-22 13:42
 */
public class BeanConverter {

    public static WeChatLedgerItem convert2WeChatLedgerItem(CSVRecord record) {
        WeChatLedgerItem ret = null;
        Date date = DateTimeUtils.parse(record.get(0));
        if (date != null) {
            ret = new WeChatLedgerItem();
            ret.setTransactionTime(DateTimeUtils.parse(record.get(0)));
            ret.setTransactionType(record.get(1));
            ret.setCounterparty(record.get(2));
            ret.setProductName(record.get(3));
            ret.setIncomeOrExpenditure(record.get(4));
            ret.setMoney(record.get(5));
            ret.setPayType(record.get(6));
            ret.setStatus(record.get(7));
            ret.setTransactionNumber(record.get(8));
            ret.setMerchantNumber(record.get(9));
            ret.setRemark(record.get(10));
        }
        return ret;
    }

    public static AliPayLedgerItem convert2AliPayLedgerItem(CSVRecord record) {
        AliPayLedgerItem ret = null;
        if (record.size() >= 16 && !record.get(0).startsWith("交易号")) {
            ret = new AliPayLedgerItem();
            ret.setTransactionNumber(record.get(0).trim());
            ret.setMerchantOrderId(record.get(1).trim());
            ret.setDealCreateTime(DateTimeUtils.parse(record.get(2).trim()));
            ret.setPayTime(DateTimeUtils.parse(record.get(3).trim()));
            ret.setLastUpdateTime(DateTimeUtils.parse(record.get(4).trim()));
            ret.setDealSource(record.get(5).trim());
            ret.setType(record.get(6).trim());
            ret.setCounterparty(record.get(7).trim());
            ret.setProductName(record.get(8).trim());
            ret.setMoney(record.get(9).trim());
            ret.setIncomeOrExpenditure(record.get(10).trim());
            ret.setStatus(record.get(11).trim());
            ret.setServiceFee(record.get(12).trim());
            ret.setRefundMoney(record.get(13).trim());
            ret.setRemark(record.get(14).trim());
            ret.setFundStatus(record.get(15).trim());
        }
        return ret;
    }

    public static Order convert2Order(WeChatLedgerItem weChatLedgerItem) {

        return null;
    }

    public static QuickBill convert2QuickBill(CSVRecord record, BillFileTypeEnum type) {
        QuickBill quickBill = null;
        if (BillFileTypeEnum.ALIPAY.equals(type)) {
            quickBill = convertAlipay2QuickBill(record);
        } else if (BillFileTypeEnum.WECHAT.equals(type)) {
            quickBill = convertWechat2QuickBill(record);
        }
        return quickBill;
    }

    private static QuickBill convertWechat2QuickBill(CSVRecord record) {
        QuickBill quickBill;
        quickBill = new QuickBill();
        // 交易时间
        Date consumeTime = DateTimeUtils.parse(record.get(0).trim());
        quickBill.setConsumeTime(consumeTime);
        // 交易类型
        String categoryName = record.get(1).trim();
        quickBill.setCategoryName(categoryName);
        // 交易对方
        String counterpartyName = record.get(2).trim();
        quickBill.setCounterpartyName(counterpartyName);
        // 商品
        String desc = record.get(3).trim();
        quickBill.setDesc(desc);
        // 收/支
        BillTypeEnum billTypeEnum = BillTypeEnum.getBillTypeEnum(record.get(4).trim());
        if (billTypeEnum != null && consumeTime == null) {
            throw new ReconciliationException(CodeMessageEnum.QUICK_BILL_IMPORT_FAILED, "失败原因：交易时间不能为空！");
        }
        quickBill.setType(billTypeEnum);
        // 金额(元)
        String money = record.get(5).trim().substring(1);
        quickBill.setMoney(new BigDecimal(money));
        // 支付方式
        String payTypeName = record.get(6).trim();
        quickBill.setPayTypeName(payTypeName);
        // 当前状态
        String note = record.get(7).trim();
        quickBill.setNote(note);
        // 交易单号
        String payOrderId = record.get(8).trim();
        quickBill.setPayOrderId(payOrderId);
        // 商户单号
        String counterpartyOrderId = record.get(9).trim();
        quickBill.setCounterpartyOrderId(counterpartyOrderId);
        // 备注
        String note1 = record.get(10).trim();
        note1 += "---" + quickBill.getNote();
        quickBill.setNote(note1);
        return quickBill;
    }

    private static QuickBill convertAlipay2QuickBill(CSVRecord record) {
        QuickBill quickBill;
        quickBill = new QuickBill();
        // 收/支
        String billTypeString = record.get(0).trim();
        // 交易对方
        String counterpartyName = record.get(1).trim();
        // 对方账号(暂时没有使用到)
        // String counterpartyCount = record.get(2).trim();
        // 商品说明
        String desc = record.get(3).trim();

        BillTypeEnum billTypeEnum = BillTypeEnum.getBillTypeEnum(billTypeString);
        if (desc.startsWith("余额宝") && desc.endsWith("收益发放")) {
            billTypeEnum = BillTypeEnum.INCOME;
        } else if (billTypeEnum == null) {
            throw new ReconciliationException(CodeMessageEnum.QUICK_BILL_IMPORT_FAILED, "失败原因：系统不支持的收支类型！");
        }
        quickBill.setType(billTypeEnum);
        quickBill.setCounterpartyName(counterpartyName);
        quickBill.setDesc(desc);
        // 收/付款方式
        String payTypeName = record.get(4).trim();
        quickBill.setPayTypeName(payTypeName);
        // 金额
        String money = record.get(5).trim();
        quickBill.setMoney(new BigDecimal(money));
        // 交易状态
        String status = record.get(6).trim();
        if (savable(status)) {
            throw new ReconciliationException(CodeMessageEnum.QUICK_BILL_IMPORT_FAILED, "失败原因：交易状态不正确！");
        }
        // 交易分类(暂时没有使用到)
        String categoryName = record.get(7).trim();
        quickBill.setCategoryName(categoryName);
        // 交易订单号
        String payOrderId = record.get(8).trim();
        quickBill.setPayOrderId(payOrderId);
        // 商家订单号
        String counterpartyOrderId = record.get(9).trim();
        quickBill.setCounterpartyOrderId(counterpartyOrderId);
        // 交易时间
        Date consumeTime = DateTimeUtils.parse(record.get(10).trim());
        if (consumeTime == null) {
            throw new ReconciliationException(CodeMessageEnum.QUICK_BILL_IMPORT_FAILED, "失败原因：交易时间不能为空！");
        }
        quickBill.setConsumeTime(consumeTime);
        return quickBill;
    }

    private static boolean savable(String status) {
        return !Arrays.asList(AliPayStatusEnum.PAY_SUCCESS.getMessage(), AliPayStatusEnum.WAITING_SHIPMENT.getMessage(), AliPayStatusEnum.WAITING_CONFIRM_DELIVERY.getMessage(), AliPayStatusEnum.TRANSACTION_SUCCESS.getMessage()).contains(status);
    }

}
