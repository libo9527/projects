package com.gzhennaxia.bill.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.dto.WXCarCodeDto;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.mapper.QuickBillMapper;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * easy excel 处理器
 * 微信乘车码
 *
 * @author lib
 */
public class WXCarCodeListener extends AnalysisEventListener<WXCarCodeDto> {

    private final QuickBillMapper quickBillMapper;

    public WXCarCodeListener(QuickBillMapper quickBillMapper) {
        this.quickBillMapper = quickBillMapper;
    }

    @Override
    public void invoke(WXCarCodeDto wxCarCodeDto, AnalysisContext analysisContext) {
        Date time = wxCarCodeDto.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + 1);
        Date time2 = cal.getTime();

        QuickBill quickBill = quickBillMapper.selectOne(Wrappers.<QuickBill>lambdaQuery()
                .eq(QuickBill::getDesc, "深圳地铁")
                .eq(QuickBill::getCounterpartyName, "深圳市地铁相关运营主体")
                .between(QuickBill::getConsumeTime, time, time2));
//                        .eq(QuickBill::getMoney, new BigDecimal(wxCarCodeDto.getMoney().substring(1)))

        if (quickBill != null) {
            if (quickBill.getMoney().compareTo(new BigDecimal(wxCarCodeDto.getMoney().substring(1))) == 0) {
                quickBill.setDesc(quickBill.getDesc() + "：" + wxCarCodeDto.getLine());
                quickBillMapper.updateById(quickBill);
            }
        } else {
            // todo
            System.out.println(wxCarCodeDto);
        }

    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }

}
