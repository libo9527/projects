package com.gzhennaxia.bill.query;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Data
public class ProductQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer categoryId;

    /**
     * 关键字
     */
    private String keyword;


}
