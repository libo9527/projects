package com.gzhennaxia.bill.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.dto.AnalyticsResultDto;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.mapper.BillMapper;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.BillQuery;
import com.gzhennaxia.bill.service.*;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import com.gzhennaxia.bill.vo.BillVo;
import com.gzhennaxia.bill.vo.DailySummaryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lib
 */
@Service
@RequiredArgsConstructor
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill> implements IBillService {

    private final IBillTagRelService billTagRelService;
    private final IBillTagService billTagService;
    private final IBillDiscountRelService billDiscountRelService;
    private final IDiscountService discountService;
    private final IBillItemService billItemService;

    @Override
    public BillVo getDetailById(Integer id) {
        return baseMapper.getDetailById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(BillParam billParam) {
        Bill bill = new Bill(billParam);
        saveOrUpdate(bill);
        billParam.setId(bill.getId());

        // 处理账单标记
        processBillTags(billParam, bill);

        // 处理账单折扣
        processBillDiscount(billParam, bill);

        // 处理账单项
        if (CollectionUtils.isNotEmpty(billParam.getBillItems())) {
            billItemService.saveBatch(bill, billParam.getBillItems());
        }
    }

    private void processBillDiscount(BillParam billParam, Bill bill) {
        if (CollectionUtils.isNotEmpty(billParam.getDiscountIds())) {
            List<BillDiscountRel> billDiscountRels = new ArrayList<>();
            Integer billId = bill.getId();
            List<Integer> oldDiscountIds = billDiscountRelService.list(Wrappers.<BillDiscountRel>lambdaQuery().eq(BillDiscountRel::getBillId, billId).eq(BillDiscountRel::getDelFlag, IsEnum.NO)).stream().map(BillDiscountRel::getDiscountId).collect(Collectors.toList());
            for (Object discountIdOrDesc : billParam.getDiscountIds()) {
                if (discountIdOrDesc instanceof Integer) {
                    Integer discountId = (Integer) discountIdOrDesc;
                    if (oldDiscountIds.contains(discountId)) continue;
                    billDiscountRels.add(new BillDiscountRel(billId, discountId));
                } else if (discountIdOrDesc instanceof String) {
                    String discountDesc = (String) discountIdOrDesc;
                    Discount discount = discountService.getOne(Wrappers.<Discount>lambdaQuery().eq(Discount::getDesc, discountDesc));
                    if (discount == null) {
                        discount = new Discount();
                        discount.setDesc(discountDesc);
                        discountService.save(discount);
                    }
                    billDiscountRels.add(new BillDiscountRel(billId, discount.getId()));
                }
            }
            billDiscountRelService.saveBatch(billDiscountRels);
        }
    }

    private void processBillTags(BillParam billParam, Bill bill) {
        if (CollectionUtils.isNotEmpty(billParam.getTagIds())) {
            List<BillTagRel> billTagRels = new ArrayList<>();
            Integer billId = bill.getId();
            List<Integer> oldTagIds = billTagService.getTagsByBillId(billId).stream().map(BillTag::getId).collect(Collectors.toList());
            for (Object tag : billParam.getTagIds()) {
                if (tag instanceof Integer) {
                    Integer tagId = (Integer) tag;
                    if (oldTagIds.contains(tagId)) continue;
                    billTagRels.add(new BillTagRel(billId, tagId));
                } else if (tag instanceof String) {
                    String tagName = (String) tag;
                    BillTag billTag = billTagService.getOne(Wrappers.<BillTag>lambdaQuery().eq(BillTag::getName, tagName));
                    if (billTag == null) {
                        billTag = new BillTag();
                        billTag.setName(tagName);
                        billTagService.save(billTag);
                    }
                    billTagRels.add(new BillTagRel(billId, billTag.getId()));
                }
            }
            billTagRelService.saveBatch(billTagRels);
        }
    }

    @Override
    public IPage<BillVo> pageSearch(BillQuery query) {
        Page<BillVo> page = new Page<>(query.getCurrent(), query.getSize());
        if (!StringUtils.isEmpty(query.getDesc()))
            query.setDesc("%" + query.getDesc().trim() + "%");
        return baseMapper.pageSearch(page, query);
    }

    @Override
    public List<AnalyticsResultDto> analytics(Date start, Date end) {
        return baseMapper.analytics(start, end);
    }

    @Override
    public void resolveDuplicateBill() {
        //查询重复账单的消费时间
        List<Date> billConsumeTimes = baseMapper.selectDuplicateBillConsumeTime();
        for (Date billConsumeTime : billConsumeTimes) {
            // todo
        }
    }

    @Override
    public BigDecimal sumExpenses(Date startTime, Date endTime) {
        QueryWrapper<Bill> qw = new QueryWrapper<>();
        qw.select("sum(payment_or_receipts_amount) as totalAmount");
        qw.ge("consume_time", startTime);
        qw.le("consume_time", endTime);
        Bill bill = getOne(qw);
        return bill == null ? BigDecimal.ZERO : bill.getTotalAmount();
    }

    @Override
    public List<DailySummaryVo> selectLastDays(Integer days) {
        if (days <= 0) days = 30;
        List<DailySummaryVo> result = new ArrayList<>(days);
        Date now = new Date();
        Date startTime = DateTimeUtils.getDayStart(DateTimeUtils.minusDay(now, days));
        Date endTime = DateTimeUtils.getDayEnd(now);
        List<DailySummaryVo> expenses = baseMapper.sumExpense(startTime, endTime);
        List<DailySummaryVo> incomes = baseMapper.sumIncome(startTime, endTime);
        Map<String, BigDecimal> expenseMap = expenses.stream().collect(Collectors.toMap(item -> DateTimeUtils.format(item.getDate()), DailySummaryVo::getExpense));
        Map<String, BigDecimal> incomeMap = incomes.stream().collect(Collectors.toMap(item -> DateTimeUtils.format(item.getDate()), DailySummaryVo::getExpense));
        for (int i = 0; i < days; i++) {
            String current = DateTimeUtils.format(DateTimeUtils.plusDay(startTime, i));
            Date date = DateTimeUtils.plusDay(startTime, i);
            DailySummaryVo dailySummaryVo = new DailySummaryVo();
            dailySummaryVo.setDate(date);
            dailySummaryVo.setIncome(BigDecimal.ZERO);
            dailySummaryVo.setExpense(BigDecimal.ZERO);
            if (expenseMap.containsKey(current)) {
                dailySummaryVo.setExpense(expenseMap.get(current));
            }
            if (incomeMap.containsKey(current)) {
                dailySummaryVo.setIncome(incomeMap.get(current));
            }
            result.add(dailySummaryVo);
        }
        return result;
    }

}
