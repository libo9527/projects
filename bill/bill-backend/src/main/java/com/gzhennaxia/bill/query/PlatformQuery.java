package com.gzhennaxia.bill.query;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lib
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PlatformQuery extends PageParam{

    private Integer parentId;
}
