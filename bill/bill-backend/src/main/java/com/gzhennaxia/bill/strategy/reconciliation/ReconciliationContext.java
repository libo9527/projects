package com.gzhennaxia.bill.strategy.reconciliation;

import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;

/**
 * 对账策略上下文
 *
 * @author lib
 */
@Slf4j
public class ReconciliationContext {

    public Boolean reconciliation(QuickBill quickBill) {
        if (quickBill.getStatus().equals(QuickBillStatusEnum.RECONCILED)) return true;
        ServiceLoader<ReconciliationStrategy> serviceLoader = ServiceLoader.load(ReconciliationStrategy.class);
        for (ReconciliationStrategy strategy : serviceLoader) {
            if (strategy.valid(quickBill)) {
                strategy.doReconciliation(quickBill);
                return true;
            }
        }
        return false;
    }

    public void reconciliation(List<QuickBill> quickBills) {
        ServiceLoader<ReconciliationStrategy> serviceLoader = ServiceLoader.load(ReconciliationStrategy.class);
        for (ReconciliationStrategy strategy : serviceLoader) {
            List<QuickBill> list = new ArrayList<>();
            Iterator<QuickBill> iterator = quickBills.iterator();
            while (iterator.hasNext()) {
                QuickBill quickBill = iterator.next();
                if (strategy.valid(quickBill)) {
                    list.add(quickBill);
                    iterator.remove();
                }
            }
            if (CollectionUtils.isNotEmpty(list)) strategy.doReconciliation(list);
        }
    }
}