package com.gzhennaxia.bill.controller;


import com.gzhennaxia.bill.entity.DailySummaryReport;
import com.gzhennaxia.bill.service.IDailySummaryReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dailySummaryReport")
public class DailySummaryReportController {

    private final IDailySummaryReportService IDailySummaryReportService;

    /**
     * Income and expenditure records for the last 30 days
     */
    @GetMapping("/selectLast30Days")
    public List<DailySummaryReport> selectLast30Days() {
        return IDailySummaryReportService.selectLast30Days();
    }
}
