package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.BillTagRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
public interface BillTagRelMapper extends BaseMapper<BillTagRel> {

}
