package com.gzhennaxia.bill.dto;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * 微信乘车码乘车记录
 */
@Data
@ToString
public class WXCarCodeDto {

    /**
     * 线路
     */
    private String line;

    /**
     * 金额
     */
    private String money;

    /**
     * 时间
     */
    private Date time;

}
