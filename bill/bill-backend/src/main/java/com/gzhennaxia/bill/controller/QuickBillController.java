package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.QuickBillQuery;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.vo.ImportResultVo;
import com.gzhennaxia.bill.vo.QuickBillVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 快捷账单
 *
 * @author lib
 */
@RestController
@RequestMapping("/quickbill")
public class QuickBillController {

    private final IQuickBillService quickBillService;

    public QuickBillController(IQuickBillService quickBillService) {
        this.quickBillService = quickBillService;
    }

    @GetMapping("/{id}")
    public QuickBillVo selectQuickBillDetailById(@PathVariable Integer id) {
        return quickBillService.selectQuickBillDetailById(id);
    }

    @PostMapping("/page")
    public IPage<QuickBillVo> search(@RequestBody QuickBillQuery quickBillQuery) {
        return quickBillService.search(quickBillQuery);
    }

    @PostMapping("/save")
    public void save(@RequestBody QuickBill quickBill) {
        quickBillService.saveOrUpdate(quickBill);
    }

    @GetMapping("/importFromFile/{importHistoryId}")
    public ImportResultVo importFromFile(@PathVariable Integer importHistoryId, IsEnum rightNow) {
        return quickBillService.importFromFile(importHistoryId, rightNow);
    }

    @PostMapping("/delete/{id}")
    public void delete(@PathVariable Integer id) {
        quickBillService.update(Wrappers.<QuickBill>lambdaUpdate().set(QuickBill::getDelFlag, IsEnum.YES).eq(QuickBill::getId, id));
    }

    /**
     * 自动对账
     */
    @GetMapping("/reconciliation/auto")
    public void reconciliation(@RequestParam Integer quickBillId) {
        quickBillService.autoReconciliation(quickBillId);
    }


    /**
     * 自动对账
     */
    @PostMapping("/reconciliation/auto/batch")
    public void reconciliation(@RequestBody List<Integer> quickBillIds) {
        quickBillService.autoReconciliation(quickBillIds);
    }

    /**
     * 手动对账
     */
    @PostMapping("/reconciliation/manual")
    public void reconciliation(@RequestBody BillParam billParam, @RequestParam Integer quickBillId) {
        quickBillService.reconciliation(quickBillId, billParam);
    }

}
