package com.gzhennaxia.bill.param;

import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemParam implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Integer productId;

    private Object productIdOrName;

    private Integer productCategoryId;

    private List<Integer> discountIds;

    /**
     * 应付/应收金额
     */
    private BigDecimal payableOrReceivableAmount;

    /**
     * 实付/实收金额
     */
    private BigDecimal paymentOrReceiptsAmount;

    private List<BillItemSkuRelVo> billItemSkus;

}
