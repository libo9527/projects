package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.dto.EleBillDiscountDto;
import com.gzhennaxia.bill.dto.EleBillDto;
import com.gzhennaxia.bill.dto.EleBillItemDto;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.*;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.*;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import com.gzhennaxia.bill.utils.ReconciliationUtils;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class CrawlerServiceImpl implements ICrawlerService {

    @Value("${chrome.driver.path}")
    private String chromeDriverPath;

    private final IQuickBillService quickBillService;
    private final IBillCategoryService billCategoryService;
    private final ICounterpartyService counterpartyService;
    private final IPlatformService platformService;
    private final IPayTypeService payTypeService;
    private final IBillService billService;
    private final IBillItemService billItemService;
    private final ISkuService skuService;
    private final IProductService productService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void reconciliationEleOrder(String jsonString) {
        try {
            ObjectMapper om = new ObjectMapper();
            JsonNode jsonNode = om.readTree(jsonString);
            // 1. build QuickBill.
            QuickBill quickBill = new QuickBill();
            quickBill.setType(BillTypeEnum.EXPENSES);
            quickBill.setStatus(QuickBillStatusEnum.TO_BE_RECONCILED);
            quickBill.setConsumeTime(DateTimeUtils.parse(jsonNode.get("formatted_created_at").asText() + ":00"));
            quickBill.setMoney(new BigDecimal(jsonNode.get("total_amount").asText()));
            quickBill.setCounterpartyName(jsonNode.get("restaurant_name").asText());
            JsonNode goods = jsonNode.get("basket").get("group").get(0);
            quickBill.setDesc("共" + goods.size() + "件商品：" + goods.get(0).get("name").asText());
            // 2. build BillItem list.
            List<BillItemParam> billItems = buildEleBillItems(jsonNode);
            // 3. fix QuickBill.
//            ObjectMapper om = new ObjectMapper();
            om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            quickBill.setNote(om.writerWithDefaultPrettyPrinter().writeValueAsString(billItems));
            // 4. save QuickBill.
            saveEleQuickBill(quickBill);
            // 5. reconcile.
            reconciliationEleOrder(quickBill, billItems);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 构建饿了么美食订单项，填充到快捷账单的备注中，并返回订单项集合
     */
    private List<BillItemParam> buildEleBillItems(JsonNode jsonNode) throws JsonProcessingException {
        JsonNode goods = jsonNode.get("basket").get("group").get(0);
        List<BillItemParam> billItemArrayList = new ArrayList<>(goods.size());
        for (JsonNode good : goods) {
            BillItemParam billItem = new BillItemParam();
            String productName = good.get("name").asText();
            Product product = productService.getOne(Wrappers.<Product>lambdaQuery()
                    .eq(Product::getName, productName)
                    .eq(Product::getDelFlag, IsEnum.NO));
            if (product != null) {
                billItem.setProductIdOrName(product.getId());
            } else {
                billItem.setProductIdOrName(productName);
            }
            BigDecimal price = null;
            if (good.get("original_price") != null) {
                price = new BigDecimal(good.get("original_price").asText());
                if (price.compareTo(BigDecimal.ZERO) <= 0) continue;
                billItem.setPayableOrReceivableAmount(price);
            }
            billItem.setPaymentOrReceiptsAmount(new BigDecimal(good.get("current_price").asText()));
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            billItemSkuRelVo.setSkuPrice(price);
            String saleUnit = good.get("sale_unit").asText();
            Sku one = skuService.getOne(Wrappers.<Sku>lambdaQuery().eq(Sku::getUnit, saleUnit));
            if (one != null) {
                billItemSkuRelVo.setSkuIdOrUnit(one.getId());
            } else {
                Sku sku = new Sku();
                sku.setUnit(saleUnit);
                skuService.save(sku);
                billItemSkuRelVo.setSkuIdOrUnit(sku.getId());
            }
            billItemSkuRelVo.setSkuUnit(saleUnit);
            billItemSkuRelVo.setSkuQuantity(good.get("quantity").asInt());
            billItem.setBillItemSkus(Collections.singletonList(billItemSkuRelVo));
            billItemArrayList.add(billItem);
        }
        return billItemArrayList;
    }

    /**
     * 对账
     */
    private void reconciliationEleOrder(QuickBill quickBill, List<BillItemParam> billItems) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        String threeMealsName = ReconciliationUtils.parseThreeMealsName(quickBill.getConsumeTime());
        BillCategory category = billCategoryService.getOne(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getName, threeMealsName));
        bill.setCategoryId(category.getId());
        Counterparty parentCounterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, "饿了么平台商户"));
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getParentId, parentCounterparty.getId()).eq(Counterparty::getName, quickBill.getCounterpartyName()));
        if (counterparty == null) {
            counterparty = new Counterparty();
            counterparty.setName(quickBill.getCounterpartyName());
            counterparty.setParentId(parentCounterparty.getId());
            counterparty.setIsLeaf(IsEnum.YES);
            counterpartyService.save(counterparty);
            counterparty.setPath(parentCounterparty.getPath() + "," + counterparty.getId());
            counterpartyService.updateById(counterparty);
        }
        bill.setCounterpartyId(counterparty.getId());
        Platform platform = platformService.getOne(Wrappers.<Platform>lambdaQuery().eq(Platform::getName, "饿了么外卖APP"));
        bill.setPlatformId(platform.getId());
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().eq(PayType::getName, "花呗"));
        bill.setPayTypeId(payType.getId());
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setStatus(BillStatusEnum.TO_BE_ARCHIVED);
        billService.save(bill);
        quickBill.setBillId(bill.getId());
        quickBillService.updateById(quickBill);
        billItemService.saveBatch(bill, billItems);
    }

    private WebDriver getDriver() {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        WebDriver driver = new ChromeDriver();
        // 全屏
        driver.manage().window().maximize();
        // driver 全局等待时间，每一次driver操作的等待时间
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        return driver;
    }

    private void saveEleQuickBill(QuickBill quickBill) {
        QuickBill oldQuickBill = quickBillService.getOne(Wrappers.<QuickBill>lambdaQuery()
                .eq(QuickBill::getType, BillTypeEnum.EXPENSES)
                .eq(QuickBill::getStatus, QuickBillStatusEnum.TO_BE_RECONCILED)
                .eq(QuickBill::getDesc, quickBill.getCounterpartyName() + "外卖订单")
                .between(QuickBill::getConsumeTime, quickBill.getConsumeTime(), DateTimeUtils.plusMinute(quickBill.getConsumeTime(), 1)));
        if (oldQuickBill == null)
            throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("没找到快捷账单，消费时间：" + DateTimeUtils.format(quickBill.getConsumeTime()) + "，消费金额：" + quickBill.getMoney()));
        if (oldQuickBill.getMoney().compareTo(quickBill.getMoney()) == 0) {
            quickBill.setId(oldQuickBill.getId());
            quickBill.setConsumeTime(oldQuickBill.getConsumeTime());
            quickBill.setStatus(QuickBillStatusEnum.RECONCILED);
            quickBillService.updateById(quickBill);
        }
    }

    @Override
    @Transactional(rollbackFor = BaseException.class)
    public void eleSelenium(Date startTime, Date endTime) {
        try {
            WebDriver driver = getDriver();
            // 打开登录页面
            // redirect=https://h5.ele.me/order
            driver.get("https://tb.ele.me/wow/msite/act/login?redirect=https%3A%2F%2Fh5.ele.me%2Forder");
            // 登录页包含在 Frame 中，需要先切换到 Frame 里才可以获取到页面元素
            driver.switchTo().frame("alibaba-login-box");
            // 获取【手机号】输入框
            WebElement phoneInput = driver.findElement(By.id("fm-sms-login-id"));
            // 输入手机号
            phoneInput.sendKeys("18598046892");
            // 获取【发送短信验证码】按钮
            WebElement sendMessageButton = driver.findElement(By.xpath("//a[@class=\"send-btn-link\"]"));
            // 发送短信验证码
            sendMessageButton.click();
            // 每隔3秒检测是否跳转到饿了么首页，检测20次，如果还没跳转就需要人工介入分析原因
            int count = 20;
            WebElement footer = null;
            while (count > 0) {
                footer = driver.findElement(By.tagName("footer"));
                if (footer != null) break;
                Thread.sleep(3000);
                count--;
            }
            if (footer == null) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("登录失败"));
            // 跳转到订单列表页面
            driver.get("https://h5.ele.me/order/");
            int index = 0;
            // 获取【订单卡片】列表
            List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
            for (WebElement orderCard : orderCards) {
                EleBillDto eleBillDto = generateBill(driver, orderCard);
                Date consumeTime = eleBillDto.getConsumeTime();
                // 查找与爬取的饿了么账单时间相近，价格相等的记录，然后对账
//                QuickBill quickBill = quickBillService.selectNearQuickBill(eleBillDto.getConsumeTime(), eleBillDto.getPrice());

                QuickBill quickBill = quickBillService.getOne(Wrappers.<QuickBill>lambdaQuery()
                        .eq(QuickBill::getType, BillTypeEnum.EXPENSES)
                        .eq(QuickBill::getStatus, QuickBillStatusEnum.TO_BE_RECONCILED)
                        .eq(QuickBill::getMoney, eleBillDto.getPrice())
                        .between(QuickBill::getConsumeTime, DateTimeUtils.minusMinute(consumeTime, 1), DateTimeUtils.plusMinute(consumeTime, 1)));
                if (quickBill == null) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("没找到快捷账单"));
//                oldQuickBill.setStatus(QuickBillStatusEnum.RECONCILED);
//                quickBillService.updateById(oldQuickBill);

                quickBill.setCounterpartyName(eleBillDto.getCounterpartyName());


                Bill bill = new Bill();
                BeanUtils.copyProperties(quickBill, bill);
                String threeMealsName = ReconciliationUtils.parseThreeMealsName(quickBill.getConsumeTime());
                BillCategory category = billCategoryService.getOne(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getName, threeMealsName));
                bill.setCategoryId(category.getId());
                Counterparty parentCounterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, "饿了么平台商户"));
                Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getParentId, parentCounterparty.getId()).eq(Counterparty::getName, quickBill.getCounterpartyName()));
                if (counterparty == null) {
                    counterparty = new Counterparty();
                    counterparty.setName(quickBill.getCounterpartyName());
                    counterparty.setParentId(parentCounterparty.getId());
                    counterparty.setIsLeaf(IsEnum.YES);
                    counterpartyService.save(counterparty);
                    counterparty.setPath(parentCounterparty.getPath() + "," + counterparty.getId());
                    counterpartyService.updateById(counterparty);
                }
                bill.setCounterpartyId(counterparty.getId());
                Platform platform = platformService.getOne(Wrappers.<Platform>lambdaQuery().eq(Platform::getName, "饿了么外卖APP"));
                bill.setPlatformId(platform.getId());
                PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().eq(PayType::getName, "花呗"));
                bill.setPayTypeId(payType.getId());
                bill.setType(BillTypeEnum.EXPENSES);
                bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
                bill.setStatus(BillStatusEnum.TO_BE_ARCHIVED);
                billService.save(bill);
                quickBill.setBillId(bill.getId());
                quickBillService.updateById(quickBill);
                billItemService.saveBatch(bill, eleBillDto.getBillItems());
            }


        } catch (Exception e) {
            e.printStackTrace();
            throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage(e.getMessage()));
        }
    }

    @Override
    @Transactional(rollbackFor = BaseException.class)
    public void reconciliationEleFruitOrder(JsonNode jsonNode) {
        try {
            jsonNode = jsonNode.get("data").get("data");
            // 1. build QuickBill.
            QuickBill quickBill = new QuickBill();
            quickBill.setType(BillTypeEnum.EXPENSES);
            quickBill.setStatus(QuickBillStatusEnum.TO_BE_RECONCILED);
            quickBill.setConsumeTime(DateTimeUtils.parse(jsonNode.get("orderCreateTime").asText() + ":00"));
            quickBill.setMoney(new BigDecimal(jsonNode.get("paidAmountActual").asText()));
            quickBill.setCounterpartyName(jsonNode.get("restaurantName").asText());
            JsonNode goods = jsonNode.get("basket").get("group");
            quickBill.setDesc("共" + goods.size() + "件商品：" + goods.get(0).get("name").asText());
            // 2. build BillItem list.
            List<BillItemParam> billItems = buildEleFruitBillItems(goods);
            // 3. fix QuickBill.
            ObjectMapper om = new ObjectMapper();
            om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
            quickBill.setNote(om.writerWithDefaultPrettyPrinter().writeValueAsString(billItems));
            // 4. save QuickBill.
            saveEleQuickBill(quickBill);
            // 5. reconcile.
            reconciliationEleOrder(quickBill, billItems);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 构建饿了么果蔬订单项，填充到快捷账单的备注中，并返回订单项集合
     */
    private List<BillItemParam> buildEleFruitBillItems(JsonNode goods) {
        List<BillItemParam> billItemArrayList = new ArrayList<>(goods.size());
        for (JsonNode good : goods) {
            BillItemParam billItem = new BillItemParam();
            String productName = good.get("name").asText();
            Product product = productService.getOne(Wrappers.<Product>lambdaQuery().eq(Product::getName, productName));
            if (product != null) {
                billItem.setProductIdOrName(product.getId());
            } else {
                billItem.setProductIdOrName(productName);
            }
            BigDecimal price = null;
            if (good.get("price") != null) {
                price = new BigDecimal(good.get("price").asText());
                if (price.compareTo(BigDecimal.ZERO) <= 0) continue;
                billItem.setPayableOrReceivableAmount(price);
            }
            billItem.setPaymentOrReceiptsAmount(new BigDecimal(good.get("currentPrice").asText()));
            BillItemSkuRelVo billItemSkuRelVo = new BillItemSkuRelVo();
            billItemSkuRelVo.setSkuPrice(price);
            Sku sku = skuService.getOne(Wrappers.<Sku>lambdaQuery().eq(Sku::getUnit, "份"));
            billItemSkuRelVo.setSkuIdOrUnit(sku.getId());
            billItemSkuRelVo.setSkuQuantity(good.get("quantity").asInt());
            billItem.setBillItemSkus(Collections.singletonList(billItemSkuRelVo));
            billItemArrayList.add(billItem);
        }
        return billItemArrayList;
    }

    private EleBillDto generateBill(WebDriver driver, WebElement orderCard) throws ParseException, JsonProcessingException {
        EleBillDto eleBill = new EleBillDto();
        eleBill.setType(ThirdpartyBillTypeEnum.ELE);
        // 获取商家名称
        WebElement titleNameContent = orderCard.findElement(By.cssSelector(".title>.name>.content"));
        String counterpartyName = titleNameContent.getText();
        eleBill.setCounterpartyName(counterpartyName);
        // 获取消费时间
        WebElement datetime = orderCard.findElement(By.cssSelector(".datetime"));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date consumeTime = df.parse(datetime.getText());
        eleBill.setConsumeTime(consumeTime);
        // 获取账单描述
        List<WebElement> details = orderCard.findElements(By.cssSelector(".ordercard-detail>.detail>span"));
        StringBuilder products = new StringBuilder();
        for (WebElement detail : details) {
            products.append(detail.getText());
        }
        String desc = products.toString();
        eleBill.setDesc(desc);
        // 获取账单价格
        WebElement priceElement = orderCard.findElement(By.cssSelector(".ordercard-detail>.price"));
        BigDecimal price = new BigDecimal(priceElement.getText().substring(1));
        eleBill.setPrice(price);
        List<ThirdpartyBill> list = Collections.emptyList();
        if (CollectionUtils.isEmpty(list)) {
            // 获取账单详情div元素
            WebElement orderDetailElement = orderCard.findElement(By.cssSelector(".ordercard-detail"));
            orderDetailElement.click();

            // 获取订单详情
            WebElement restaurantElement = driver.findElement(By.xpath("//div[@class=\"restaurant-card\"]"));
            // 商家名称
            WebElement counterpartyNameElement = restaurantElement.findElement(By.cssSelector(".head>.name-wrap>.name"));
            counterpartyName = counterpartyNameElement.getText();
            eleBill.setCounterpartyName(counterpartyName);

            // 商品列表
            List<WebElement> productElements = restaurantElement.findElements(By.cssSelector(".product-list>.cart-item>.product-item"));
            List<EleBillItemDto> eleBillItems = new ArrayList<>(productElements.size());
            for (WebElement productElement : productElements) {
                EleBillItemDto eleBillItem = new EleBillItemDto();
                // 商品名称
                WebElement productNameElement = productElement.findElement(By.cssSelector(".profile>.name"));
                String productName = productNameElement.getText();
                eleBillItem.setProductName(productName);
                // 商品数量
                WebElement quantityElement = productElement.findElement(By.cssSelector(".quantity"));
                Integer quantity = Integer.valueOf(quantityElement.getText().substring(1));
                eleBillItem.setQuantity(quantity);
                // 商品金额
                WebElement numberElement = productElement.findElement(By.cssSelector(".number"));
                BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
                eleBillItem.setAmount(amount);

                eleBillItems.add(eleBillItem);
            }

            // 餐盒费/配送费等额外信息，在账单系统中均被认为是商品
            List<WebElement> otherElements = restaurantElement.findElements(By.cssSelector(".listitem>.product-item"));
            for (WebElement otherElement : otherElements) {
                EleBillItemDto eleBillItem = new EleBillItemDto();
                // 名称
                WebElement nameElement = otherElement.findElement(By.cssSelector(".name"));
                String name = nameElement.getText();
                eleBillItem.setProductName(name);

                List<WebElement> quantityPriceElements = otherElement.findElements(By.cssSelector(".price-wrap>.quantity"));
                if (CollectionUtils.isNotEmpty(quantityPriceElements)) {
                    // 数量
                    Integer quantity = Integer.valueOf(quantityPriceElements.get(0).getText().substring(1));
                    eleBillItem.setQuantity(quantity);
                    // 金额
                    WebElement numberElement = otherElement.findElement(By.cssSelector(".price-wrap>.number"));
                    BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
                    eleBillItem.setAmount(amount);
                } else {
                    List<WebElement> numberElement = otherElement.findElements(By.cssSelector(".number"));
                    if (CollectionUtils.isNotEmpty(numberElement)) {
                        // 金额
                        BigDecimal amount = new BigDecimal(numberElement.get(0).getText().substring(1));
                        eleBillItem.setAmount(amount);
                    }
                }
            }
            eleBill.setEleBillItems(eleBillItems);

            // 折扣
            List<WebElement> discountElements = restaurantElement.findElements(By.cssSelector(".price>.product-item"));
            if (CollectionUtils.isNotEmpty(discountElements)) {
                List<EleBillDiscountDto> eleBillDiscounts = new ArrayList<>(discountElements.size());
                for (WebElement discountElement : discountElements) {
                    EleBillDiscountDto eleBillDiscount = new EleBillDiscountDto();
//                            WebElement nameElement = discountElement.findElement(By.cssSelector(".name")).findElement(By.xpath("//span"));
                    WebElement nameElement = discountElement.findElement(By.xpath("//span[@class=\"name\"]/span"));
                    String name = nameElement.getText();
                    eleBillDiscount.setDesc(name);

                    StringBuilder sb = new StringBuilder();
                    WebElement amountElement = discountElement.findElement(By.cssSelector(".discount"));
                    List<WebElement> spanElements = amountElement.findElements(By.xpath("span"));
                    for (WebElement spanElement : spanElements) {
                        sb.append(spanElement.getText());
                    }
                    sb.append(amountElement.getText());
                    eleBillDiscount.setAmount(sb.toString());
                    eleBillDiscounts.add(eleBillDiscount);
                }
                eleBill.setEleBillDiscounts(eleBillDiscounts);
            }

            // 配送信息
            WebElement deliveryElement = driver.findElement(By.cssSelector(".detailcard>.detailcard-delivery"));
            String deliveryHTML = deliveryElement.getAttribute("outerHTML");
            eleBill.setDeliveryHtml(deliveryHTML);

            // 订单信息
            WebElement orderElement = driver.findElement(By.cssSelector(".detailcard>.detailcard-order"));
            String orderHTML = orderElement.getAttribute("outerHTML");
            eleBill.setOrderHtml(orderHTML);

            List<WebElement> orderElements = orderElement.findElements(By.cssSelector(".cardlist>.listitem"));
            for (WebElement orderElement1 : orderElements) {
                WebElement span = orderElement1.findElement(By.xpath("//span"));
                if ("订单号：".equals(span.getText())) {
                    eleBill.setThirdpartyId(orderElement1.getText().trim());
                    break;
                }
            }
        }
        String retS = new ObjectMapper().writeValueAsString(eleBill);
        // todo 存进记录表
        log.info("###### 爬取结果：" + retS);
        return eleBill;
    }

}
