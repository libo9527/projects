package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.mapper.PlatformMapper;
import com.gzhennaxia.bill.query.PlatformQuery;
import com.gzhennaxia.bill.service.IPlatformService;
import com.gzhennaxia.bill.vo.PlatformNodeVo;
import com.gzhennaxia.bill.vo.PlatformVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements IPlatformService {

    @Override
    public List<PlatformNodeVo> getPlatformNodesByParentId(Integer parentId) {
        return baseMapper.getPlatformNodesByParentId(parentId).stream().map(PlatformNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public List<PlatformNodeVo> getSourceNodesByPath(String path) {
        List<Integer> sourceIds = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<Platform> list = baseMapper.selectBatchIds(sourceIds);
        return list.stream().map(PlatformNodeVo::new).collect(Collectors.toList());
    }


    @Override
    public IPage<PlatformNodeVo> selectPlatformNodeVoPage(PlatformQuery query) {
        Page<PlatformNodeVo> page = new Page<>(query.getCurrent(), query.getSize());
        return baseMapper.selectPlatformNodeVoPage(page, query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(PlatformVo platformVo) {
        Integer currentId = platformVo.getId();
        Integer parentId = platformVo.getParentId();
        Platform current = new Platform(platformVo);
        // 如果 id 为空，并且 parentId 不为空，则表示新增；否则表示更新
        if (currentId == null && parentId != null) {
            Platform parent = baseMapper.selectById(parentId);
            baseMapper.insert(current);
            currentId = current.getId();
            if (parent == null) {
                current.setPath("0," + current.getId());
            } else {
                parent.setIsLeaf(IsEnum.NO);
                baseMapper.updateById(parent);
                current.setPath(parent.getPath() + "," + current.getId());
            }
        }
        // 新增后需要更新path，因此新增和修改都需要该语句
        baseMapper.updateById(current);
    }

    @Override
    public void delete(PlatformVo platformVo) {
        assert platformVo.getId() != null;
        Platform platform = new Platform(platformVo);
        platform.setDelFlag(IsEnum.YES);
        baseMapper.updateById(platform);
        deleteChildren(platform.getId());
    }

    @Override
    public List<TreeNodeVo> getSelectOptions(Integer id) {
        Platform platform = baseMapper.selectById(id);
        String path = platform.getPath();
        List<Integer> ids = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<Platform> list = baseMapper.selectBatchIds(ids);

        TreeNodeVo root = new TreeNodeVo();
        root.setValue(0);

        TreeNodeVo parent = root;
        Iterator<Platform> iterator = list.iterator();
        while (iterator.hasNext()) {
            Platform next = iterator.next();
            if (next.getParentId().equals(parent.getValue())) {
                List<TreeNodeVo> children = parent.getChildren();
                if (CollectionUtils.isEmpty(children)) {
                    children = new ArrayList<>(1);
                    parent.setChildren(children);
                }
                TreeNodeVo child = new TreeNodeVo(next);
                children.add(child);
                iterator.remove();
                parent = child;
            }
        }
        return root.getChildren();
    }

    @Async
    void deleteChildren(Integer parentId) {
        List<Platform> children = baseMapper.selectList(Wrappers.<Platform>lambdaQuery().eq(Platform::getParentId, parentId));
        children.forEach(child -> deleteChildren(child.getId()));
        baseMapper.update(new Platform(), Wrappers.<Platform>lambdaUpdate().set(Platform::getDelFlag, IsEnum.YES).eq(Platform::getId, parentId));
    }
}
