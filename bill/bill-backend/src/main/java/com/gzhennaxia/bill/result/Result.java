package com.gzhennaxia.bill.result;

import com.gzhennaxia.bill.exception.BaseException;
import lombok.Data;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.List;

import static com.gzhennaxia.bill.result.CodeMessageEnum.INVALIDATION_ERROR;

@Data
public class Result<T> {

    private Integer code;

    private T data;

    private String message;

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(CodeMessageEnum codeMessageEnum) {
        this.code = codeMessageEnum.getCode();
        this.message = codeMessageEnum.getMessage();
    }

    public static Result<Object> success() {
        return new Result<>(CodeMessageEnum.SUCCESS);
    }

    public static Result<Object> success(Object data) {
        Result<Object> success = success();
        success.setData(data);
        return success;
    }

    public static Result<Object> error(CodeMessageEnum codeMessageEnum) {
        return new Result<>(codeMessageEnum);
    }

    public static Result<Object> error(CodeMessageEnum codeMessageEnum, String message) {
        Result<Object> error = new Result<>(codeMessageEnum);
        error.setMessage(error.getMessage() + ", " + message);
        return error;
    }

    public static Result<Object> error(BaseException e) {
//        if (!StringUtils.isEmpty(e.getExtraMessage())) return error(e.getCodeMessage(), e.getExtraMessage());
        return error(e.getCodeMessage());
    }

    public static Result<Object> error(MethodArgumentNotValidException e) {
        String msg = "";
        List<FieldError> fieldErrors = e.getBindingResult().getFieldErrors();
        if (!CollectionUtils.isEmpty(fieldErrors)) {
            msg = fieldErrors.get(0).toString();
        }
        Result<Object> error = new Result<>(INVALIDATION_ERROR);
        error.setMessage(error.getMessage() + ": " + msg);
        return error;
    }
}
