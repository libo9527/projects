package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.PayTypeMapper;
import com.gzhennaxia.bill.query.PayTypeQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IPayTypeService;
import com.gzhennaxia.bill.vo.PayTypeNodeVo;
import com.gzhennaxia.bill.vo.PayTypeVo;
import com.gzhennaxia.bill.vo.TreeNodeMoveVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lib
 */
@Service
@RequiredArgsConstructor
public class PayTypeServiceImpl extends ServiceImpl<PayTypeMapper, PayType> implements IPayTypeService {

    private final IBillService billService;

    @Override
    public List<PayTypeNodeVo> getPayTypeNodesByParentId(Integer parentId) {
        return baseMapper.getPayTypeNodesByParentId(parentId).stream().map(PayTypeNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public List<PayTypeNodeVo> getPayTypeNodesByPath(String path) {
        List<Integer> ids = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<PayType> list = baseMapper.selectBatchIds(ids);
        return list.stream().map(PayTypeNodeVo::new).collect(Collectors.toList());
    }


    @Override
    public IPage<PayTypeNodeVo> selectPayTypeNodeVoPage(PayTypeQuery query) {
        Page<PayTypeNodeVo> page = new Page<>(query.getCurrent(), query.getSize());
        return baseMapper.selectPayTypeNodeVoPage(page, query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(PayTypeVo payTypeVo) {
        Integer currentId = payTypeVo.getId();
        Integer parentId = payTypeVo.getParentId();
        PayType current = new PayType(payTypeVo);
        // 如果 id 为空，并且 parentId 不为空，则表示新增；否则表示更新
        if (currentId == null && parentId != null) {
            PayType parent = baseMapper.selectById(parentId);
            if (parent != null && parent.getIsLeaf().equals(IsEnum.YES)) {
                List<Bill> list = billService.list(Wrappers.<Bill>lambdaQuery().eq(Bill::getPayTypeId, parentId));
                if (CollectionUtils.isNotEmpty(list)) {
                    throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("父节点下存在账单，不可新增子节点"));
                }
            }
            baseMapper.insert(current);
            currentId = current.getId();
            if (parent == null) {
                current.setPath("0," + current.getId());
            } else {
                parent.setIsLeaf(IsEnum.NO);
                baseMapper.updateById(parent);
                current.setPath(parent.getPath() + "," + current.getId());
            }
        }
        // 新增后需要更新path，因此新增和修改都需要该语句
        baseMapper.updateById(current);
    }

    @Override
    public void delete(PayTypeVo payTypeVo) {
        assert payTypeVo.getId() != null;
        PayType payType = new PayType(payTypeVo);
        payType.setDelFlag(IsEnum.YES);
        baseMapper.updateById(payType);
        deleteChildren(payType.getId());
    }

    @Override
    public List<TreeNodeVo> getSelectOptions(Integer id) {
        PayType payType = baseMapper.selectById(id);
        String path = payType.getPath();
        List<Integer> ids = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<PayType> list = baseMapper.selectBatchIds(ids);

        TreeNodeVo root = new TreeNodeVo();
        root.setValue(0);

        TreeNodeVo parent = root;
        Iterator<PayType> iterator = list.iterator();
        while (iterator.hasNext()) {
            PayType next = iterator.next();
            if (next.getParentId().equals(parent.getValue())) {
                List<TreeNodeVo> children = parent.getChildren();
                if (CollectionUtils.isEmpty(children)) {
                    children = new ArrayList<>(1);
                    parent.setChildren(children);
                }
                TreeNodeVo child = new TreeNodeVo(next);
                children.add(child);
                iterator.remove();
                parent = child;
            }
        }
        return root.getChildren();
    }

    @Async
    void deleteChildren(Integer parentId) {
        List<PayType> children = baseMapper.selectList(Wrappers.<PayType>lambdaQuery().eq(PayType::getParentId, parentId));
        children.forEach(child -> deleteChildren(child.getId()));
        baseMapper.update(new PayType(), Wrappers.<PayType>lambdaUpdate().set(PayType::getDelFlag, IsEnum.YES).eq(PayType::getId, parentId));
    }

    @Override
    public void move(TreeNodeMoveVo treeNodeMoveVo) {
        Integer parentId = treeNodeMoveVo.getParentId();
        List<Bill> list = billService.list(Wrappers.<Bill>lambdaQuery().eq(Bill::getPayTypeId, treeNodeMoveVo.getParentId()));
        if (CollectionUtils.isNotEmpty(list)) {
            throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("父节点下存在账单，不可新增子节点"));
        }
        List<Integer> childIds = treeNodeMoveVo.getChildIds();
        PayType parent = baseMapper.selectById(parentId);
        for (Integer childId : childIds) {
            PayType child = baseMapper.selectById(childId);
            updateHierarchicalStructure(parent, child);
        }
    }

    /**
     * 递归更新层级结构关系
     */
    private void updateHierarchicalStructure(PayType parent, PayType child) {
        child.setParentId(parent.getId());
        child.setPath(parent.getPath() + "," + child.getId());
        baseMapper.updateById(child);
        parent.setIsLeaf(IsEnum.NO);
        baseMapper.updateById(parent);
        List<PayType> children = baseMapper.selectList(Wrappers.<PayType>lambdaQuery().eq(PayType::getParentId, child.getId()));
        if (CollectionUtils.isNotEmpty(children)) {
            for (PayType child1 : children) {
                updateHierarchicalStructure(child, child1);
            }
        }
    }

}
