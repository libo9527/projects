package com.gzhennaxia.bill.controller;


import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IDailySummaryReportService;
import com.gzhennaxia.bill.utils.DateTimeUtils;
import com.gzhennaxia.bill.vo.DailySummaryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gzhennaxia
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/summary")
public class SummaryController {

    private final IDailySummaryReportService IDailySummaryReportService;
    private final IBillService billService;

    /**
     * 日、周、月、年支出汇总
     */
    @GetMapping("/totalExpenses")
    public Map<String, BigDecimal> totalExpenses() {
        Map<String, BigDecimal> map = new HashMap<>(4);
        Date now = new Date();
        map.put("dailyTotalExpenses", billService.sumExpenses(DateTimeUtils.getDayStart(now), DateTimeUtils.getDayEnd(now)));
        map.put("weekTotalExpenses", billService.sumExpenses(DateTimeUtils.getWeekStart(now), DateTimeUtils.getWeekEnd(now)));
        map.put("monthTotalExpenses", billService.sumExpenses(DateTimeUtils.getMonthStart(now), DateTimeUtils.getMonthEnd(now)));
        map.put("yearTotalExpenses", billService.sumExpenses(DateTimeUtils.getYearStart(now), DateTimeUtils.getYearEnd(now)));
        return map;
    }

    /**
     * Income and expenditure records for the last 30 days
     */
    @GetMapping("/selectLastDays")
    public List<DailySummaryVo> selectLastDays(@RequestParam Integer days) {
        return billService.selectLastDays(days);
    }
}
