package com.gzhennaxia.bill.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.dto.QuickBillDto;
import com.gzhennaxia.bill.dto.WXCarCodeDto;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.*;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.exception.ReconciliationException;
import com.gzhennaxia.bill.listener.QuickBillDtoListener;
import com.gzhennaxia.bill.listener.WXCarCodeListener;
import com.gzhennaxia.bill.mapper.QuickBillMapper;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.query.QuickBillQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.*;
import com.gzhennaxia.bill.strategy.reconciliation.ReconciliationContext;
import com.gzhennaxia.bill.utils.BeanConverter;
import com.gzhennaxia.bill.utils.FileUtils;
import com.gzhennaxia.bill.utils.ValidatorUtils;
import com.gzhennaxia.bill.vo.ImportResultVo;
import com.gzhennaxia.bill.vo.QuickBillVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.nio.cs.ext.GBK;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class QuickBillServiceImpl extends ServiceImpl<QuickBillMapper, QuickBill> implements IQuickBillService {

    private final ImportHistoryService importHistoryService;
    private final IBillCategoryService billCategoryService;
    private final IPayTypeService payTypeService;
    private final ICounterpartyService counterpartyService;
    private final IPlatformService platformService;
    private final IQuickBillItemService quickBillItemService;
    private final IBillService billService;

    @Override
    public ImportResultVo importFromFile(Integer importHistoryId, IsEnum rightNow) {
        ImportHistory importHistory = importHistoryService.getById(importHistoryId);
        File file = new File(importHistory.getFilePath());
        if (!file.exists()) {
            throw new BaseException(CodeMessageEnum.FILE_NOT_EXIST);
        }
        if (IsEnum.YES.equals(rightNow)) {
            importFromFile(file, importHistory);
        } else {
            asyncImportFromFile(file, importHistory);
        }
        return new ImportResultVo(importHistory);
    }

    @Override
    public IPage<QuickBillVo> search(QuickBillQuery query) {
        Page<QuickBillVo> pageRet = new Page<>(query.getCurrent(), query.getSize());
        if (!StringUtils.isEmpty(query.getDesc()))
            query.setDesc("%" + query.getDesc().trim() + "%");
        if (!StringUtils.isEmpty(query.getCounterpartyName()))
            query.setCounterpartyName("%" + query.getCounterpartyName().trim() + "%");
        return baseMapper.pageSearch(pageRet, query);
    }

    @Async
    void asyncImportFromFile(File file, ImportHistory importHistory) {
        importFromFile(file, importHistory);
    }

    private void importFromFile(File file, ImportHistory importHistory) {
        String fileName = file.getName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        if ("xlsx".equals(suffix)) {
            EasyExcel.read(file, QuickBillDto.class, new QuickBillDtoListener(baseMapper, importHistoryService, importHistory)).sheet("records").doRead();
        } else if ("csv".equals(suffix)) {
            importFromCsv(file, importHistory);
        }
    }

    private void importFromCsv(File file, ImportHistory importHistory) {
        CSVParser parser = null;
        CSVPrinter printer = null;
        try {
            parser = CSVParser.parse(file, new GBK(), CSVFormat.DEFAULT);
            String fileName = file.getName();
            fileName = fileName.substring(0, fileName.lastIndexOf(".csv"));
            String absolutePath = file.getAbsolutePath();
//            String replace = absolutePath.replace(fileName, fileName + "-result");
            printer = new CSVPrinter(new FileWriter(absolutePath.replace(fileName, fileName + "-result")), CSVFormat.DEFAULT);

            List<CSVRecord> records = parser.getRecords();
            String title = records.get(0).get(0);
            BillFileTypeEnum type;
            if (title.contains("支付宝（中国）网络技术有限公司  电子客户回单")) {
                type = BillFileTypeEnum.ALIPAY;
            } else {
                parser = CSVParser.parse(file, StandardCharsets.UTF_8, CSVFormat.DEFAULT);
                records = parser.getRecords();
                title = records.get(0).get(0);
                if (title.contains("微信支付账单明细")) {
                    type = BillFileTypeEnum.WECHAT;
                } else {
                    throw new ReconciliationException(CodeMessageEnum.QUICK_BILL_IMPORT_FAILED, "失败原因：未知结构文件！");
                }
            }
            // 导入成功总数
            int successCount = 0;
            // 重复总数：有多少条记录是数据库已经存在的
            int repeatCount = 0;
            // 失败条数
            int failedCount = 0;
            // 导入总数
            int total = 0;
            for (CSVRecord record : records) {
                try {
                    QuickBill newBill = BeanConverter.convert2QuickBill(record, type);
                    QuickBill oldBill = baseMapper.selectOne(Wrappers.<QuickBill>lambdaQuery().select(QuickBill::getId, QuickBill::getConsumeTime).eq(QuickBill::getConsumeTime, newBill.getConsumeTime()).eq(QuickBill::getPayOrderId, newBill.getPayOrderId()));
                    if (oldBill == null) {
                        baseMapper.insert(newBill);
                        successCount++;
                    } else {
                        repeatCount++;
                        log.warn("重复导入：old - {}, new - {}", oldBill, newBill);
                        printNote(printer, record, "重复导入，oldId：" + oldBill.getId());
                    }
                    total++;
                } catch (ReconciliationException e) {
                    log.error("导入失败：record = {}", record, e);
                    printNote(printer, record, e.getMessage());
                    failedCount++;
                } catch (Exception e) {
                    log.error("导入失败：record = {}", record, e);
                    printNote(printer, record, "快捷账单导入失败，失败原因：未知错误");
                    failedCount++;
                }
            }
            importHistory.setSuccessCount(successCount);
            importHistory.setRepeatCount(repeatCount);
            importHistory.setFailedCount(failedCount);
            importHistory.setTotal(total);
            importHistory.setUploadTime(new Date());
            importHistory.setStatus(ImportHistoryStatusEnum.IMPORTED);
            importHistoryService.updateById(importHistory);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (parser != null) parser.close();
                if (printer != null) printer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void printNote(CSVPrinter printer, CSVRecord record, String note) throws IOException {
        List<String> ss = new ArrayList<>(record.size() + 1);
        record.forEach(ss::add);
        ss.add(note);
        printer.printRecord(ss);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoReconciliation(Integer quickBillId) {
        QuickBill quickBill = baseMapper.selectById(quickBillId);
        if (new ReconciliationContext().reconciliation(quickBill)) return;
        autoReconciliation(quickBill);
    }

    private void autoReconciliation(QuickBill quickBill) {
        ValidatorUtils.assertNotNull(quickBill.getConsumeTime(), CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：消费时间为空", baseMapper, quickBill.getId());
        BillCategory category = billCategoryService.getOne(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getName, quickBill.getCategoryName()).eq(BillCategory::getIsLeaf, IsEnum.YES));
        ValidatorUtils.assertNotNull(category, CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：系统不存在名为【" + quickBill.getCategoryName() + "】的最底级分类", baseMapper, quickBill.getId());
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().eq(PayType::getName, quickBill.getPayTypeName()).eq(PayType::getIsLeaf, IsEnum.YES));
        ValidatorUtils.assertNotNull(payType, CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：系统不存在名为【" + quickBill.getPayTypeName() + "】的最底级支付方式", baseMapper, quickBill.getId());
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, quickBill.getCounterpartyName()).eq(Counterparty::getIsLeaf, IsEnum.YES));
        ValidatorUtils.assertNotNull(counterparty, CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：系统不存在名为【" + quickBill.getCounterpartyName() + "】的最底级交易对方", baseMapper, quickBill.getId());
        Platform platform = platformService.getOne(Wrappers.<Platform>lambdaQuery().eq(Platform::getName, quickBill.getPlatformName()).eq(Platform::getIsLeaf, IsEnum.YES));
        ValidatorUtils.assertNotNull(platform, CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：系统不存在名为【" + quickBill.getPlatformName() + "】的最底级交易平台", baseMapper, quickBill.getId());
        List<QuickBillItem> quickBillItems = quickBillItemService.list(Wrappers.<QuickBillItem>lambdaQuery().eq(QuickBillItem::getQuickBillId, quickBill.getId()));
        Bill bill = new Bill();
        if (CollectionUtils.isNotEmpty(quickBillItems)) {
            // TODO
        }
        BeanUtils.copyProperties(quickBill, bill);
        bill.setStatus(BillStatusEnum.TO_BE_ARCHIVED);
        bill.setCategoryId(category.getId());
        bill.setPayTypeId(payType.getId());
        bill.setCounterpartyId(counterparty.getId());
        bill.setPlatformId(platform.getId());
        bill.setType(quickBill.getType());
        bill.setPayableOrReceivableAmount(quickBill.getMoney());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        Bill one = billService.getOne(Wrappers.<Bill>lambdaQuery().eq(Bill::getConsumeTime, bill.getConsumeTime()).eq(Bill::getType, bill.getType()));
        ValidatorUtils.assertNull(one, CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：系统已存在消费时间为【" + quickBill.getConsumeTime() + "】的账单记录", baseMapper, quickBill.getId());
        billService.save(bill);
        baseMapper.updateById(QuickBill.builder().id(quickBill.getId()).status(QuickBillStatusEnum.RECONCILED).build());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoReconciliation(List<Integer> quickBillIds) {
        List<QuickBill> quickBills = baseMapper.selectBatchIds(quickBillIds);
        new ReconciliationContext().reconciliation(quickBills);
        if (CollectionUtils.isNotEmpty(quickBills)) {
            for (QuickBill quickBill : quickBills) {
                autoReconciliation(quickBill);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void reconciliation(Integer quickBillId, BillParam billParam) {
        QuickBill quickBill = baseMapper.selectById(quickBillId);
        if (QuickBillStatusEnum.RECONCILED.equals(quickBill.getStatus()) && billParam.getId() == null)
            throw new ReconciliationException(CodeMessageEnum.RECONCILIATION_FAILED, "失败原因：快捷账单已对账，而正式账单不存在！");
        billService.save(billParam);
        quickBill.setStatus(QuickBillStatusEnum.RECONCILED);
        quickBill.setBillId(billParam.getId());
        baseMapper.updateById(quickBill);
    }

    @Override
    public QuickBillVo selectQuickBillDetailById(Integer id) {
        QuickBill quickBill = getById(id);
        if (quickBill == null) return null;
        return new QuickBillVo(quickBill);
    }

    @Override
    public void autoWXCarCodeReconciliation(File file) {
        if (!FileUtils.isExcel(file)) throw new ReconciliationException(CodeMessageEnum.RECONCILIATION_FAILED, "文件格式不正确");
        EasyExcel.read(file, WXCarCodeDto.class, new WXCarCodeListener(baseMapper)).sheet().doRead();
    }
}
