package com.gzhennaxia.bill.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 对账工具类
 *
 * @author lib
 */
public class ReconciliationUtils {

    /**
     * 解析三餐名
     */
    public static String parseThreeMealsName(Date consumeTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(consumeTime);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        String category;
        if (5 < hour && hour <= 10) {
            category = "早餐";
        } else if (10 < hour && hour <= 13) {
            category = "午餐";
        } else if (13 < hour && hour <= 19) {
            category = "晚餐";
        } else {
            category = "夜宵";
        }
        return category;
    }
}
