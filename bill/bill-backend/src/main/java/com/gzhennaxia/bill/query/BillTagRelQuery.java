package com.gzhennaxia.bill.query;

import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@Data
public class BillTagRelQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Integer tagId;


}
