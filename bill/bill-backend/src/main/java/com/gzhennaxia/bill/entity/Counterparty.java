package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.vo.CounterpartyVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Counterparty extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer parentId;

    private String name;

    private String path;

    private IsEnum isLeaf;

    private Integer status;

    public Counterparty(CounterpartyVo counterpartyVo) {
        BeanUtils.copyProperties(counterpartyVo, this);
    }
}
