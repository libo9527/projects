package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.entity.PayType;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.service.IPayTypeService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

/**
 * 深圳通对账策略
 *
 * @author lib
 */
public class ShenZhenTongReconciliationStrategy extends AbstractReconciliationStrategy {

    private final Bill shenZhenTongBill;
    private static final String DESC = "深圳通乘车码乘公交消费";
    private static final String COUNTERPARTY_NAME = "深圳通";


    public ShenZhenTongReconciliationStrategy() {
        ICounterpartyService counterpartyService = SpringBeanUtils.getBean(ICounterpartyService.class);
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, COUNTERPARTY_NAME));
        this.shenZhenTongBill = this.billService.getOne(Wrappers.<Bill>lambdaQuery()
                .eq(Bill::getDesc, DESC)
                .eq(Bill::getCounterpartyId, counterparty.getId())
                .last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        String desc = quickBill.getDesc();
        if (!DESC.equals(desc)) return false;
        String counterpartyName = quickBill.getCounterpartyName();
        return COUNTERPARTY_NAME.equals(counterpartyName);
    }

    @Override
    Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setCategoryId(shenZhenTongBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        IPayTypeService payTypeService = SpringBeanUtils.getBean(IPayTypeService.class);
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().like(PayType::getName, quickBill.getPayTypeName()).last("LIMIT 1"));
        bill.setPayTypeId(payType.getId());
        bill.setPlatformId(shenZhenTongBill.getPlatformId());
        bill.setCounterpartyId(shenZhenTongBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {

    }
}
