package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.ImportHistory;
import com.gzhennaxia.bill.mapper.ImportHistoryMapper;
import com.gzhennaxia.bill.service.ImportHistoryService;
import org.springframework.stereotype.Service;

/**
 * @author lib
 */
@Service
public class ImportHistoryServiceImpl extends ServiceImpl<ImportHistoryMapper, ImportHistory> implements ImportHistoryService {

    @Override
    public void insert(ImportHistory importHistory) {
        baseMapper.insert(importHistory);
    }
}
