package com.gzhennaxia.bill.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Sku;
import com.gzhennaxia.bill.mapper.SkuMapper;
import com.gzhennaxia.bill.query.SkuQuery;
import com.gzhennaxia.bill.service.ISkuService;
import com.gzhennaxia.bill.vo.SkuVo;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

    @Override
    public List<SkuVo> search(SkuQuery skuQuery) {
        String keyword = skuQuery.getKeyword();
        if (StringUtils.isEmpty(keyword) || StringUtils.isEmpty(keyword.trim()))
            return Collections.emptyList();
        return list(Wrappers.<Sku>lambdaQuery().like(Sku::getUnit, keyword.trim())).stream().map(SkuVo::new).collect(Collectors.toList());
    }
}
