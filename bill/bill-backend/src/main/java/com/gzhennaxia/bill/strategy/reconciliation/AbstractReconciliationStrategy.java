package com.gzhennaxia.bill.strategy.reconciliation;

import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * 摩拜单车对账策略
 *
 * @author lib
 */
public abstract class AbstractReconciliationStrategy implements ReconciliationStrategy {

    protected final IBillService billService;
    protected final IQuickBillService quickBillService;

    public AbstractReconciliationStrategy() {
        this.billService = SpringBeanUtils.getBean(IBillService.class);
        this.quickBillService = SpringBeanUtils.getBean(IQuickBillService.class);
    }

    @Override
    public void doReconciliation(QuickBill quickBill) {
        Bill bill = buildBill(quickBill);
        billService.save(bill);
        quickBill.setStatus(QuickBillStatusEnum.RECONCILED);
        quickBill.setBillId(bill.getId());
        quickBillService.updateById(quickBill);
        buildAndSaveBillItem(bill);
    }

    abstract Bill buildBill(QuickBill quickBill);

    abstract void buildAndSaveBillItem(Bill bill);

    @Override
    public void doReconciliation(List<QuickBill> quickBills) {
        if (CollectionUtils.isNotEmpty(quickBills)) quickBills.forEach(this::doReconciliation);
    }
}
