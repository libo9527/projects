package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.BillItemDiscountRelQuery;
import com.gzhennaxia.bill.service.IBillItemDiscountRelService;
import com.gzhennaxia.bill.vo.BillItemDiscountRelVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-item-discount-rel")
public class BillItemDiscountRelController {

    private final IBillItemDiscountRelService billItemDiscountRelService;

    @PostMapping("/search")
    public List<BillItemDiscountRelVo> search(BillItemDiscountRelQuery billItemDiscountRelQuery) {
       return billItemDiscountRelService.search(billItemDiscountRelQuery);
    }
}
