package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 账单类型
 *
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum BillTypeEnum implements BaseEnum {

    INCOME(1, "收入"), EXPENSES(-1, "支出");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private Integer code;

    private String message;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static BillTypeEnum getBillTypeEnum(Integer code) {
        for (BillTypeEnum item : values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        return null;
    }

    public static BillTypeEnum getBillTypeEnum(String message) {
        for (BillTypeEnum item : values()) {
            if (item.getMessage().equals(message)) {
                return item;
            }
        }
        return null;
    }
}
