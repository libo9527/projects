package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class BillItemSkuRel extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer billItemId;

    private Integer productId;

    private Integer skuId;

    private BigDecimal price;

    private Integer quantity;


    public BillItemSkuRel(BillItemSkuRelVo billItemSkuRelVo) {
        BeanUtils.copyProperties(billItemSkuRelVo, this);
        this.price = billItemSkuRelVo.getSkuPrice();
        this.quantity = billItemSkuRelVo.getSkuQuantity();
    }
}
