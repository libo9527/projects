package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.Discount;
import com.gzhennaxia.bill.query.DiscountQuery;
import com.gzhennaxia.bill.vo.DiscountVo;

import java.util.List;

public interface DiscountMapper extends BaseMapper<Discount> {
    List<DiscountVo> selectAll();

    IPage<DiscountVo> pageSearch(Page<DiscountVo> page, DiscountQuery query);

    List<Discount> searchByBillId(Integer billId);
}
