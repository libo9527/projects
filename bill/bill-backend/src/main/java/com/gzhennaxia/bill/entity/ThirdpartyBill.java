package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.ThirdpartyBillStatusEnum;
import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import com.gzhennaxia.bill.type_handler.MybatisPlusDateTimeTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ThirdpartyBill extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private ThirdpartyBillTypeEnum type;

    @TableField(typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date consumeTime;

    /**
     * 第三方主键ID
     */
    private String thirdpartyId;

    /**
     * 交易对方
     */
    private String counterpartyName;

    /**
     * 描述
     */
    private String desc;

    /**
     * 价格
     */
    private BigDecimal price;

    private String jsonData;

    private ThirdpartyBillStatusEnum status;


    public ThirdpartyBill(String thirdpartyId, String jsonData) {
        this.thirdpartyId = thirdpartyId;
        this.jsonData = jsonData;
    }
}
