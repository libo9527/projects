package com.gzhennaxia.bill.entity;

import java.math.BigDecimal;
import com.gzhennaxia.bill.entity.CommonColumn;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-05-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class QuickBillItem extends CommonColumn {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer quickBillId;

    private Integer productId;

    private Integer skuId;

    private BigDecimal price;

    private Integer quantity;

    private BigDecimal totalPrice;

    private Integer discountId;

    private String note;

    private Integer status;


}
