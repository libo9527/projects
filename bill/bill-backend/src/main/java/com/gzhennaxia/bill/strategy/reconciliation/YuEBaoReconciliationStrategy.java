package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.QuickBillStatusEnum;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 余额宝对账策略
 *
 * @author lib
 */
public class YuEBaoReconciliationStrategy extends AbstractReconciliationStrategy {

    private final IBillService billService;
    private final Bill yuEBaoBill;
    private final IQuickBillService quickBillService;


    public YuEBaoReconciliationStrategy() {
        this.billService = SpringBeanUtils.getBean(IBillService.class);
        this.quickBillService = SpringBeanUtils.getBean(IQuickBillService.class);
        this.yuEBaoBill = billService.getOne(Wrappers.<Bill>lambdaQuery().likeRight(Bill::getDesc, "余额宝").likeLeft(Bill::getDesc, "收益发放").last("LIMIT 1"));
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        String desc = quickBill.getDesc();
        return desc.startsWith("余额宝") && desc.endsWith("收益发放");
    }

    @Override
    public Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        bill.setType(BillTypeEnum.INCOME);
        bill.setCategoryId(yuEBaoBill.getCategoryId());
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setPayTypeId(yuEBaoBill.getPayTypeId());
        bill.setPlatformId(yuEBaoBill.getPlatformId());
        bill.setCounterpartyId(yuEBaoBill.getCounterpartyId());
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {

    }

    @Override
    public void doReconciliation(List<QuickBill> quickBills) {
        List<Bill> list = quickBills.stream().map(this::buildBill).collect(Collectors.toList());
        billService.saveBatch(list);
        quickBillService.update(Wrappers.<QuickBill>lambdaUpdate().set(QuickBill::getStatus, QuickBillStatusEnum.RECONCILED).in(QuickBill::getId, quickBills.stream().map(QuickBill::getId).collect(Collectors.toList())));
    }
}
