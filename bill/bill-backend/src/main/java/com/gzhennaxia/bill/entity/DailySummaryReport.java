package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author gzhennaxia
 */
@Data
public class DailySummaryReport {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Date date;

    private BigDecimal income;

    private BigDecimal expense;

}
