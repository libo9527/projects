package com.gzhennaxia.bill.vo;

import lombok.Data;

/**
 * @author lib
 */
@Data
public class DiscountVo {

    private Integer id;

    private String desc;
}
