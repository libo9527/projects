package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.entity.ImportHistory;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IQuickBillService;
import com.gzhennaxia.bill.service.IThirdpartyBillService;
import com.gzhennaxia.bill.service.ImportHistoryService;
import com.gzhennaxia.bill.vo.ImportHistoryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author lib
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {

    @Value("${file-upload-path}")
    private String fileUploadPath;

    private final ImportHistoryService importHistoryService;
    private final IQuickBillService quickBillService;
    private final IThirdpartyBillService thirdpartyBillService;

    @PostMapping("/upload")
    public ImportHistoryVo upload(MultipartFile file) throws IOException {
        File desc = new File(fileUploadPath + file.getOriginalFilename());
        if (desc.exists()) throw new BaseException(CodeMessageEnum.FILE_ALREADY_EXIST);
        file.transferTo(desc);
        ImportHistory importHistory = new ImportHistory(desc.getAbsolutePath());
        importHistoryService.insert(importHistory);
        System.out.println(desc.getAbsolutePath());
        System.out.println(file.getOriginalFilename());
        ImportHistoryVo importHistoryVo = new ImportHistoryVo(importHistory);
        importHistoryVo.setFileName(file.getOriginalFilename());
        return importHistoryVo;
    }

    /**
     * 导入乘车码记录
     */
    @PostMapping("/uploadWXCarCodeRecords")
    public void uploadWXCarCodeRecords(MultipartFile file) throws IOException {
        File desc = new File(fileUploadPath + file.getOriginalFilename());
        if (desc.exists()) throw new BaseException(CodeMessageEnum.FILE_ALREADY_EXIST);
        file.transferTo(desc);
        quickBillService.autoWXCarCodeReconciliation(desc);
        desc.delete();
    }

    /**
     * 导入饿了么订单记录
     */
    @GetMapping("/ele")
    public void importEleResponse() {
        thirdpartyBillService.importEleResponse();
    }
}
