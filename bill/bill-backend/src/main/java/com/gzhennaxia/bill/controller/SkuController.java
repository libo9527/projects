package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.SkuQuery;
import com.gzhennaxia.bill.service.ISkuService;
import com.gzhennaxia.bill.vo.SkuVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/sku")
public class SkuController {

    private final ISkuService skuService;

    @PostMapping("/search")
    public List<SkuVo> search(@RequestBody SkuQuery skuQuery) {
       return skuService.search(skuQuery);
    }
}
