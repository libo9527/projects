package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.ThirdpartyBillQuery;
import com.gzhennaxia.bill.service.IThirdpartyBillService;
import com.gzhennaxia.bill.vo.ThirdpartyBillVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/thirdparty-bill")
public class ThirdpartyBillController {

    private final IThirdpartyBillService thirdpartyBillService;

    @PostMapping("/search")
    public List<ThirdpartyBillVo> search(@RequestBody ThirdpartyBillQuery thirdpartyBillQuery) {
       return thirdpartyBillService.search(thirdpartyBillQuery);
    }

    /**
     * 爬取饿了么订单
     */
    @PostMapping("/crawl/ele")
    public void syncEle() {
       thirdpartyBillService.syncEle();
    }
}