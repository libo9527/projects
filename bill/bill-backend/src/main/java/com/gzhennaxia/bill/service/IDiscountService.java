package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.Discount;
import com.gzhennaxia.bill.query.DiscountQuery;
import com.gzhennaxia.bill.vo.DiscountVo;

import java.util.List;

public interface IDiscountService extends IService<Discount>  {
    List<DiscountVo> getAllDiscounts();

    IPage<DiscountVo> pageSearch(DiscountQuery discountQuery);

    List<Discount> searchByBillId(Integer billId);
}
