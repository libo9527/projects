package com.gzhennaxia.bill.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.query.CounterpartyQuery;
import com.gzhennaxia.bill.result.Result;
import com.gzhennaxia.bill.service.ICounterpartyService;
import com.gzhennaxia.bill.vo.CounterpartyNodeVo;
import com.gzhennaxia.bill.vo.CounterpartyVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/counterparty")
public class CounterpartyController {

    private final ICounterpartyService counterpartyService;

    @GetMapping("/children/{parentId}")
    public Result<?> getCounterpartyNodesByParentId(@PathVariable Integer parentId) {
        return Result.success(counterpartyService.getCounterpartyNodesByParentId(parentId));
    }

    @GetMapping("/path/{path}")
    public Result<?> getCategoryNodesByPath(@PathVariable String path) {
        return Result.success(counterpartyService.getCounterpartyNodesByPath(path));
    }

    @PostMapping("/search")
    public IPage<CounterpartyNodeVo> search(@RequestBody CounterpartyQuery query) {
        return counterpartyService.selectCounterpartyNodeVoPage(query);
    }

    @PostMapping("/save")
    public void save(@RequestBody CounterpartyVo counterpartyVo) {
        counterpartyService.save(counterpartyVo);
    }

    @PostMapping("/delete")
    public void delete(@RequestBody CounterpartyVo counterpartyVo) {
        counterpartyService.delete(counterpartyVo);
    }

    @GetMapping("/{id}")
    public Counterparty getById(@PathVariable Integer id) {
        return counterpartyService.getById(id);
    }

}
