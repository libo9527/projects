package com.gzhennaxia.bill.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author gzhennaxia
 */
@Data
public class DailySummaryReportVo {

    private Date date;

    private BigDecimal income;

    private BigDecimal expense;
}
