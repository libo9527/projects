package com.gzhennaxia.bill.strategy.reconciliation;

import com.gzhennaxia.bill.entity.QuickBill;

import java.util.List;

/**
 * 对账策略接口
 * @author lib
 */
public interface ReconciliationStrategy {

    /**
     * 校验当前策略是否有效
     */
    Boolean valid(QuickBill quickBill);

    void doReconciliation(QuickBill quickBill);

    void doReconciliation(List<QuickBill> list);
}
