package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.BillTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
public interface BillTagMapper extends BaseMapper<BillTag> {

    List<BillTag> getTagsByBillId(Integer billId);
}
