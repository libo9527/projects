package com.gzhennaxia.bill.type_handler;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author gzhennaxia
 */
@Component
@MappedJdbcTypes(value = JdbcType.TIMESTAMP, includeNullJdbcType = true)
@MappedTypes({Date.class})
public class MybatisPlusDateTimeTypeHandler implements TypeHandler<Date> {

    private Date parse(String json) {
        Date result = null;
        try {
            result = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(json);
        } catch (ParseException e) {
            try {
                result = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(json);
            } catch (ParseException ex) {
                try {
                    result = new SimpleDateFormat("yyyy-MM-dd").parse(json);
                } catch (ParseException exc) {
                    exc.printStackTrace();
                }
            }
        }
        return result;
    }

    private String format(Date obj) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(obj);
    }

    @Override
    public void setParameter(PreparedStatement ps, int i, Date parameter, JdbcType jdbcType) throws SQLException {
        ps.setString(i, format(parameter));
    }

    @Override
    public Date getResult(ResultSet rs, String columnName) throws SQLException {
        String dateTimeString = rs.getString(columnName);
        return dateTimeString != null ? parse(dateTimeString) : null;
    }

    @Override
    public Date getResult(ResultSet rs, int columnIndex) throws SQLException {
        String dateTimeString = rs.getString(columnIndex);
        return dateTimeString != null ? parse(dateTimeString) : null;
    }

    @Override
    public Date getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String dateTimeString = cs.getString(columnIndex);
        return dateTimeString != null ? parse(dateTimeString) : null;
    }
}
