package com.gzhennaxia.bill.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author lib
 */
@Getter
@AllArgsConstructor
public enum ThirdpartyBillStatusEnum {

    CREATE(0, "创建"),

    /**
     * 已生成正式账单
     */
    RECONCILED(5, "已对账");

    /**
     * @EnumValue MyBatis-Plus 通用枚举
     * https://baomidou.com/guide/enum.html
     * @JsonValue jackson 会在序列化时将 BillTypeEnum 类型的字段序列化为 message 的值
     */
    @EnumValue
    @JsonValue
    private final Integer code;
    private final String message;

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ThirdpartyBillStatusEnum getThirdpartyBillStatusEnum(Integer code) {
        if (code != null) {
            for (ThirdpartyBillStatusEnum item : values()) {
                if (item.getCode().equals(code)) {
                    return item;
                }
            }
        }
        return null;
    }
}
