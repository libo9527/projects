package com.gzhennaxia.bill.exception;

import com.gzhennaxia.bill.result.CodeMessageEnum;

/**
 * 对账异常类
 *
 * @author lib
 */
public class ReconciliationException extends BaseException {

    public ReconciliationException(CodeMessageEnum codeMessage, String s) {
        super(codeMessage, codeMessage.getMessage() + s);
    }
}
