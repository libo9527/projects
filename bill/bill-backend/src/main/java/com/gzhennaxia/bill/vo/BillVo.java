package com.gzhennaxia.bill.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gzhennaxia.bill.enums.BillStatusEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.serialize.BigDecimalSerializer;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author lib
 */
@Data
@ToString
public class BillVo {
    private Integer id;
    private Integer userId;
    private String desc;
    private String note;
    private BillStatusEnum status;
    private Date consumeTime;
    @JsonFormat(pattern = "0.00", shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal payableOrReceivableAmount;
    @JsonFormat(pattern = "0.00", shape = JsonFormat.Shape.STRING)
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal paymentOrReceiptsAmount;

    /**
     * 账单分类
     */
    private Integer categoryId;
    private String categoryName;
    private BillCategoryVo category;

    /**
     * 交易对方
     */
    private Integer counterpartyId;
    private String counterpartyName;
    private CounterpartyVo counterparty;

    /**
     * 交易平台
     */
    private Integer platformId;
    private String platformName;
    private PlatformVo platform;

    /**
     * 收支类型
     */
    private BillTypeEnum type;

    /**
     * 支付类型
     */
    private Integer payTypeId;
    private String payTypeName;
    private PayTypeVo payType;

    /**
     * 标记ID/NAME
     */
    private List<Integer> tagIds;

    /**
     * 折扣ID/NAME
     */
    private List<Integer> discountIds;

    private List<BillItemVo> billItems;
}
