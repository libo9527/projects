package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gzhennaxia.bill.entity.ProductCategory;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.service.IProductCategoryService;
import com.gzhennaxia.bill.vo.ProductCategoryNodeVo;
import com.gzhennaxia.bill.vo.ProductCategoryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/product-category")
public class ProductCategoryController {

    private final IProductCategoryService productCategoryService;

    @PostMapping("/search")
    public IPage<ProductCategoryNodeVo> search(@RequestBody ProductCategoryQuery query) {
        return productCategoryService.selectProductCategoryNodeVoPage(query);
    }


    @GetMapping("/children/{parentId}")
    public List<ProductCategoryNodeVo> getProductCategoryNodesByParentId(@PathVariable Integer parentId) {
        return productCategoryService.getProductCategoryNodesByParentId(parentId);
    }

    @PostMapping("/save")
    public void save(@RequestBody ProductCategoryVo productCategoryVo) {
        productCategoryService.save(productCategoryVo);
    }

    @GetMapping("/{id}")
    public ProductCategory getById(@PathVariable Integer id) {
        return productCategoryService.getById(id);
    }


    @PostMapping("/delete")
    public void delete(@RequestBody ProductCategoryVo productCategoryVo) {
        productCategoryService.delete(productCategoryVo);
    }

}
