package com.gzhennaxia.bill.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.BillItem;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.query.BillItemQuery;
import com.gzhennaxia.bill.service.IBillItemService;
import com.gzhennaxia.bill.vo.BillItemVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-item")
public class BillItemController {

    private final IBillItemService billItemService;

    @PostMapping("/search")
    public List<BillItemVo> search(BillItemQuery billItemQuery) {
        return billItemService.search(billItemQuery);
    }

    @PostMapping("/save")
    public void saveBillItem(@RequestBody BillItemParam billItemParam) {
        billItemService.save(billItemParam);
    }

    @PostMapping("/delete/{id}")
    public void deleteById(@PathVariable Integer id) {
        billItemService.update(Wrappers.<BillItem>lambdaUpdate().set(BillItem::getDelFlag, IsEnum.YES).eq(BillItem::getId, id));
    }
}
