package com.gzhennaxia.bill.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.entity.QuickBill;
import com.gzhennaxia.bill.entity.ThirdpartyBill;
import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
public class EleBillDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private ThirdpartyBillTypeEnum type;

    private Date consumeTime;

    /**
     * 第三方主键ID
     */
    private String thirdpartyId;

    /**
     * 交易对方
     */
    private String counterpartyName;

    /**
     * 描述
     */
    private String desc;

    /**
     * 价格
     */
    private BigDecimal price;

    private List<EleBillItemDto> eleBillItems;

    private List<EleBillDiscountDto> eleBillDiscounts;

    /**
     * 配送信息
     */
    private String deliveryHtml;

    /**
     * 订单信息
     */
    private String orderHtml;

    public ThirdpartyBill getThirdpartyBill() {
        ThirdpartyBill thirdpartyBill = new ThirdpartyBill();
        BeanUtils.copyProperties(this, thirdpartyBill);
        try {
            thirdpartyBill.setJsonData(new ObjectMapper().writeValueAsString(thirdpartyBill));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return thirdpartyBill;
    }

    public QuickBill toQuickBill() {
        return null;
    }

    public List<BillItemParam> getBillItems() {
        return null;
    }
}
