package com.gzhennaxia.bill.vo;

import java.time.LocalDateTime;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import org.springframework.beans.BeanUtils;
import com.gzhennaxia.bill.entity.ThirdpartyBill;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
@NoArgsConstructor
public class ThirdpartyBillVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer type;

    private LocalDateTime consumeTime;

    private String jsonData;

    private Integer status;


    public ThirdpartyBillVo(ThirdpartyBill thirdpartyBill) {
        BeanUtils.copyProperties(thirdpartyBill, this);
    }
}
