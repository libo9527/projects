package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
public class BillCategoryNodeVo {

    private Integer id;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Boolean hasChildren;

    private List<BillCategoryNodeVo> children;

    public BillCategoryNodeVo(BillCategory billCategory) {
        BeanUtils.copyProperties(billCategory, this);
        this.isLeaf = IsEnum.YES.getCode().equals(billCategory.getIsLeaf().getCode());
    }

    public Boolean getHasChildren() {
        return Optional.ofNullable(isLeaf).map(i -> !i).orElse(null);
    }
}
