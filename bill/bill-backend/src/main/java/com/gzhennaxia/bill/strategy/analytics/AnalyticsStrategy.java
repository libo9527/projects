package com.gzhennaxia.bill.strategy.analytics;

/**
 * 统计策略接口
 *
 * @author lib
 */
public interface AnalyticsStrategy {

    void doAnalytics();
}
