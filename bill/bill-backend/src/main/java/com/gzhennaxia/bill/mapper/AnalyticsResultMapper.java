package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
public interface AnalyticsResultMapper extends BaseMapper<AnalyticsResult> {

}
