package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzhennaxia.bill.entity.DailySummaryReport;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
public interface DailySummaryReportMapper extends BaseMapper<DailySummaryReport> {

    List<DailySummaryReport> sumExpense();

    List<DailySummaryReport> sumIncome();
}
