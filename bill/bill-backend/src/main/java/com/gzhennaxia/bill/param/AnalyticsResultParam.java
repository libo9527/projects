package com.gzhennaxia.bill.param;

import java.math.BigDecimal;
import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
@Data
public class AnalyticsResultParam implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer type;

    private BigDecimal value;


}
