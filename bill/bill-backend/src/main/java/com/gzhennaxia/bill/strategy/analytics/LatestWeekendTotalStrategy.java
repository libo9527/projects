package com.gzhennaxia.bill.strategy.analytics;

import com.gzhennaxia.bill.dto.AnalyticsResultDto;
import com.gzhennaxia.bill.enums.AnalyticsTypeEnum;
import com.gzhennaxia.bill.utils.DateTimeUtils;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 统计最近一年的总收入
 *
 * @author lib
 */
public class LatestWeekendTotalStrategy extends AbstractAnalyticsStrategy {

    @Override
    protected AnalyticsTypeEnum getType() {
        return AnalyticsTypeEnum.LATEST_WEEKEND_TOTAL;
    }

    @Override
    protected String getLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月第W周");
        return sdf.format(latestOne.getConsumeTime());
    }

    @Override
    protected List<AnalyticsResultDto> analytics() {
        return billService.analytics(DateTimeUtils.getMondayStart(latestOne.getConsumeTime()), DateTimeUtils.getSundayEnd(latestOne.getConsumeTime()));
    }
}
