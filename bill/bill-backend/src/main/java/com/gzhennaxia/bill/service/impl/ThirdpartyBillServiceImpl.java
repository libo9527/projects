package com.gzhennaxia.bill.service.impl;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.config.EleConfig;
import com.gzhennaxia.bill.dto.EleBillDiscountDto;
import com.gzhennaxia.bill.dto.EleBillDto;
import com.gzhennaxia.bill.dto.EleBillItemDto;
import com.gzhennaxia.bill.entity.ThirdpartyBill;
import com.gzhennaxia.bill.enums.ThirdpartyBillStatusEnum;
import com.gzhennaxia.bill.enums.ThirdpartyBillTypeEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.ThirdpartyBillMapper;
import com.gzhennaxia.bill.query.ThirdpartyBillQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.ICrawlerService;
import com.gzhennaxia.bill.service.IThirdpartyBillService;
import com.gzhennaxia.bill.utils.ChromeUtil;
import com.gzhennaxia.bill.vo.ThirdpartyBillVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ThirdpartyBillServiceImpl extends ServiceImpl<ThirdpartyBillMapper, ThirdpartyBill> implements IThirdpartyBillService {

    @Value("${chrome.driver.path}")
    private String chromeDriverPath;

    private final EleConfig eleConfig;
    private final ICrawlerService crawlerService;

    @Override
    public List<ThirdpartyBillVo> search(ThirdpartyBillQuery thirdpartyBillQuery) {
        return list().stream().map(ThirdpartyBillVo::new).collect(Collectors.toList());
    }

    @Override
    public void syncEle() {
        WebDriver driver = loginEle();
        crawl(driver);
        driver.close();
    }

    @Override
    public void importEleResponse() {
        File ordersDir = new File(eleConfig.getOrders());
        for (File ordersResponseFile : Objects.requireNonNull(ordersDir.listFiles())) {
            if (".DS_Store".equals(ordersResponseFile.getName())) {
                continue;
            }
            try {
                BufferedReader reader = new BufferedReader(new FileReader(ordersResponseFile));
                String ordersResponseJsonString = reader.readLine();
                if (ordersResponseJsonString != null) {
                    ObjectMapper om = new ObjectMapper();
                    JsonNode ordersResponseJsonNode = om.readTree(ordersResponseJsonString);
                    if (ordersResponseFile.getName().startsWith("old_orders")) {
                        ordersResponseJsonNode = ordersResponseJsonNode.get("orders");
                    }
                    for (JsonNode orderResponseJsonNode : ordersResponseJsonNode) {
                        String orderId = orderResponseJsonNode.get("id").asText();
                        ThirdpartyBill thirdpartyBill = this.getOne(Wrappers.<ThirdpartyBill>lambdaQuery()
                                .eq(ThirdpartyBill::getType, ThirdpartyBillTypeEnum.ELE)
                                .eq(ThirdpartyBill::getThirdpartyId, orderId));
                        File file = new File(eleConfig.getOrderDetail() + "/" + orderId);
                        if (thirdpartyBill != null && thirdpartyBill.getStatus().equals(ThirdpartyBillStatusEnum.RECONCILED)) {
                            file.delete();
                        } else {
                            if (thirdpartyBill == null) {
                                thirdpartyBill = new ThirdpartyBill();
                                thirdpartyBill.setType(ThirdpartyBillTypeEnum.ELE);
                                thirdpartyBill.setThirdpartyId(orderId);
                                thirdpartyBill.setJsonData(ordersResponseJsonString);
                                this.save(thirdpartyBill);
                            }
                            int count = 0;
                            if (!file.exists()) {
                                orderResponseJsonNode.get("order_scheme").isNull();
                                String orderDetailUrl = orderResponseJsonNode.get("order_scheme").asText();
                                // "order_scheme": null 代表是水果订单，水果订单要使用 order_detail_scheme
                                if ("null".equals(orderDetailUrl)) {
                                    orderDetailUrl = orderResponseJsonNode.get("order_detail_scheme").asText();
                                    ChromeUtil.open(orderDetailUrl);
                                    continue;
                                }
                                ChromeUtil.open(orderDetailUrl);
                            }
                            while (!file.exists()) {
                                Thread.sleep(10000);
                                if (count++ == 3) break;
                            }
                            if (!file.exists()) {
                                throw new BaseException(CodeMessageEnum.FILE_NOT_EXIST.replaceMessage("没有订单详情响应文件，订单号：" + orderId));
                            }
                            File orderDetailResponseFile = Objects.requireNonNull(file.listFiles())[0];
                            BufferedReader reader2 = new BufferedReader(new FileReader(orderDetailResponseFile));
                            String orderDetailJsonString = reader2.readLine();
                            crawlerService.reconciliationEleOrder(orderDetailJsonString);
                            // update thirdparty bill status.
                            this.update(new LambdaUpdateWrapper<ThirdpartyBill>()
                                    .set(ThirdpartyBill::getStatus, ThirdpartyBillStatusEnum.RECONCILED.getCode())
                                    .eq(ThirdpartyBill::getThirdpartyId, orderId));
                            reader2.close();
                            FileUtils.deleteDirectory(file);
                        }
                    }
                    ordersResponseFile.delete();
                }
                reader.close();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }

        importEleFruitOrderDetailResponse();
    }

    private void importEleFruitOrderDetailResponse() {
        try {
            // 水果订单的单独对账 todo 之后把外面订单的也独立出来
            File file = new File(eleConfig.getFruitOrderDetail());
            for (File fruitOrderDetailResponseFile : Objects.requireNonNull(file.listFiles())) {
                BufferedReader reader = new BufferedReader(new FileReader(fruitOrderDetailResponseFile));
                String fruitOrderDetailJsonString = reader.readLine();
                ObjectMapper om = new ObjectMapper();
                JsonNode jsonNode = om.readTree(fruitOrderDetailJsonString);
                crawlerService.reconciliationEleFruitOrder(jsonNode);
                // update thirdparty bill status.
                this.update(new LambdaUpdateWrapper<ThirdpartyBill>()
                        .set(ThirdpartyBill::getStatus, ThirdpartyBillStatusEnum.RECONCILED.getCode())
                        .eq(ThirdpartyBill::getThirdpartyId, jsonNode.get("data").get("data").get("alscOrderNo").asText()));
                reader.close();
                fruitOrderDetailResponseFile.delete();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void crawl(WebDriver driver) {
        // 打开订单列表页面
        driver.navigate().to("https://h5.ele.me/order");
        // 获取【订单卡片】列表
        List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
        crawlEleOrder(driver, orderCards.get(0));

        // 滚动到页面底部
//        JavascriptExecutor js = (JavascriptExecutor)driver;
//        js.executeScript("window.scrollBy(0,200)");
    }

    /**
     * @param start 起始下标
     * @param end   结束下标
     */
    private void crawl(WebDriver driver, int start, int end) {
        if (start >= end) return;
        int currentSize;
        do {
            // 跳转到订单列表页面
            driver.navigate().to("https://h5.ele.me/order");
            // 获取【订单卡片】列表
            List<WebElement> orderCards = driver.findElements(By.xpath("//div[@class=\"ordercard\"]"));
            currentSize = orderCards.size();
            if (currentSize > start) {
                crawlEleOrder(driver, orderCards.get(start));
            }
        } while (currentSize > start);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        crawl(driver, start, end);
    }

    /**
     * 登录饿了么
     */
    private WebDriver loginEle() {
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        WebDriver driver = new ChromeDriver();
        // 全屏
        driver.manage().window().maximize();
        // driver 全局等待时间，每一次driver操作的等待时间
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        // 打开登录页面
        // redirect=https://h5.ele.me/order
        driver.get("https://tb.ele.me/wow/msite/act/login?redirect=https%3A%2F%2Fh5.ele.me%2Forder");
        // 登录页包含在 Frame 中，需要先切换到 Frame 里才可以获取到页面元素
        driver.switchTo().frame("alibaba-login-box");
        // 获取【手机号】输入框
        WebElement phoneInput = driver.findElement(By.id("fm-sms-login-id"));
        // 输入手机号
        phoneInput.sendKeys("18598046892");
        // 获取【发送短信验证码】按钮
        WebElement sendMessageButton = driver.findElement(By.xpath("//a[@class=\"send-btn-link\"]"));
        // 发送短信验证码
        sendMessageButton.click();
        // 获取【短信验证码】输入框
        WebElement smsCodeInput = driver.findElement(By.id("fm-smscode"));
        int timeout = 60;
        while (timeout >= 0) {
            if (timeout == 0) throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("短信验证码已过期"));
            String value = smsCodeInput.getAttribute("value");
            if (value.length() == 6) break;
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeout--;
        }
        // 获取【登录】按钮
        WebElement loginButton = driver.findElement(By.xpath("//div/button[@type=\"submit\"]"));
        // 登录
        loginButton.click();
        return driver;
    }

    /**
     * 爬取饿了么订单信息
     */
    private void crawlEleOrder(WebDriver driver, WebElement orderCard) {
        EleBillDto eleBill = new EleBillDto();
        eleBill.setType(ThirdpartyBillTypeEnum.ELE);
        // 获取商家名称
        WebElement titleNameContent = orderCard.findElement(By.cssSelector(".title>.name>.content"));
        String counterpartyName = titleNameContent.getText();
        eleBill.setCounterpartyName(counterpartyName);
        // 获取消费时间
        WebElement datetime = orderCard.findElement(By.cssSelector(".datetime"));
        Date consumeTime = null;
        try {
            consumeTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(datetime.getText());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        eleBill.setConsumeTime(consumeTime);
        // 获取账单描述
        List<WebElement> details = orderCard.findElements(By.cssSelector(".ordercard-detail>.detail>span"));
        StringBuilder products = new StringBuilder();
        for (WebElement detail : details) {
            products.append(detail.getText());
        }
        String desc = products.toString();
        eleBill.setDesc(desc);
        // 获取账单价格
        WebElement priceElement = orderCard.findElement(By.cssSelector(".ordercard-detail>.price"));
        BigDecimal price = new BigDecimal(priceElement.getText().substring(1));
        eleBill.setPrice(price);
        // 查询该账单是否存在
        List<ThirdpartyBill> list = list(Wrappers.<ThirdpartyBill>lambdaQuery()
                .eq(ThirdpartyBill::getType, ThirdpartyBillTypeEnum.ELE)
                .eq(ThirdpartyBill::getCounterpartyName, counterpartyName)
                .eq(ThirdpartyBill::getConsumeTime, consumeTime)
                .eq(ThirdpartyBill::getDesc, desc)
                .eq(ThirdpartyBill::getPrice, price));
        if (CollectionUtils.isEmpty(list)) {
            try {
                crawlEleOrderDetail(driver, orderCard, eleBill);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("################ crawl error ################\n" + orderCard.getAttribute("outerHTML"));
            }
        }
    }

    /**
     * 爬取饿了么订单详情
     */
    private void crawlEleOrderDetail(WebDriver driver, WebElement orderCard, EleBillDto eleBill) {
        String counterpartyName;
        // 获取账单详情div元素
        WebElement orderDetailElement = orderCard.findElement(By.cssSelector(".ordercard-detail"));
        orderDetailElement.click();

        // 获取订单详情
        WebElement restaurantElement = driver.findElement(By.xpath("//div[@class=\"restaurant-card\"]"));
        // 商家名称
        WebElement counterpartyNameElement = restaurantElement.findElement(By.cssSelector(".head>.name-wrap>.name"));
        counterpartyName = counterpartyNameElement.getText();
        eleBill.setCounterpartyName(counterpartyName);

        // 商品列表
        List<WebElement> productElements = restaurantElement.findElements(By.cssSelector(".product-list>.cart-item>.product-item"));
        List<EleBillItemDto> eleBillItems = new ArrayList<>(productElements.size());
        for (WebElement productElement : productElements) {
            EleBillItemDto eleBillItem = new EleBillItemDto();
            // 商品名称
            WebElement productNameElement = productElement.findElement(By.cssSelector(".profile>.name"));
            String productName = productNameElement.getText();
            eleBillItem.setProductName(productName);
            // 商品数量
            WebElement quantityElement = productElement.findElement(By.cssSelector(".quantity"));
            Integer quantity = Integer.valueOf(quantityElement.getText().substring(1));
            eleBillItem.setQuantity(quantity);
            // 商品金额
            WebElement numberElement = productElement.findElement(By.cssSelector(".number"));
            BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
            eleBillItem.setAmount(amount);

            eleBillItems.add(eleBillItem);
        }

        // 餐盒费/配送费等额外信息，在账单系统中均被认为是商品
        List<WebElement> otherElements = restaurantElement.findElements(By.cssSelector(".listitem>.product-item"));
        for (WebElement otherElement : otherElements) {
            EleBillItemDto eleBillItem = new EleBillItemDto();
            // 名称
            WebElement nameElement = otherElement.findElement(By.cssSelector(".name"));
            String name = nameElement.getText();
            eleBillItem.setProductName(name);

            List<WebElement> quantityPriceElements = otherElement.findElements(By.cssSelector(".price-wrap>.quantity"));
            if (CollectionUtils.isNotEmpty(quantityPriceElements)) {
                // 数量
                Integer quantity = Integer.valueOf(quantityPriceElements.get(0).getText().substring(1));
                eleBillItem.setQuantity(quantity);
                // 金额
                WebElement numberElement = otherElement.findElement(By.cssSelector(".price-wrap>.number"));
                BigDecimal amount = new BigDecimal(numberElement.getText().substring(1));
                eleBillItem.setAmount(amount);
            } else {
                List<WebElement> numberElement = otherElement.findElements(By.cssSelector(".number"));
                if (CollectionUtils.isNotEmpty(numberElement)) {
                    // 金额
                    BigDecimal amount = new BigDecimal(numberElement.get(0).getText().substring(1));
                    eleBillItem.setAmount(amount);
                }
            }
        }
        eleBill.setEleBillItems(eleBillItems);

        // 折扣
        List<WebElement> discountElements = restaurantElement.findElements(By.cssSelector(".price>.product-item"));
        if (CollectionUtils.isNotEmpty(discountElements)) {
            List<EleBillDiscountDto> eleBillDiscounts = new ArrayList<>(discountElements.size());
            for (WebElement discountElement : discountElements) {
                EleBillDiscountDto eleBillDiscount = new EleBillDiscountDto();
                WebElement nameElement = discountElement.findElement(By.xpath("//span[@class=\"name\"]/span"));
                String name = nameElement.getText();
                eleBillDiscount.setDesc(name);

                StringBuilder sb = new StringBuilder();
                WebElement amountElement = discountElement.findElement(By.cssSelector(".discount"));
                List<WebElement> spanElements = amountElement.findElements(By.xpath("span"));
                for (WebElement spanElement : spanElements) {
                    sb.append(spanElement.getText());
                }
                sb.append(amountElement.getText());
                eleBillDiscount.setAmount(sb.toString());
                eleBillDiscounts.add(eleBillDiscount);
            }
            eleBill.setEleBillDiscounts(eleBillDiscounts);
        }

        // 配送信息
        eleBill.setDeliveryHtml(driver.findElement(By.cssSelector(".detailcard>.detailcard-delivery")).getAttribute("outerHTML"));

        // 订单信息
        WebElement orderElement = driver.findElement(By.cssSelector(".detailcard>.detailcard-order"));
        eleBill.setOrderHtml(orderElement.getAttribute("outerHTML"));

        List<WebElement> orderElements = orderElement.findElements(By.cssSelector(".cardlist>.listitem"));
        for (WebElement orderElement1 : orderElements) {
            WebElement span = orderElement1.findElement(By.xpath("//span"));
            if ("订单号：".equals(span.getText())) {
                eleBill.setThirdpartyId(orderElement1.getText().trim());
                break;
            }
        }
        save(eleBill.getThirdpartyBill());
    }
}
