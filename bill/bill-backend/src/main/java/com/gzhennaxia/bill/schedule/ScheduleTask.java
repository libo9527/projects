package com.gzhennaxia.bill.schedule;

import com.gzhennaxia.bill.strategy.analytics.AnalyticsStrategy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ServiceLoader;

/**
 * @author lib
 */
@Component
public class ScheduleTask {

    /**
     * 每隔1个小时执行全部统计策略
     */
//    @Scheduled(fixedRate = 1 * 60 * 60 * 1000)
    public void analytics() {
        ServiceLoader<AnalyticsStrategy> serviceLoader = ServiceLoader.load(AnalyticsStrategy.class);
        for (AnalyticsStrategy analyticsStrategy : serviceLoader) {
            analyticsStrategy.doAnalytics();
        }
    }

}
