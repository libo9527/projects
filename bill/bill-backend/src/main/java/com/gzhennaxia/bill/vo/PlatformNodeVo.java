package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Optional;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class PlatformNodeVo {

    private Integer id;
    private String name;
    private String path;
    private Boolean isLeaf;
    private Boolean hasChildren;
    private List<PlatformNodeVo> children;

    public PlatformNodeVo(Platform platform) {
        BeanUtils.copyProperties(platform, this);
        this.isLeaf = IsEnum.YES.getCode().equals(platform.getIsLeaf().getCode());
    }

    public Boolean getHasChildren() {
        return Optional.ofNullable(isLeaf).map(i -> !i).orElse(null);
    }
}
