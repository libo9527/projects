package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.AnalyticsResult;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.AnalyticsResultQuery;
import com.gzhennaxia.bill.vo.AnalyticsResultVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-07-14
 */
public interface IAnalyticsResultService extends IService<AnalyticsResult> {

    List<AnalyticsResultVo> search(AnalyticsResultQuery analyticsResultQuery);
}
