package com.gzhennaxia.bill.entity;

import com.gzhennaxia.bill.vo.PlatformVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Platform extends TreeEntityCommonColumn {
    public Platform(PlatformVo platformVo) {
        BeanUtils.copyProperties(platformVo, this);
    }
}
