package com.gzhennaxia.bill.utils;

import java.io.File;

public class FileUtils {

    /**
     * 获取文件格式名
     */
    public static String getFileFormatName(File file){
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static boolean isExcel(File file) {
        return "xlsx".equals(getFileFormatName(file));
    }
}
