package com.gzhennaxia.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gzhennaxia.bill.entity.Counterparty;
import com.gzhennaxia.bill.query.CounterpartyQuery;
import com.gzhennaxia.bill.vo.CounterpartyNodeVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
public interface CounterpartyMapper extends BaseMapper<Counterparty> {

    /**
     * 获取账单分类节点
     *
     * @param parentId 父级分类ID
     */
    List<Counterparty> getCounterpartyNodesByParentId(Integer parentId);


    IPage<CounterpartyNodeVo> selectCounterpartyNodeVoPage(Page<CounterpartyNodeVo> page, CounterpartyQuery query);
}
