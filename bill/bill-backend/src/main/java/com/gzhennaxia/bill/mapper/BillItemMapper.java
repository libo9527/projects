package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.BillItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface BillItemMapper extends BaseMapper<BillItem> {

}
