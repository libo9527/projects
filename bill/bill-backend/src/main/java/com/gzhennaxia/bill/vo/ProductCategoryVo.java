package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.ProductCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Data
@NoArgsConstructor
public class ProductCategoryVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer parentId;

    private String name;

    private String path;

    private Boolean isLeaf;

    private Integer status;

    public ProductCategoryVo(ProductCategory productCategory) {
        BeanUtils.copyProperties(productCategory, this);
        this.isLeaf = IsEnum.YES.getCode().equals(productCategory.getIsLeaf().getCode());
    }
}
