package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.entity.ProductCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.ProductCategoryMapper;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IProductCategoryService;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.vo.ProductCategoryNodeVo;
import com.gzhennaxia.bill.vo.ProductCategoryVo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Service
@RequiredArgsConstructor
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryMapper, ProductCategory> implements IProductCategoryService {

    private final IProductService productService;

    @Override
    public List<ProductCategoryNodeVo> getProductCategoryNodesByParentId(Integer parentId) {
        return list(Wrappers.<ProductCategory>lambdaQuery().eq(ProductCategory::getParentId, parentId).eq(ProductCategory::getDelFlag, IsEnum.NO)).stream().map(ProductCategoryNodeVo::new).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(ProductCategoryVo productCategoryVo) {
        Integer currentId = productCategoryVo.getId();
        Integer parentId = productCategoryVo.getParentId();
        ProductCategory current = new ProductCategory(productCategoryVo);
        // 如果 id 为空，并且 parentId 不为空，则表示新增；否则表示更新
        if (currentId == null && parentId != null) {
            ProductCategory parent = baseMapper.selectById(parentId);
            if (parent != null && parent.getIsLeaf().equals(IsEnum.YES)) {
                List<Product> list = productService.list(Wrappers.<Product>lambdaQuery().eq(Product::getCategoryId, parentId));
                if (CollectionUtils.isNotEmpty(list)) {
                    throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("父节点下存在商品，不可新增子节点"));
                }
            }
            baseMapper.insert(current);
            currentId = current.getId();
            if (parent == null) {
                current.setPath("0," + current.getId());
            } else {
                parent.setIsLeaf(IsEnum.NO);
                baseMapper.updateById(parent);
                current.setPath(parent.getPath() + "," + current.getId());
            }
        }
        // 新增后也需要更新path，因此新增和修改都需要该语句
        baseMapper.updateById(current);
    }

    @Override
    public IPage<ProductCategoryNodeVo> selectProductCategoryNodeVoPage(ProductCategoryQuery query) {
        Page<ProductCategoryNodeVo> page = new Page<>(query.getCurrent(), query.getSize());
        return baseMapper.selectProductCategoryNodeVoPage(page, query);
    }

    @Override
    public void delete(ProductCategoryVo productCategoryVo) {
        assert productCategoryVo.getId() != null;
        ProductCategory productCategory = new ProductCategory(productCategoryVo);
        productCategory.setDelFlag(IsEnum.YES);
        baseMapper.updateById(productCategory);
        deleteChildren(productCategory.getId());
    }

    @Async
    void deleteChildren(Integer parentId) {
        List<ProductCategory> children = baseMapper.selectList(Wrappers.<ProductCategory>lambdaQuery().eq(ProductCategory::getParentId, parentId));
        children.forEach(child -> deleteChildren(child.getId()));
        baseMapper.update(new ProductCategory(), Wrappers.<ProductCategory>lambdaUpdate().set(ProductCategory::getDelFlag, IsEnum.YES).eq(ProductCategory::getId, parentId));
    }
}
