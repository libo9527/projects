package com.gzhennaxia.bill.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-05-08
 */
@Data
public class QuickBillItemDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer quickBillId;

    private Integer productId;

    private Integer skuId;

    private BigDecimal price;

    private Integer quantity;

    private BigDecimal totalPrice;

    private Integer discountId;

    private String note;

    private Integer status;


}
