package com.gzhennaxia.bill.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gzhennaxia.bill.entity.Platform;
import com.gzhennaxia.bill.query.PlatformQuery;
import com.gzhennaxia.bill.vo.PlatformNodeVo;
import com.gzhennaxia.bill.vo.PlatformVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-05-07
 */
public interface IPlatformService extends IService<Platform> {

    List<PlatformNodeVo> getPlatformNodesByParentId(Integer parentId);

    List<PlatformNodeVo> getSourceNodesByPath(String parentId);

    IPage<PlatformNodeVo> selectPlatformNodeVoPage(PlatformQuery query);

    void save(PlatformVo platformVo);

    void delete(PlatformVo platformVo);

    List<TreeNodeVo> getSelectOptions(Integer id);
}
