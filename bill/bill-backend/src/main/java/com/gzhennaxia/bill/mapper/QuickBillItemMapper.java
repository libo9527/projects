package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.QuickBillItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-05-08
 */
public interface QuickBillItemMapper extends BaseMapper<QuickBillItem> {

}
