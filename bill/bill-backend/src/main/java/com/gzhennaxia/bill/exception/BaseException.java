package com.gzhennaxia.bill.exception;

import com.gzhennaxia.bill.result.CodeMessageEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author bo li
 * @date 2020-07-22 14:41
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseException extends RuntimeException {

    private CodeMessageEnum codeMessage;

    public BaseException(CodeMessageEnum codeMessage) {
        super(codeMessage.getMessage());
        this.codeMessage = codeMessage;
    }

    public BaseException(CodeMessageEnum codeMessage, String errorMessage) {
        super(errorMessage);
        this.codeMessage = codeMessage;
    }
}

