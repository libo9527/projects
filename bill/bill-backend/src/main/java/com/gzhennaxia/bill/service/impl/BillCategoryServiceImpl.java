package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.Bill;
import com.gzhennaxia.bill.entity.BillCategory;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.mapper.BillCategoryMapper;
import com.gzhennaxia.bill.query.ProductCategoryQuery;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import com.gzhennaxia.bill.service.IBillCategoryService;
import com.gzhennaxia.bill.service.IBillService;
import com.gzhennaxia.bill.vo.BillCategoryNodeVo;
import com.gzhennaxia.bill.vo.BillCategoryVo;
import com.gzhennaxia.bill.vo.TreeNodeVo;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lib
 */
@Service
@RequiredArgsConstructor
public class BillCategoryServiceImpl extends ServiceImpl<BillCategoryMapper, BillCategory> implements IBillCategoryService {

    private final IBillService billService;

    @Override
    public List<BillCategoryNodeVo> getBillCategoryNodesByParentId(Integer parentId) {
        return baseMapper.getBillCategoryNodesByParentId(parentId).stream().map(BillCategoryNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public List<BillCategoryNodeVo> getBillCategoryNodesByPath(String path) {
        List<Integer> categoryIds = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<BillCategory> list = baseMapper.selectBatchIds(categoryIds);
        return list.stream().map(BillCategoryNodeVo::new).collect(Collectors.toList());
    }

    @Override
    public IPage<BillCategoryNodeVo> selectBillCategoryNodeVoPage(ProductCategoryQuery query) {
        Page<BillCategoryNodeVo> page = new Page<>(query.getCurrent(), query.getSize());
        return baseMapper.selectBillCategoryNodeVoPage(page, query);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(BillCategoryVo billCategoryVo) {
        Integer currentId = billCategoryVo.getId();
        Integer parentId = billCategoryVo.getParentId();
        BillCategory current = new BillCategory(billCategoryVo);
        // 如果 id 为空，并且 parentId 不为空，则表示新增；否则表示更新
        if (currentId == null && parentId != null) {
            BillCategory parent = baseMapper.selectById(parentId);
            if (parent != null && parent.getIsLeaf().equals(IsEnum.YES)) {
                List<Bill> list = billService.list(Wrappers.<Bill>lambdaQuery().eq(Bill::getCategoryId, parentId));
                if (CollectionUtils.isNotEmpty(list)) {
                    throw new BaseException(CodeMessageEnum.UN_KNOWN.replaceMessage("父节点下存在账单，不可新增子节点"));
                }
            }
            baseMapper.insert(current);
            currentId = current.getId();
            if (parent == null) {
                current.setPath("0," + current.getId());
            } else {
                parent.setIsLeaf(IsEnum.NO);
                baseMapper.updateById(parent);
                current.setPath(parent.getPath() + "," + current.getId());
            }
        }
        // 新增后也需要更新path，因此新增和修改都需要该语句
        baseMapper.updateById(current);
    }

    @Override
    public void delete(BillCategoryVo billCategoryVo) {
        assert billCategoryVo.getId() != null;
        BillCategory billCategory = new BillCategory(billCategoryVo);
        billCategory.setDelFlag(IsEnum.YES);
        baseMapper.updateById(billCategory);
        deleteChildren(billCategory.getId());
    }

    @Async
    void deleteChildren(Integer parentId) {
        List<BillCategory> children = baseMapper.selectList(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getParentId, parentId));
        children.forEach(child -> deleteChildren(child.getId()));
        baseMapper.update(new BillCategory(), Wrappers.<BillCategory>lambdaUpdate().set(BillCategory::getDelFlag, IsEnum.YES).eq(BillCategory::getId, parentId));
    }

    @Override
    public List<TreeNodeVo> getSelectOptions(Integer id) {
        BillCategory billCategory = baseMapper.selectById(id);
        String path = billCategory.getPath();
        List<Integer> categoryIds = Arrays.stream(path.split(",")).map(s -> Integer.parseInt(s.trim())).collect(Collectors.toList());
        List<BillCategory> list = baseMapper.selectBatchIds(categoryIds);

        TreeNodeVo root = new TreeNodeVo();
        root.setValue(0);

        TreeNodeVo parent = root;
        Iterator<BillCategory> iterator = list.iterator();
        while (iterator.hasNext()) {
            BillCategory next = iterator.next();
            if (next.getParentId().equals(parent.getValue())) {
                List<TreeNodeVo> children = parent.getChildren();
                if (CollectionUtils.isEmpty(children)) {
                    children = new ArrayList<>(1);
                    parent.setChildren(children);
                }
                TreeNodeVo child = new TreeNodeVo(next);
                children.add(child);
                iterator.remove();
                parent = child;
            }
        }
        return root.getChildren();
    }
}
