package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.query.BillTagRelQuery;
import com.gzhennaxia.bill.service.IBillTagRelService;
import com.gzhennaxia.bill.vo.BillTagRelVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lib
 * @since 2021-06-22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bill-tag-rel")
public class BillTagRelController {

    private final IBillTagRelService billTagRelService;

    @PostMapping("/search")
    public List<BillTagRelVo> search(BillTagRelQuery billTagRelQuery) {
       return billTagRelService.search(billTagRelQuery);
    }
}
