package com.gzhennaxia.bill.mapper;

import com.gzhennaxia.bill.entity.ThirdpartyBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
public interface ThirdpartyBillMapper extends BaseMapper<ThirdpartyBill> {

}
