package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.param.BillItemParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class BillItem extends CommonColumn {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer billId;

    private Integer productId;

    /**
     * 应付/应收金额
     */
    private BigDecimal payableOrReceivableAmount;

    /**
     * 实付/实收金额
     */
    private BigDecimal paymentOrReceiptsAmount;

    private String note;

    public BillItem(BillItemParam billItemParam) {
        BeanUtils.copyProperties(billItemParam, this);
    }
}
