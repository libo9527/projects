package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.IsEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 层级结构实体的公共列
 *
 * @author lib
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TreeEntityCommonColumn extends CommonColumn {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer parentId;
    private String name;
    private String path;
    private IsEnum isLeaf;
    private Integer status;
    private IsEnum delFlag;
}
