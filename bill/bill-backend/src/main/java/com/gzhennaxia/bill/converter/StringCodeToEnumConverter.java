package com.gzhennaxia.bill.converter;

import com.alibaba.excel.util.StringUtils;
import com.gzhennaxia.bill.enums.BaseEnum;
import com.gzhennaxia.bill.exception.BaseException;
import com.gzhennaxia.bill.result.CodeMessageEnum;
import org.springframework.core.convert.converter.Converter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author lib
 */
public class StringCodeToEnumConverter<T extends BaseEnum> implements Converter<String, T> {

    Map<Integer, T> map = new HashMap<>();

    public StringCodeToEnumConverter(Class<T> enumType) {
        T[] enumConstants = enumType.getEnumConstants();
        for (T enumConstant : enumConstants) {
            map.put(enumConstant.getCode(), enumConstant);
        }
    }

    @Override
    public T convert(String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        int integer;
        if (Boolean.TRUE.toString().equals(code)) {
            integer = 1;
        } else if (Boolean.FALSE.toString().equals(code)) {
            integer = 0;
        } else {
            integer = Integer.parseInt(code);
        }
        T t = map.get(integer);
        if (Objects.isNull(t)) {
            throw new BaseException(CodeMessageEnum.UNMATCHED_ENUM);
        }
        return t;
    }
}
