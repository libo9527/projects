package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lib
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Discount extends CommonColumn {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String desc;

}
