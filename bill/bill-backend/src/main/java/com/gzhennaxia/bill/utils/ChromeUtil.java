package com.gzhennaxia.bill.utils;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

/**
 * @author gzhennaxia
 */
public class ChromeUtil {

    /**
     * 使用默认浏览器打开指定网址
     *
     * @param url
     */
    public static void open(String url) {
        try {
            Runtime.getRuntime().exec("open " + url);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
