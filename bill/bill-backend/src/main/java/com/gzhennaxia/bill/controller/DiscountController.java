package com.gzhennaxia.bill.controller;

import com.alibaba.excel.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.gzhennaxia.bill.entity.Discount;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.query.DiscountQuery;
import com.gzhennaxia.bill.result.Result;
import com.gzhennaxia.bill.service.IDiscountService;
import com.gzhennaxia.bill.vo.DiscountVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/discount")
public class DiscountController {

    private final IDiscountService discountService;

    @GetMapping("/all")
    public Result<?> getAllDiscounts() {
        return Result.success(discountService.getAllDiscounts());
    }

    @PostMapping("/page")
    public IPage<DiscountVo> pageSearch(@RequestBody DiscountQuery discountQuery) {
        return discountService.pageSearch(discountQuery);
    }

    @PostMapping("/save")
    public void save(@RequestBody Discount discount) {
        discountService.saveOrUpdate(discount);
    }

    @GetMapping("/keyword/{keyword}")
    public List<Discount> searchByKeyword(@PathVariable String keyword) {
        if (StringUtils.isEmpty(keyword) || StringUtils.isEmpty(keyword.trim())) return null;
        return discountService.list(Wrappers.<Discount>lambdaQuery().eq(Discount::getDelFlag, IsEnum.NO).like(Discount::getDesc, keyword.trim()));
    }

    @GetMapping("/billId/{billId}")
    public List<Discount> searchByBillId(@PathVariable Integer billId) {
        return discountService.searchByBillId(billId);
    }
}
