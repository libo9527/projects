package com.gzhennaxia.bill.vo;

import com.gzhennaxia.bill.entity.ImportHistory;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
public class ImportResultVo {

    /**
     * 导入成功总数
     */
    private Integer successCount;

    /**
     * 重复总数：有多少条记录是数据库已经存在的
     */
    private Integer repeatCount;

    /**
     * 失败条数
     */
    private Integer failedCount;

    /**
     * 导入总数
     */
    private Integer total;

    public ImportResultVo(ImportHistory importHistory) {
        BeanUtils.copyProperties(importHistory, this);
    }
}
