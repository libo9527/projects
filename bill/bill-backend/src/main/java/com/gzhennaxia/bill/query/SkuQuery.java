package com.gzhennaxia.bill.query;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class SkuQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String desc;

    private String unit;

    private String keyword;


}
