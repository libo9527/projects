package com.gzhennaxia.bill.strategy.reconciliation;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzhennaxia.bill.entity.*;
import com.gzhennaxia.bill.enums.BillStatusEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.param.BillItemParam;
import com.gzhennaxia.bill.service.*;
import com.gzhennaxia.bill.utils.ReconciliationUtils;
import com.gzhennaxia.bill.utils.SpringBeanUtils;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;
import com.gzhennaxia.bill.vo.BillItemVo;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 饿了么外卖订单对账策略
 * 修改完爬虫接口后会自动对账，此策略实现类即被废弃
 * TODO 以下两个接口需自测，自测完后即可删除 com.gzhennaxia.bill.strategy.reconciliation.ReconciliationStrategy 文件中该策略类
 * http://localhost:9527/bill/crawler/ele/order
 * http://localhost:9527/bill/crawler/ele/order/fruit
 *
 * @author lib
 */
@Deprecated
public class EleOrderReconciliationStrategy extends AbstractReconciliationStrategy {

    private final IQuickBillService quickBillService;
    private final IBillService billService;
    private final IBillCategoryService billCategoryService;
    private final ICounterpartyService counterpartyService;
    private final IPlatformService platformService;
    private final IPayTypeService payTypeService;
    private final IBillItemService billItemService;
    private final ISkuService skuService;

    public EleOrderReconciliationStrategy() {
        this.billService = SpringBeanUtils.getBean(IBillService.class);
        this.billCategoryService = SpringBeanUtils.getBean(IBillCategoryService.class);
        this.quickBillService = SpringBeanUtils.getBean(IQuickBillService.class);
        this.counterpartyService = SpringBeanUtils.getBean(ICounterpartyService.class);
        this.platformService = SpringBeanUtils.getBean(IPlatformService.class);
        this.payTypeService = SpringBeanUtils.getBean(IPayTypeService.class);
        this.billItemService = SpringBeanUtils.getBean(IBillItemService.class);
        this.skuService = SpringBeanUtils.getBean(ISkuService.class);
    }

    @Override
    public Boolean valid(QuickBill quickBill) {
        if (!"餐饮美食".equals(quickBill.getCategoryName())) return false;
        if ("拼多多平台商户".equals(quickBill.getCounterpartyName())) return false;
        if (!quickBill.getDesc().contains("件商品：")) return false;
        ObjectMapper om = new ObjectMapper();
        try {
            List<BillItemVo> billItemVos = om.readValue(quickBill.getNote(), new TypeReference<List<BillItemVo>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Bill buildBill(QuickBill quickBill) {
        Bill bill = new Bill();
        BeanUtils.copyProperties(quickBill, bill);
        String threeMealsName = ReconciliationUtils.parseThreeMealsName(quickBill.getConsumeTime());
        BillCategory category = billCategoryService.getOne(Wrappers.<BillCategory>lambdaQuery().eq(BillCategory::getName, threeMealsName));
        bill.setCategoryId(category.getId());
        Counterparty parentCounterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getName, "饿了么平台商户"));
        Counterparty counterparty = counterpartyService.getOne(Wrappers.<Counterparty>lambdaQuery().eq(Counterparty::getParentId, parentCounterparty.getId()).eq(Counterparty::getName, quickBill.getCounterpartyName()));
        if (counterparty == null) {
            counterparty = new Counterparty();
            counterparty.setName(quickBill.getCounterpartyName());
            counterparty.setParentId(parentCounterparty.getId());
            counterparty.setIsLeaf(IsEnum.YES);
            counterpartyService.save(counterparty);
            counterparty.setPath(parentCounterparty.getPath() + "," + counterparty.getId());
            counterpartyService.updateById(counterparty);
        }
        bill.setCounterpartyId(counterparty.getId());
        Platform platform = platformService.getOne(Wrappers.<Platform>lambdaQuery().eq(Platform::getName, "饿了么外卖APP"));
        bill.setPlatformId(platform.getId());
        PayType payType = payTypeService.getOne(Wrappers.<PayType>lambdaQuery().eq(PayType::getName, "花呗"));
        bill.setPayTypeId(payType.getId());
        bill.setType(BillTypeEnum.EXPENSES);
        bill.setPaymentOrReceiptsAmount(quickBill.getMoney());
        bill.setStatus(BillStatusEnum.TO_BE_ARCHIVED);
        billService.save(bill);
        quickBill.setBillId(bill.getId());
        quickBillService.updateById(quickBill);
        return bill;
    }

    @Override
    void buildAndSaveBillItem(Bill bill) {
        ObjectMapper om = new ObjectMapper();
        try {
            List<BillItemVo> billItemVos = om.readValue(bill.getNote(), new TypeReference<List<BillItemVo>>() {
            });
            for (BillItemVo billItemVo : billItemVos) {
                BillItemParam billItemParam = new BillItemParam();
                BeanUtils.copyProperties(billItemVo, billItemParam);
                if (billItemVo.getPaymentOrReceiptsAmount().compareTo(BigDecimal.ZERO) <= 0) continue;
                billItemParam.setBillId(bill.getId());
                billItemParam.setProductIdOrName(billItemVo.getProductIdOrName());
                List<BillItemSkuRelVo> billItemSkus = billItemParam.getBillItemSkus();
                for (BillItemSkuRelVo itemSku : billItemSkus) {
                    itemSku.setSkuPrice(billItemVo.getPaymentOrReceiptsAmount());
                    Sku one = skuService.getOne(Wrappers.<Sku>lambdaQuery().eq(Sku::getUnit, itemSku.getSkuUnit()));
                    if (one != null) {
                        itemSku.setSkuIdOrUnit(one.getId());
                    } else {
                        Sku sku = new Sku();
                        sku.setUnit(itemSku.getSkuUnit());
                        skuService.save(sku);
                        itemSku.setSkuIdOrUnit(sku.getId());
                    }
                }
                billItemService.save(billItemParam);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
