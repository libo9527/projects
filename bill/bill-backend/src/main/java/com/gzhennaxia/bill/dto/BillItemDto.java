package com.gzhennaxia.bill.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
@Data
public class BillItemDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer billId;

    private Integer productId;

    /**
     * 应付/应收金额
     */
    private BigDecimal payableOrReceivableAmount;

    /**
     * 实付/实收金额
     */
    private BigDecimal paymentOrReceiptsAmount;


}
