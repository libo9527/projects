package com.gzhennaxia.bill.entity;

import com.gzhennaxia.bill.vo.PayTypeVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PayType extends TreeEntityCommonColumn {
    public PayType(PayTypeVo payTypeVo) {
        BeanUtils.copyProperties(payTypeVo, this);
    }
}
