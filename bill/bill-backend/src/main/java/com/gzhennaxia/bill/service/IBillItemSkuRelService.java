package com.gzhennaxia.bill.service;

import com.gzhennaxia.bill.entity.BillItemSkuRel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import com.gzhennaxia.bill.query.BillItemSkuRelQuery;
import com.gzhennaxia.bill.vo.BillItemSkuRelVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lib
 * @since 2021-06-25
 */
public interface IBillItemSkuRelService extends IService<BillItemSkuRel> {

    List<BillItemSkuRelVo> search(BillItemSkuRelQuery billItemSkuRelQuery);
}
