package com.gzhennaxia.bill.param;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-07-06
 */
@Data
public class ThirdpartyBillParam implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer type;

    private LocalDateTime consumeTime;

    private String jsonData;

    private Integer status;


}
