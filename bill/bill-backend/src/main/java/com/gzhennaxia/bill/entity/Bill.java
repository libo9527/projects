package com.gzhennaxia.bill.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.gzhennaxia.bill.enums.BillStatusEnum;
import com.gzhennaxia.bill.enums.BillTypeEnum;
import com.gzhennaxia.bill.param.BillParam;
import com.gzhennaxia.bill.type_handler.MybatisPlusDateTimeTypeHandler;
import com.gzhennaxia.bill.vo.BillVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author lib
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Bill extends CommonColumn {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer userId;

    private String desc;

    private String note;

    private Integer categoryId;

    private Integer counterpartyId;

    /**
     * 交易对方订单号
     */
    private String counterpartyOrderId;

    private Integer platformId;

    private Integer payTypeId;

    /**
     * 支付方订单号
     */
    private String payOrderId;

//    private Integer discountId;

    @TableField(typeHandler = MybatisPlusDateTimeTypeHandler.class)
    private Date consumeTime;

    /**
     * 账单类型
     * 0：收入
     * 1：支出
     */
    private BillTypeEnum type;

    /**
     * 应付/应收金额
     */
    private BigDecimal payableOrReceivableAmount;

    /**
     * 实付/实收金额
     */
    private BigDecimal paymentOrReceiptsAmount;

    private BillStatusEnum status;

    @TableField(exist = false)
    private BigDecimal totalAmount;

    public Bill(BillVo billVo) {
        BeanUtils.copyProperties(billVo, this);
    }

    public Bill(BillParam billVo) {
        BeanUtils.copyProperties(billVo, this);
    }
}
