package com.gzhennaxia.bill.controller;

import com.gzhennaxia.bill.entity.User;
import com.gzhennaxia.bill.vo.UserVo;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@RestController
@RequestMapping("/vue-element-admin")
public class VueElementAdminController {

    @PostMapping("/user/login")
    public User userLogin(@RequestBody User user) {
        user.setToken("admin-token");
        return user;
    }

    @GetMapping("/user/info")
    public UserVo userInfo() {
        UserVo user = new UserVo();
        user.setName("Super Admin");
        user.setIntroduction("I am a super administrator");
        user.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        user.setRoles(Collections.singletonList("admin"));
        return user;
    }
}
