package com.gzhennaxia.bill.serialize;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.gzhennaxia.bill.converter.StringCodeToEnumConverterFactory;
import com.gzhennaxia.bill.enums.BaseEnum;
import lombok.Setter;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.core.convert.converter.Converter;

import java.io.IOException;

/**
 * @author lib
 */
@Setter
@JsonComponent
public class EnumDeserializer extends JsonDeserializer<Enum<? extends BaseEnum>> implements ContextualDeserializer {

    private Class<? extends BaseEnum> aClass;

    @Override
    public Enum<? extends BaseEnum> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        String text = jsonParser.getText();
        Converter<String, ? extends BaseEnum> converter = StringCodeToEnumConverterFactory.getConverter1(aClass);
        return (Enum<? extends BaseEnum>) converter.convert(text);
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty beanProperty) throws JsonMappingException {
        Class<?> rawClass = deserializationContext.getContextualType().getRawClass();
        EnumDeserializer deserializer = new EnumDeserializer();
        deserializer.setAClass((Class<? extends BaseEnum>) rawClass);
        return deserializer;
    }
}
