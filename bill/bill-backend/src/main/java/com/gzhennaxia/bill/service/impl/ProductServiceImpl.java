package com.gzhennaxia.bill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gzhennaxia.bill.entity.BillItem;
import com.gzhennaxia.bill.entity.BillItemSkuRel;
import com.gzhennaxia.bill.entity.Product;
import com.gzhennaxia.bill.enums.IsEnum;
import com.gzhennaxia.bill.mapper.BillItemMapper;
import com.gzhennaxia.bill.mapper.BillItemSkuRelMapper;
import com.gzhennaxia.bill.mapper.ProductMapper;
import com.gzhennaxia.bill.query.ProductQuery;
import com.gzhennaxia.bill.service.IProductService;
import com.gzhennaxia.bill.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private BillItemSkuRelMapper billItemSkuRelMapper;

    @Autowired
    private BillItemMapper billItemMapper;

    @Override
    public List<ProductVo> search(ProductQuery productQuery) {
        return baseMapper.search(productQuery);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resolveDuplicateName() {
        // 查询重复的商品名称
        List<String> duplicateNames = baseMapper.selectDuplicateName();
        for (String duplicateName : duplicateNames) {
            List<Product> products = baseMapper.selectList(Wrappers.<Product>lambdaQuery().eq(Product::getName, duplicateName).eq(Product::getDelFlag, IsEnum.NO.getCode()));
            if (products.size() > 1) {
                Integer id = products.get(0).getId();
                for (int i = 1; i < products.size(); i++) {
                    Product product = products.get(i);
                    billItemSkuRelMapper.update(new BillItemSkuRel().setProductId(id), Wrappers.<BillItemSkuRel>lambdaUpdate().set(BillItemSkuRel::getProductId, id).eq(BillItemSkuRel::getProductId, products.get(i).getId()));
                    billItemMapper.update(new BillItem().setProductId(id), Wrappers.<BillItem>lambdaUpdate().set(BillItem::getProductId, id).eq(BillItem::getProductId, products.get(i).getId()));
                    product.setDelFlag(IsEnum.YES);
                    baseMapper.updateById(product);
                }
            }
        }
    }
}
