package com.gzhennaxia.bill.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author lib
 * @since 2021-06-24
 */
@Data
public class ProductCategoryDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer parentId;

    private String name;

    private String path;

    private Integer isLeaf;

    private Integer status;


}
