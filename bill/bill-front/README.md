# bill-front

## el-cascader

当使用懒加载时，如何设置初始默认值？

1. 通过手动构造 options。
   注意：最后一级节点需要设置 leaf=true 才会回显！
   
2. 只需赋值为数组即可，且作为表单项时，取值时也是数组类型。（采用）

   需要注意的是，懒加载模式只会在组件初始化时才检查是否有默认值。

## el-form

1. form 表单如果不设置 label-width 会出现包含 `width:100%` 属性的表单项占整行。

   ![image-20210419104437125](D:\GitLab\projects\bill\bill-front\images\image-20210419104437125.png)

2. form 表单中的 input 无法输入

   需要使用 `v-model` 绑定，不能使用 `:model` / `:value`

## vue filter

> [Vue：filter全局使用](https://blog.csdn.net/HUSHILIN001/article/details/104355596)

可以把一些公共的格式化的代码抽取出来。

## el-input type=number

当给表单添加校验规则时，如果给input属性值添加 `type: number` 的校验，则需要使用 `v-model.number` 才可以。

## 正则表达式

> https://refrf.shreyasminocha.me/

## el-table

> 树形表格刷新问题，待实现（目前采用更改key，强制重新渲染的方式）

### 多选表格获取选中项

> [Element table 获取所有选择的行](https://blog.csdn.net/qq_36537108/article/details/89261394)

```js
// multipleTable 为表格的 ref
this.$refs.multipleTable.selection

## el-upload

el-upload 的 `on-error` 回调函数中的 err 参数（类型为object，不是 Error），无法通过 err.message 拿到后端定义的错误信息

​```js
handleError(error, file, fileList) {
    console.log('error', error)
    // error Error: {"code":4002,"data":null,"message":"文件已存在"}
    //    at getError (element-ui.common.js?5c96:29003)
    //    at XMLHttpRequest.onload (element-ui.common.js?5c96:29056)

	console.log('type', typeof error)
    // type object

    console.log('error.message', error.message)
    // error.message {"code":4002,"data":null,"message":"文件已存在"}

    console.log('error.message.message', error.message.message)
    // error.message.message undefined

    console.log('JSON.parse(error.message).message', JSON.parse(error.message).message)
    // JSON.parse(error.message).message 文件已存在
}
```

## Object.assign()

> [深入响应式原理#对于对象](https://cn.vuejs.org/v2/guide/reactivity.html#%E5%AF%B9%E4%BA%8E%E5%AF%B9%E8%B1%A1)

有时你可能需要为已有对象赋值多个新 property，比如使用 `Object.assign()` 或 `_.extend()`。但是，这样添加到对象上的新 property 不会触发更新。在这种情况下，你应该用原对象与要混合进去的对象的 property 一起创建一个新的对象。

```vue
// 代替 `Object.assign(this.someObject, { a: 1, b: 2 })`
this.someObject = Object.assign({}, this.someObject, { a: 1, b: 2 })
```

