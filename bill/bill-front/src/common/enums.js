export const BillTypeMap = new Map([
    [-1, '支出'],
    [1, '收入']
])

export const BillStatusMap = new Map([
    [0, '待填充账单项'],
    [1, '待完善账单项'],
    [2, '待归档'],
    [3, '已归档']
])

export const QuickBillStatusEnum = {
    'TO_BE_RECONCILED': {
        code: 0,
        message: "待对账"
    },
    'RECONCILIATION_FAILED': {
        code: 1,
        message: "对账失败"
    },
    'RECONCILED': {
        code: 2, message: "已对账"
    }
}

/**
 * 导入历史状态枚举
 */
export const ImportHistoryStatusEnum = {
    WAIT_IMPORT: {
        code: 0,
        message: "待导入"
    },
    IMPORTED: {
        code: 1,
        message: "已导入"
    }
}

/**
 * 统计类型枚举
 */
export const AnalyticsTypeEnum = {
    /**
     * 最新，即最新一条账单所在的年份
     */
    LATEST_ANNUAL_TOTAL: {
        code: 1,
        message: "最近一年的年度总收入/支出"
    },

    /**
     * 最新，即最新一条账单所在的年份
     */
    LATEST_MONTHLY_TOTAL: {
        code: 2,
        message: "最近一个月的月度总收入/支出"
    },

    /**
     * 最新，即最新一条账单所在的月份
     */
    LATEST_WEEKEND_TOTAL: {
        code: 3,
        message: "最近一周的周度总收入/支出"
    },

    LATEST_DAY_TOTAL: {
        code: 4,
        message: "最近一天的天度总收入/支出"
    }
}

const convertEnum2Map = function (enumObject) {
    let map = new Map()
    Object.keys(enumObject).map((key) => {
        map.set(enumObject[key].code, enumObject[key].message)
    })
    return map
}

export default {
    'billType': BillTypeMap,
    'billStatus': BillStatusMap,
    'quickBillStatus': convertEnum2Map(QuickBillStatusEnum),
    'importHistoryStatus': convertEnum2Map(ImportHistoryStatusEnum)
}
