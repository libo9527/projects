import Enums from "@/common/enums";
import {formatDateTime} from "@/utils/datetime";

const formatDict = (value, type) => {
    return Enums[type].get(value)
}

export default {
    formatDict,
    formatDateTime
}
