import {faEdit, faFileInvoiceDollar, faPlus, faQuestion, faTrashAlt} from "@fortawesome/free-solid-svg-icons";

export default [
    faEdit,
    faPlus,
    faTrashAlt,
    faFileInvoiceDollar,
    faQuestion
]
