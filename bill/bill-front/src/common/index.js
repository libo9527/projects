// 全局组件/方法
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {library} from '@fortawesome/fontawesome-svg-core'
import icons from './icon'
import ElementUI from "element-ui";
import filters from "@/common/filters";

library.add(icons)

// eslint-disable-next-line no-unused-vars
const install = function (Vue, options = {}) {

    Vue.component('font-awesome-icon', FontAwesomeIcon)

    Vue.use(ElementUI, {size: 'small'});

    Object.keys(filters).forEach(key => {
        Vue.filter(key, filters[key])
    })
}

export default {
    version: '1.0.0',
    install
}
