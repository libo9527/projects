import axios from "axios";
import {Message} from "element-ui";

const http = axios.create({
    baseURL: '/bill',
    timeout: 30 * 1000,
})

http.interceptors.response.use(res => {
    // console.log('res', res)
    const status = res.status
    if (status === 200) {
        if (res.data.code === 20000) {
            return Promise.resolve(res.data)
        } else {
            Message.error(res.data.message)
        }
    } else if (status === 404) {
        Message.error('接口不存在')
    } else {
        Message.error('系统错误')
    }
    return Promise.reject(res)
}, error => {
    console.log('error', error)
    Message.error('系统错误: ' + error.message)
    return Promise.reject(error)
})

export default http
