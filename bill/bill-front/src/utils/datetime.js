import moment from "moment";

export const formatDateTime = function (s) {
    if (s) {
        return moment(s).format("YYYY-MM-DD HH:mm:ss")
    } else {
        return ''
    }
}
