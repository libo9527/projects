import Vue from 'vue'
import App from './App.vue'
import router from "@/router"
import 'element-ui/lib/theme-chalk/index.css'
import CommonComponent from './common'
import less from 'less'
import './assets/css/global.css'

Vue.use(less)
Vue.use(CommonComponent)

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
