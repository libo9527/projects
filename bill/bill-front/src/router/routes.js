export default [
    {
        path: '/',
        name: 'home',
        redirect: '/bill',
        component: () => import('@/view/home'),
        children: [
            {
                path: '/dashboard',
                name: 'Dashboard',
                meta: {
                    title: '汇总面板'
                },
                component: () => import('@/view/dashboard')
            },
            {
                path: '/quick-bill',
                name: 'quick-bill',
                meta: {
                    title: '快捷账单'
                },
                component: () => import('@/view/quick-bill')
            },
            {
                path: '/quick-bill/edit',
                name: 'quick-bill-edit',
                meta: {
                    title: '快捷账单 > 编辑'
                },
                hidden: true,
                component: () => import('@/view/quick-bill/edit'),
            },
            {
                path: '/bill',
                name: 'bill',
                meta: {
                    title: '正式账单'
                },
                component: () => import('@/view/bill')
            },
            {
                path: '/bill/edit',
                name: 'bill-edit',
                meta: {
                    title: '正式账单 > 编辑'
                },
                hidden: true,
                component: () => import('@/view/bill/edit'),
            },
            {
                path: '/bill-category',
                name: 'bill-category',
                meta: {
                    title: '账单分类'
                },
                component: () => import('@/view/bill-category')
            }, {
                path: '/bill-tag',
                name: 'bill-tag',
                meta: {
                    title: '账单标记'
                },
                component: () => import('@/view/bill-tag')
            }, {
                path: '/platform',
                name: 'platform',
                meta: {
                    title: '交易平台'
                },
                component: () => import('@/view/platform')
            }, {
                path: '/pay-type',
                name: 'pay-type',
                meta: {
                    title: '支付方式'
                },
                component: () => import('@/view/pay-type')
            }, {
                path: '/counterparty',
                name: 'counterparty',
                meta: {
                    title: '交易对方'
                },
                component: () => import('@/view/counterparty')
            }, {
                path: '/product-category',
                name: 'product-category',
                meta: {
                    title: '商品分类'
                },
                component: () => import('@/view/product-category')
            }, {
                path: '/discount',
                name: 'discount',
                meta: {
                    title: '折扣类型'
                },
                component: () => import('@/view/discount')
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/view/login')
    }, {
        path: '/quick-bill/add',
        name: 'QuickBillAdd',
        component: () => import('@/view/quick-bill/add')
    }, {
        path: '/test',
        name: 'Test',
        component: () => import('@/view/test')
    }, {
        path: '/test/detail',
        name: 'EleOrderDetail',
        component: () => import('@/view/test/eleOrderDetail')
    }
]
