export default {
    mounted() {
        this.getTableData()
    },
    data() {
        return {
            searchConditions: {},
            loading: false,
            tableData: [],
            page: {}
        }
    },
    methods: {
        getTableData() {
            this.loading = true
            this.getTableApi(Object.assign(this.searchConditions, this.page)).then(res => {
                this.tableData = res.data.records
                this.page = {
                    current: res.data.current,
                    size: res.data.size,
                    total: res.data.total,
                }
                this.loading = false
            })
        },
        search() {
            this.page = {
                current: 1,
                size: 10,
                total: 0
            }
            this.getTableData()
        },
        reset() {
            this.$refs.searchForm.resetFields()
            this.search()
        },
        handleSizeChange(size) {
            this.page.size = size
            this.getTableData()
        },
        handleCurrentChange(current) {
            this.page.current = current
            this.getTableData()
        },
    }
}
