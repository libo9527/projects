import http from "@/utils/http.js";

export const getImportHistoryById = (id) => {
    return http.get(`/importHistory/${id}`)
}
