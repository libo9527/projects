import http from "@/utils/http.js";

export const searchSkus = (data) => {
    return http.post(`/sku/search`, data)
}
