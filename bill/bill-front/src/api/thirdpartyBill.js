import http from "@/utils/http";

/**
 * 爬取饿了么账单
 */
export const crawlEleBill = () => {
    return http.get(`/thirdparty-bill/crawl/ele`)
}
