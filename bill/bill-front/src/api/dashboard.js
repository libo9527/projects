import http from "@/utils/http.js";

export const getAnalytics = (data) => {
    return http.post('/analytics-result/analytics', data)
}
