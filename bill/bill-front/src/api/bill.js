import http from "@/utils/http.js";

export const getBillList = (data) => {
    return http.post('/bill/pageSearch', data)
}

export const saveBill = (data) => {
    return http.post('/bill/save', data)
}

export const getById = (id) => {
    return http.get(`/bill/${id}`)
}

export const getBillDetail = (id) => {
    return http.get(`/bill/${id}`)
}

export const deleteBillList = (ids) => {
    return http.post(`/bill/delete`, ids)
}
