import http from "@/utils/http.js";

export const getNodesByParentId = (parentId) => {
    return http.get(`/platform/children/${parentId}`)
}

export const getNodesByPath = (path) => {
    return http.get(`/platform/path/${path}`)
}
