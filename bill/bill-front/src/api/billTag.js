import http from "@/utils/http.js";

export const getBillTagList = (data) => {
    return http.post('/bill-tag/search', data)
}

export const getTagsByBillId = (billId) => {
    return http.get(`/bill-tag/billId/${billId}`)
}

export const searchTagsByKeyword = (keyword) => {
    return http.get(`/bill-tag/keyword/${keyword}`)
}

