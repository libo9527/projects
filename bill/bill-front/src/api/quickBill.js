/**
 * 保存快捷账单录入
 */
import http from "@/utils/http.js";

export const getQuickBillDetailById = (id) => {
    return http.get(`/quickbill/${id}`)
}
export const getQuickBillPage = (data = {}) => {
    return http.post('/quickbill/page', data)
}
export const saveQuickBill = (data) => {
    return http.post('/quickbill/save', data)
}
export const importQuickBillFromFile = (importHistoryId, params) => {
    return http.get(`/quickbill/importFromFile/${importHistoryId}`, {params: params})
}
export const deleteQuickBill = (id) => {
    return http.post(`/quickbill/delete/${id}`)
}
/**
 * 自动对账
 */
export const autoReconciliation = (quickBillId) => {
    return http.get(`/quickbill/reconciliation/auto?quickBillId=${quickBillId}`)
}
/**
 * 批量自动对账
 */
export const batchAutoReconciliation = (quickBillIds) => {
    return http.post(`/quickbill/reconciliation/auto/batch`, quickBillIds)
}
/**
 * 手动对账
 */
export const manualReconciliation = (data, quickBillId) => {
    return http.post(`/quickbill/reconciliation/manual?quickBillId=${quickBillId}`, data)
}
