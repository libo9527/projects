import http from "@/utils/http.js";

export const saveBillItem = (data) => {
    return http.post(`/bill-item/save`, data)
}
export const deleteBillItemById = (id) => {
    return http.post(`/bill-item/delete/${id}`)
}
