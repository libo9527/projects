import http from "@/utils/http.js";

export const searchProducts = (data) => {
    return http.post(`/product/search`, data)
}
