import http from "@/utils/http.js";

export const getCategoryNodesByParentId = (parentId) => {
    return http.get(`/bill-category/children/${parentId}`)
}

export const getCategoryNodesByPath = (path) => {
    return http.get(`/bill-category/path/${path}`)
}

export const getBillCategoryList = (data) => {
    return http.post(`/bill-category/search`, data)
}

export const saveBillCategory = (data) => {
    return http.post(`/bill-category/save`, data)
}

export const deleteBillCategory = (data) => {
    return http.post(`/bill-category/delete`, data)
}

export const getCategoryLikeName = (name) => {
    return http.get(`/bill-category/namelike/${name}`, )
}
