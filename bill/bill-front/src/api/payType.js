import http from "@/utils/http.js";

export const getPayTypeNodesByParentId = (parentId) => {
    return http.get(`/pay-type/children/${parentId}`)
}

export const getPayTypeNodesByPath = (path) => {
    return http.get(`/pay-type/path/${path}`)
}
