import http from "@/utils/http.js";

export const getAllDiscount = () => {
    return http.get('/discount/all')
}

export const pageSearchDiscounts = (data) => {
    return http.post('/discount/page', data)
}

export const saveDiscount = (data) => {
    return http.post('/discount/save', data)
}
export const searchDiscountsByKeyword = (keyword) => {
    return http.get(`/discount/keyword/${keyword}`)
}

export const getDiscountsByBillId = (billId) => {
    return http.get(`/discount/billId/${billId}`)
}
