<#assign toCamel = "com.gzhennaxia.generator.bill.Entity2Camel"?new() />
<#assign camelEntity = toCamel(entity) />
package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;
import com.gzhennaxia.bill.query.${entity}Query;
import com.gzhennaxia.bill.vo.${entity}Vo;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

    @Override
    public List<${entity}Vo> search(${entity}Query ${camelEntity}Query) {
        return list().stream().map(${entity}Vo::new).collect(Collectors.toList());
    }
}
</#if>
