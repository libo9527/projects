package ${package.Controller};
<#assign toCamel = "com.gzhennaxia.generator.bill.Entity2Camel"?new() />
<#assign camelEntity = toCamel(entity) />

import lombok.RequiredArgsConstructor;
import com.gzhennaxia.bill.service.I${entity}Service;
import java.util.List;
import com.gzhennaxia.bill.query.${entity}Query;
import com.gzhennaxia.bill.vo.${entity}Vo;
import org.springframework.web.bind.annotation.PostMapping;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequiredArgsConstructor
<#assign toLowerHyphen = "com.gzhennaxia.generator.bill.Entity2LowerHyphen"?new() />
@RequestMapping("/${toLowerHyphen(entity)}")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    private final I${entity}Service ${camelEntity}Service;

    @PostMapping("/search")
    public List<${entity}Vo> search(${entity}Query ${camelEntity}Query) {
       return ${camelEntity}Service.search(${camelEntity}Query);
    }
}
</#if>
