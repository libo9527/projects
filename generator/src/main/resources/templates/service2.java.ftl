<#assign toCamel = "com.gzhennaxia.generator.bill.Entity2Camel"?new() />
<#assign camelEntity = toCamel(entity) />
package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import java.util.List;
import com.gzhennaxia.bill.query.${entity}Query;
import com.gzhennaxia.bill.vo.${entity}Vo;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    List<${entity}Vo> search(${entity}Query ${camelEntity}Query);
}
</#if>
