package com.gzhennaxia.generator.bill;

import com.google.common.base.CaseFormat;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;

import java.util.List;

/**
 * @author lib
 */
public class Entity2Camel implements TemplateMethodModelEx {

    @Override
    public Object exec(List args) {
        SimpleScalar arg0 = (SimpleScalar) args.get(0);
        String s = arg0.toString();
        return toCamel(s);
    }

    private String toCamel(String s) {
        char[] chars = s.toCharArray();
        chars[0] += 32;
        return String.valueOf(chars);
    }

    public static void main(String[] args) {
        Entity2Camel utils = new Entity2Camel();
        System.out.println(utils.toCamel("Entity"));

        String to = CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, "test-data");

    }
}
