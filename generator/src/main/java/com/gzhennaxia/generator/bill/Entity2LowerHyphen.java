package com.gzhennaxia.generator.bill;

import com.google.common.base.CaseFormat;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;

import java.util.List;

/**
 * @author lib
 */
public class Entity2LowerHyphen implements TemplateMethodModelEx {

    @Override
    public Object exec(List args) {
        SimpleScalar arg0 = (SimpleScalar) args.get(0);
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, arg0.toString());
    }
}
