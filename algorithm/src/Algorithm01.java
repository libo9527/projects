import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * ## 判断数组是否连续
 * <p>
 * > 笔试题：2021-11-17 16:00 ｜ BOSS | 行云集团
 * >
 * > [判断数组中的元素是否连续](https://blog.csdn.net/zrh_CSDN/article/details/81081856)
 * <p>
 * 判断一个数组中非负整数是否连续，零可以替换成任何非负整数。除0外其他值不重复。
 * <p>
 * 例子：输入 1,3,5,0,0,0,7,6，输出该数组连续，零替换为 2,4,8 号数字可以为连续数字 1,2,3,4,5,6,7,8
 * <p>
 * 解析：题目关键字：非负整数、不重复。如果0也不重复（即整个数组都是不重复的，只是乱序而已），
 * 那要使数组连续，那最大值减去最小值就要等于（len-1）。缺了非负和不重复，上述关系就不成立。
 * 但是有0这个通配符可以重复的时候，那么只要 `max-min<=len-1` 就可以了。
 */
public class Algorithm01 {

    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 5, 0, 0, 0, 7, 6};
        System.out.println(isConsecutiveArray(array));
    }

    private static boolean isConsecutiveArray(int[] array) {
        if (array == null) return false;
        int min = Integer.MAX_VALUE;
        int max = 0;
        for (int value : array) {
            if (value != 0) {
                if (value < min) min = value;
                if (value > max) max = value;
            }
        }
        // 如果max==0，说明数组中的元素全为0。
        // max-min<=len-1，即max-min<len
        return max == 0 || max - min < array.length;
    }
}
