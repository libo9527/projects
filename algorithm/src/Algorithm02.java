import java.util.HashMap;

public class Algorithm02 {

    /**
     * 普通写法：
     * new Thread(new Runnable() {
     *
     * @Override public void run() {
     * <p>
     * }
     * }).start();
     * lambda写法：
     * new Thread(() -> {
     * <p>
     * }).start();
     */
    public static void main(String[] args) {
        Object lock = new Object();
        int i = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (lock) {
                        lock.notifyAll();
                        System.out.println(Thread.currentThread().getName());
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }, "thread-01").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    synchronized (lock) {
                        lock.notifyAll();
                        System.out.println(Thread.currentThread().getName());
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        new HashMap<>();
                    }
                }
            }
        }, "thread-02").start();
    }
}
