# [数据库](./DATASOURCE.md)





### 用 Typora 打开 IDEA 中的 md 文件

> [IntelliJ IDEA使用第三方应用程序打开项目中的某类型文件_棒棒 ...](https://blog.csdn.net/lqs365/article/details/106278533)
>
> [Intellij IDEA 使用系统默认工具打开文件]([https://note.sunfeilong.com/%E7%BC%96%E7%A8%8B%E5%B7%A5%E5%85%B7/%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7/Intellij%20IDEA/Intellij%20IDEA%20%E4%BD%BF%E7%94%A8%E7%B3%BB%E7%BB%9F%E9%BB%98%E8%AE%A4%E5%B7%A5%E5%85%B7%E6%89%93%E5%BC%80%E6%96%87%E4%BB%B6/](https://note.sunfeilong.com/编程工具/开发工具/Intellij IDEA/Intellij IDEA 使用系统默认工具打开文件/))


1. 在 Settings > Tools > External Tools 中添加 Typora

   ![image-20210417104229583](D:\GitLab\projects\images\image-20210417104229583.png)

2. 添加快捷键

   ![image-20210417104433974](D:\GitLab\projects\images\image-20210417104433974.png)

### Windows 批处理

> [bat 批处理教程](https://www.w3cschool.cn/dosmlxxsc1/wvqyr9.html)

1. `@echo off`

   `echo off` 命令表示关闭回显，但 `echo off` 也是命令，它本身也会回显，如果连这条也不显示，就在前面加个 `@`，它的作用是让执行窗口中不显示它后面这一行的命令本身。
   
2. for

   1. for命令的形式变量只能是26个字母中的任意一个。
   2. 字母区分大小写（即就是 %%a 和 %%A 是两个不同的变量名）。

## TODO

- [ ] 学习[Spring Cloud Sleuth](https://spring.io/projects/spring-cloud-sleuth)
- [ ] 学习[Zipkin](https://zipkin.io/)

- [ ] 学习[Zan Proxy](https://youzan.github.io/zan-proxy/book/)

