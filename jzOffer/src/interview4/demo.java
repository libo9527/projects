package interview4;

/**
 * 剑指Offer（第二版）
 * <p>
 * 面试题4:二维数组中的查找
 * 题目:在一个二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
 * 请完成一个函数，输入这样的个二维数组和一个整数，判断数组中是否含有该整数。
 * <p>
 * 例如下面的二维数组就是每行、每列都递增排序。
 * 如果在这个数组中查找数字7，则返回true;如果查找数字5，由于数组不含有该数字，则返回 false。
 * 1    2   8   9
 * 2    4   9   12
 * 4    7   10  13
 * 6    8   11  15
 * <p>
 * 解析：从二维数组的右上角开始比较，
 * 如果等于要找的数，直接返回true；
 * 如果比要找的数大，那么要找的数一定不在当前遍历数组所在的行，则可以删掉这一行，继续比较下一个右上角的数字；
 * 如果比要找的数小，那么要找的数一定不在当前遍历数组所在的列，则可以删掉这一列，继续比较下一个右上角的数字；
 * <p>
 * 在前面的分析中，我们每次都选取数组查找范围内的右上角数字。同样，我们也可以选取左下角的数字。
 * 但我们不能选择左上角数字或者右下角数字。
 * 以左上角数字为例，最初数字!位干初始救组的左上角，由干!小干7.那么7应该位于1的右边或者下边。
 * 此时我们既不能从查找范围内剔除1所在的行，也不能剔除!所在的列，这样我们就无法缩小查找的范围。
 */
public class demo {

    public static void main(String[] args) {
        int[][] array = new int[][]{
                {1, 2, 8, 9},
                {2, 4, 9, 12},
                {4, 7, 10, 13},
                {6, 8, 11, 15}
        };
        System.out.println(exit(array, 7));
    }

    public static boolean exit(int[][] arr, int number) {
        if (arr == null || arr.length <= 0) return false;
        int row = 0;
        int col = arr[0].length - 1;
        while (row < arr.length && col >= 0) {
            if (number == arr[row][col]) {
                return true;
            } else if (number > arr[row][col]) {
                row++;
            } else {
                col--;
            }
        }
        return false;
    }
}
