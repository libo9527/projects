package interview12;

/**
 * 剑指Offer（第二版）
 */
public class Demo {

    public static void main(String[] args) {

        char[][] matrix = new char[][]{
                {'a', 'b', 't', 'g'},
                {'c', 'f', 'c', 's'},
                {'j', 'd', 'e', 'h'}
        };
//        String str = "bfce";
//        String str = "abfb";
        String str = "cfcs";
        System.out.println(hasPath(matrix, str));
    }

    private static boolean hasPath(char[][] matrix, String str) {
        int rows, cols;
        if (matrix == null || (rows = matrix.length) <= 0 || (cols = matrix[0].length) <= 0 || str == null) {
            return false;
        }
        boolean[][] visited = new boolean[rows][cols];
        int i = 0;
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (hasPath(matrix, row, col, str, i, visited)) {
                    printArray(visited);
                    return true;
                }
            }
        }
        return false;
    }

    private static void printArray(boolean[][] visited) {
        for (int i = 0; i < visited.length; i++) {
            for (int j = 0; j < visited[i].length; j++) {
                System.out.print(visited[i][j] + "\t");
            }
            System.out.println();
        }
    }

    /**
     * 从字符数组matrix第row行第col列开始走，是否存在与字符串str从下表i开始的子字符串相等的路径
     */
    private static boolean hasPath(char[][] matrix, int row, int col, String str, int i, boolean[][] visited) {
        int rows, cols;
        if (matrix == null || (rows = matrix.length) <= 0 || (cols = matrix[0].length) <= 0
                || row < 0 || row >= rows
                || col < 0 || col >= cols
                || str == null || str.length() <= 0 ||
                i < 0 || i > str.length()) {
            return false;
        }
        boolean hasPath = false;
        if (i == str.length()) return true;
        if (str.charAt(i) == matrix[row][col] && !visited[row][col]) {
            visited[row][col] = true;
            i++;
            hasPath = hasPath(matrix, row - 1, col, str, i, visited) ||
                    hasPath(matrix, row + 1, col, str, i, visited) ||
                    hasPath(matrix, row, col - 1, str, i, visited) ||
                    hasPath(matrix, row, col + 1, str, i, visited);
            if (!hasPath) {
                visited[row][col] = false;
            }
        }
        return hasPath;
    }
}
