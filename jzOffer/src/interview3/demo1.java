package interview3;

/**
 * 剑指Offer（第二版）
 * 面试题3:数组中重复的数字
 * 题目一:找出数组中重复的数字。
 * 在一个长度为n的数组里的所有数字都在0~n-1的范围内。数组中某些数字是重复的，
 * 但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。
 * 例如，如果输入长度为7的数组{2，3，1，0，2，5，3，1}，那么对应的输出是重复的数字2或者3.
 */
public class demo1 {

    public static void main(String[] args) {
//        int[] a = null;
//        int[] a = new int[]{};
//        int[] a = new int[]{0, 1, 2};
        int[] a = new int[]{2, 3, 1, 0, 2, 5, 3};
        System.out.println(duplicate(a));
    }

    public static int duplicate(int[] arr) {
        int n;
        if (arr == null || (n = arr.length) <= 0) return -1;
        for (int i = 0; i < n; i++) {
            if (arr[i] < 0 || arr[i] > n - 1) return -1;
        }
        for (int i = 0; i < n; i++) {
            while (arr[i] != i) {
                if (arr[i] == arr[arr[i]]) return arr[i];
                // 错误：
                // int temp = arr[i];
                // arr[i] = arr[arr[i]];
                // arr[arr[i]] = temp; 上一步中arr[i]的值已经变了！
                int temp = arr[i];
                arr[i] = arr[temp];
                arr[temp] = temp;
            }
        }
        return -1;
    }
}
