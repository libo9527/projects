package interview3;

/**
 * 剑指Offer（第二版）
 * 面试题3:数组中重复的数字
 * 题目二:不修改数组找出重复的数字。
 * 在一个长度为n+1的数组里的所有数字都在1~n的范围内，所以数组中至少有一个数字是重复的。
 * 请找出数组中任意一个重复的数字，但不能修改输入的数组。
 * 例如，如果输入长度为8的数组{2，3，5，4，3，2，6，7}，那么对应的输出是重复的数字2或者3。
 * <p>
 * 解析：我们把从1~n的数字从中间的数字m分为两部分，前面一半为1~m，后面一半为m+1~n。
 * 如果1~m的数字的数目超过m，那么这一半的区间里一定包含重复的数字;否则，另一半m+1~n的区间里一定包含重复的数字。
 * 我们可以继续把包含重复数字的区间一分为二，直到找到一个重复的数字。这个过程和二分查找算法很类似，只是多了一步统计区间里数字的数目。
 * <p>
 * 证明：如果1~m的数字的数目超过m，那么这一半的区间里一定包含重复的数字;否则，另一半m+1~n的区间里一定包含重复的数字。
 * <p>
 * 证明1：如果1~m的数字的数目超过m，那么这一半的区间里一定包含重复的数字;
 * 利用反证法，假设：1~m的数字的数目超过m，且这一半的区间里不包含重复的数字;
 * 因为这一半的区间里不包含重复的数字，所以区间内每个数字x要么没出现，要么只出现一次，即x出现的次数y(x)<=1。
 * 所以1~m的数字的数目y(1~m)<=m，与假设矛盾，所以原命题成立。
 * <p>
 * 证明2：否则，另一半m+1~n的区间里一定包含重复的数字。
 * 设：1~m出现的次数为x，m+1~n出现的次数为y。
 * 则x+y=n+1，y=n+1-x
 * 因为x<=m，所以-x>=-m，y>=n+1-m>n-m。
 * m+1~n区间数字个数=n-(m+1)+1=n-m
 * 即，m+1~n出现的次数为y，大于m+1~n区间数字个数，又证明1得知，在m+1~n区间一定存在重复的数字！
 */
public class demo2 {

    public static void main(String[] args) {
//        int[] a = null;
//        int[] a = new int[]{};
//        int[] a = new int[]{0, 1, 2};
//        int[] a = new int[]{2, 3, 1, 1, 2, 5, 3};
        int[] a = new int[]{2, 3, 5, 4, 3, 2, 6, 7};
        System.out.println(duplicate(a));
    }

    public static int duplicate(int[] arr) {
        int len;
        if (arr == null || (len = arr.length) <= 1) return -1;
        for (int i = 0; i < len; i++) {
            if (arr[i] < 1 || arr[i] > len - 1) return -1;
        }
        int start = 1;
        int end = len - 1;
        while (start <= end) {
            int middle = start + (end - start) / 2;
            int count = countRange(arr, start, middle);
            if (start == end) {
                if (count > 1) {
                    return start;
                } else {
                    break;
                }
            }
            if (count > middle - start + 1) {
                end = middle;
            } else {
                start = middle + 1;
            }
        }
        return -1;
    }

    private static int countRange(int[] arr, int start, int end) {
        if (arr == null || arr.length <= 0) return 0;
        int count = 0;
        for (int e : arr) {
            if (e >= start && e <= end) count++;
        }
        return count;
    }
}
